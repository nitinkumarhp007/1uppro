package com.uppro.Util;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.os.StrictMode;

import com.google.firebase.FirebaseApp;


public class AppController extends Application
{
    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;

    @Override
    public void onCreate()
    {
        super.onCreate();
        mInstance = this;
        FirebaseApp.initializeApp(this);
        Intent intent= new Intent(mInstance, NetworkServices.class);
        mInstance.startService(intent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

        }
    }

    public static synchronized AppController getInstance()
    {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener)
    {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}