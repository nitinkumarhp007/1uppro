package com.uppro.Util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;

import androidx.appcompat.app.AlertDialog;

import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public abstract class GetAsyncSSL extends AsyncTask<String, Void, String>

{

    String url = "";
    RequestBody requestBody;
    Context context;
    WeakReference<Context> contextWeakReference;
    AlertDialog.Builder builder;
    private SavePref savePref;
    String AUTHORIZATION_KEY="";

    public GetAsyncSSL(Context context, String url, RequestBody requestBody, String AUTHORIZATION_KEY)

    {
        this.url = url;
        this.AUTHORIZATION_KEY = AUTHORIZATION_KEY;
        this.requestBody = requestBody;
        contextWeakReference = new WeakReference<Context>(context);
        this.context = contextWeakReference.get();
    }

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        savePref=new SavePref(context);
    }

    @Override
    protected String doInBackground(String... params) {
        String jsonData = "";
        try {
            try {
                OkHttpClient client = getUnsafeOkHttpClient();
                       /* .readTimeout(30, TimeUnit.SECONDS)
                        .sslSocketFactory(SslMockUtils.TRUST_ALL_SOCKET_FACTORY, SslMockUtils.TRUST_MANAGER)
                        .hostnameVerifier(SslMockUtils.TRUST_ALL_HOSTNAME_VERIFIER);
                        .build();*/

                Request request = new Request.Builder()
                        .post(requestBody)
                        .addHeader(Parameters.AUTHORIZATION_KEY,"Bearer "+AUTHORIZATION_KEY)
                        .addHeader(Parameters.SECURITYKEY,util.SECURITYKEY)
                        .url(url)
                        .build();

                try
                {
                    Response responses = client.newCall(request).execute();

                    jsonData = responses.body().string();
                }
                catch (SocketTimeoutException ex)
                {
                    showDialog();
                }
                catch (ConnectException ex)
                {
                    showDialog();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonData;
    }

    @Override
    protected void onPostExecute(String result)
    {
        super.onPostExecute(result);
        if (result != null) getValueParse(result);
    }

    public abstract void getValueParse(String listValue);

    public abstract void retry();

    private void showDialog() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)

        {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        else
        {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle("Warning!");
        builder.setMessage(util.internet_Connection_Error);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
        {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        retry();
                    }
                });

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog alert11 = builder.create();
                alert11.show();
            }
        });

    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}