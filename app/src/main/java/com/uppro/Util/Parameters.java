package com.uppro.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "Authorization";
    public static final String AUTH_KEY = "auth_key";
    public static final String SECURITYKEY = "securitykey";
    public static final String DEVICE_TOKEN = "deviceToken";
    public static final String EMAIL = "email";
    public static final String USERID ="userId" ;
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_TYPE = "soical_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "deviceType";
    public static final String RANDOM_NUMBER = "random_number";
    public static final String FIRST_NAME = "first_name";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String PAYPALID = "paypalId";
    public static final String COUNTRY = "country";
    public static final String OLD_PASSWORD = "oldPassword";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String IMAGE = "image";
    public static final String IMAGES = "images";

    public static final String BLOCKEDUSERID = "blockedUserId";
    public static final String ORDERID = "orderId";
    public static final String MESSAGE = "message";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String CATEGORY_ID = "categoryId";
    public static final String FOODCATEGORY_ID = "foodCategoryId";
    public static final String RENT = "rent";
    public static final String DESCRIPTION = "description";
    public static final String PRODUCT_ID = "productId";
    public static final String RENT_FROM = "rent_from";
    public static final String RENT_TO = "rent_to";
    public static final String RENT_DAYS = "rent_days";
    public static final String BILL_AMOUNT = "bill_amount";
    public static final String BUYER_PAYMENT_JSON = "buyer_payment_json";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String IS_OFFER = "is_offer";
    public static final String OFFER_AMOUNT = "offer_amount";
    public static final String MESSAGE_ID = "message_id";
    public static final String OFFER_STATUS = "offer_status";
    public static final String STATUS = "status";
    public static final String REQUEST_ID = "request_id";
    public static final String REQUEST_ID_ = "Request_id";
    public static final String CARD_NO = "card_number";
    public static final String CARD_TYPE = "card_type";
    public static final String CARD_CVV = "card_cvv";
    public static final String EXPIRY_MONTH = "card_expiry_month";
    public static final String EXPIRY_YEAR = "card_expiry_year";

    public static final String CARD_NAME = "cardholdername";
    public static final String AMOUNT = "amount";
    public static final String CARD_ID = "card_id";
    public static final String CANCEL_BY = "cancel_by";
    public static final String PAYPAL_EMAIL = "paypal_email";
    public static final String RANGE = "range";
    public static final String DAILY = "daily";
    public static final String WEEKLY = "weekly";
    public static final String LOCATION = "location";
    public static final String STARTDATE = "startDate";
    public static final String STARTDATE_SHOW = "startDateShow";
    public static final String ENDDATE = "endDate";
    public static final String ENDDATE_SHOW = "endDateShow";
    public static final String CHALLENGEREPORTTEXTID = "challengeReportTextId";
    public static final String TYPE = "type";
    public static final String CHALLENGEDUSERID = "challengedUserId";
    public static final String CHALLENGEDID = "challengeId";
    public static final String CHALLENGERID = "challengerId";
    public static final String REPORTEDUSERID = "reportedUserId";
    public static final String ISPAID = "isPaid";
    public static final String COMPETITORS = "competitors";

    public static final String TIME ="time" ;
    public static final String ISRULES ="isRules" ;
    public static final String RULES ="rules" ;
    public static final String VIDEO ="video" ;
    public static final String VIDEOTHUMBNAIL = "videoThumbnail";
    public static final String ISACTIVE = "isActive";
    public static final String ID ="id" ;
    public static final String CHAT_ID = "chatId";
    public static final String BIO ="bio" ;
    public static final String YOUTUBE = "youtubeLink";
    public static final String INSTAGRAM ="instagramLink" ;
    public static final String WEBSITE = "websiteLink";
    public static final String TIKTOK ="tiktokLink" ;
    public static final String OTHERUSERID ="otherUserId" ;
    public static final String COMMENTTEXT = "commentText";
    public static final String USERNAME = "username";
    public static final String ISLIKE = "isLike";
    public static final String KEYWORD = "keyword";
    public static final String TAGIDS = "tagIds";
    public static final String NOTOFICATIONID ="notificationId" ;
    public static final String COMMENTID ="id" ;
    public static final String MESSAGEIDS ="messageId" ;
    public static final String STRIPEID ="stripeId" ;
    public static final String CARDNUMBER = "cardNumber";
    public static final String CARDEXPMONTH = "expMonth";
    public static final String CARDEXPYEAR = "expYear";
    public static final String CARDCVC = "cvv";
}















