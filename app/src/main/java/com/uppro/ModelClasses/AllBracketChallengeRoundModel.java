package com.uppro.ModelClasses;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AllBracketChallengeRoundModel implements Parcelable {

    public final static Creator<AllBracketChallengeRoundModel> CREATOR = new Creator<AllBracketChallengeRoundModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AllBracketChallengeRoundModel createFromParcel(android.os.Parcel in) {
            return new AllBracketChallengeRoundModel(in);
        }

        public AllBracketChallengeRoundModel[] newArray(int size) {
            return (new AllBracketChallengeRoundModel[size]);
        }

    };
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("body")
    @Expose
    private AllBracketChallengeRoundBody body;

    protected AllBracketChallengeRoundModel(android.os.Parcel in) {
        this.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.body = ((AllBracketChallengeRoundBody) in.readValue((AllBracketChallengeRoundBody.class.getClassLoader())));
    }

    public AllBracketChallengeRoundModel() {
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AllBracketChallengeRoundBody getBody() {
        return body;
    }

    public void setBody(AllBracketChallengeRoundBody body) {
        this.body = body;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(body);
    }

    public int describeContents() {
        return 0;
    }

}

/*public class Contender1 implements Parcelable {

    public final static Creator<Contender1> CREATOR = new Creator<Contender1>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Contender1 createFromParcel(android.os.Parcel in) {
            return new Contender1(in);
        }

        public Contender1[] newArray(int size) {
            return (new Contender1[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("deviceType")
    @Expose
    private Integer deviceType;
    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;

    protected Contender1(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.deviceType = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.deviceToken = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Contender1() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(username);
        dest.writeValue(image);
        dest.writeValue(deviceType);
        dest.writeValue(deviceToken);
    }

    public int describeContents() {
        return 0;
    }

}*/

/*public class Contender2 implements Parcelable {

    public final static Creator<Contender2> CREATOR = new Creator<Contender2>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Contender2 createFromParcel(android.os.Parcel in) {
            return new Contender2(in);
        }

        public Contender2[] newArray(int size) {
            return (new Contender2[size]);
        }

    };
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("deviceType")
    @Expose
    private Integer deviceType;
    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;

    protected Contender2(android.os.Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.deviceType = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.deviceToken = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Contender2() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(username);
        dest.writeValue(image);
        dest.writeValue(deviceType);
        dest.writeValue(deviceToken);
    }

    public int describeContents() {
        return 0;
    }

}*/

