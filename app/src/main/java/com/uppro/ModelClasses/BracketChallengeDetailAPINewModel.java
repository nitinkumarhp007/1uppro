package com.uppro.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class BracketChallengeDetailAPINewModel implements Parcelable {

    public BracketChallengeDetailAPINewModel() {
    }

    public String created = "";
    public String id = "";
    public String status = "";
    public String type = "";
    public String userId = "";
    public String winnerId = "";
    public String loserId = "";
    public String isRules = "";
    public String isPaid = "";
    public String categoryId = "";
    public String category_name = "";
    public String category_image = "";
    public String competitors = "";
    public String time = "";
    public String startDate = "";
    public String endDate = "";
    public String startDateTimestamp = "";
    public String endDateTimestamp = "";
    public String video = "";
    public String videoThumbnail = "";
    public String rules = "";
    public String joiningFee = "";
    public String sortTimestamp = "";
    public String createdAt = "";
    public String updatedAt = "";
    public String isEntered = "";
    String iHavePaid = "";
    String iHaveSubmitted = "";
    String joinedChallengersIncludingMe = "";
    String UserId__challenger = "";
    String Name_challenger = "";
    String Image_challenger = "";
    String Thumb_challenger = "";
    String Video_challenger = "";
    String Votes = "";

    ArrayList<UserModel> challenge_users_list;

    ArrayList<ChallengeUserModel> challengeRoundChallengerPairs;
    ArrayList<ChallengeUserModel> challengeRoundChallengerPairsTwo;
    ArrayList<ChallengeUserModel> challengeRoundChallengerPairsThree;
    ArrayList<ChallengeUserModel> challengeRoundChallengerPairsFour;
    ArrayList<ChallengeUserModel> challengeRoundChallengerPairsFive;
    ArrayList<ChallengeUserModel> challengeRoundChallengerPairsSix;


    protected BracketChallengeDetailAPINewModel(Parcel in) {
        created = in.readString();
        id = in.readString();
        status = in.readString();
        type = in.readString();
        userId = in.readString();
        winnerId = in.readString();
        loserId = in.readString();
        isRules = in.readString();
        isPaid = in.readString();
        categoryId = in.readString();
        category_name = in.readString();
        category_image = in.readString();
        competitors = in.readString();
        time = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        startDateTimestamp = in.readString();
        endDateTimestamp = in.readString();
        video = in.readString();
        videoThumbnail = in.readString();
        rules = in.readString();
        joiningFee = in.readString();
        sortTimestamp = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        isEntered = in.readString();
        iHavePaid = in.readString();
        iHaveSubmitted = in.readString();
        joinedChallengersIncludingMe = in.readString();
        UserId__challenger = in.readString();
        Name_challenger = in.readString();
        Image_challenger = in.readString();
        Thumb_challenger = in.readString();
        Video_challenger = in.readString();
        Votes = in.readString();
        challenge_users_list = in.createTypedArrayList(UserModel.CREATOR);
        challengeRoundChallengerPairs = in.createTypedArrayList(ChallengeUserModel.CREATOR);
        challengeRoundChallengerPairsTwo = in.createTypedArrayList(ChallengeUserModel.CREATOR);
        challengeRoundChallengerPairsThree = in.createTypedArrayList(ChallengeUserModel.CREATOR);
        challengeRoundChallengerPairsFour = in.createTypedArrayList(ChallengeUserModel.CREATOR);
        challengeRoundChallengerPairsFive = in.createTypedArrayList(ChallengeUserModel.CREATOR);
        challengeRoundChallengerPairsSix = in.createTypedArrayList(ChallengeUserModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(created);
        dest.writeString(id);
        dest.writeString(status);
        dest.writeString(type);
        dest.writeString(userId);
        dest.writeString(winnerId);
        dest.writeString(loserId);
        dest.writeString(isRules);
        dest.writeString(isPaid);
        dest.writeString(categoryId);
        dest.writeString(category_name);
        dest.writeString(category_image);
        dest.writeString(competitors);
        dest.writeString(time);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(startDateTimestamp);
        dest.writeString(endDateTimestamp);
        dest.writeString(video);
        dest.writeString(videoThumbnail);
        dest.writeString(rules);
        dest.writeString(joiningFee);
        dest.writeString(sortTimestamp);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(isEntered);
        dest.writeString(iHavePaid);
        dest.writeString(iHaveSubmitted);
        dest.writeString(joinedChallengersIncludingMe);
        dest.writeString(UserId__challenger);
        dest.writeString(Name_challenger);
        dest.writeString(Image_challenger);
        dest.writeString(Thumb_challenger);
        dest.writeString(Video_challenger);
        dest.writeString(Votes);
        dest.writeTypedList(challenge_users_list);
        dest.writeTypedList(challengeRoundChallengerPairs);
        dest.writeTypedList(challengeRoundChallengerPairsTwo);
        dest.writeTypedList(challengeRoundChallengerPairsThree);
        dest.writeTypedList(challengeRoundChallengerPairsFour);
        dest.writeTypedList(challengeRoundChallengerPairsFive);
        dest.writeTypedList(challengeRoundChallengerPairsSix);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BracketChallengeDetailAPINewModel> CREATOR = new Creator<BracketChallengeDetailAPINewModel>() {
        @Override
        public BracketChallengeDetailAPINewModel createFromParcel(Parcel in) {
            return new BracketChallengeDetailAPINewModel(in);
        }

        @Override
        public BracketChallengeDetailAPINewModel[] newArray(int size) {
            return new BracketChallengeDetailAPINewModel[size];
        }
    };

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public String getLoserId() {
        return loserId;
    }

    public void setLoserId(String loserId) {
        this.loserId = loserId;
    }

    public String getIsRules() {
        return isRules;
    }

    public void setIsRules(String isRules) {
        this.isRules = isRules;
    }

    public String getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(String isPaid) {
        this.isPaid = isPaid;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }

    public String getCompetitors() {
        return competitors;
    }

    public void setCompetitors(String competitors) {
        this.competitors = competitors;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDateTimestamp() {
        return startDateTimestamp;
    }

    public void setStartDateTimestamp(String startDateTimestamp) {
        this.startDateTimestamp = startDateTimestamp;
    }

    public String getEndDateTimestamp() {
        return endDateTimestamp;
    }

    public void setEndDateTimestamp(String endDateTimestamp) {
        this.endDateTimestamp = endDateTimestamp;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getJoiningFee() {
        return joiningFee;
    }

    public void setJoiningFee(String joiningFee) {
        this.joiningFee = joiningFee;
    }

    public String getSortTimestamp() {
        return sortTimestamp;
    }

    public void setSortTimestamp(String sortTimestamp) {
        this.sortTimestamp = sortTimestamp;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsEntered() {
        return isEntered;
    }

    public void setIsEntered(String isEntered) {
        this.isEntered = isEntered;
    }

    public String getiHavePaid() {
        return iHavePaid;
    }

    public void setiHavePaid(String iHavePaid) {
        this.iHavePaid = iHavePaid;
    }

    public String getiHaveSubmitted() {
        return iHaveSubmitted;
    }

    public void setiHaveSubmitted(String iHaveSubmitted) {
        this.iHaveSubmitted = iHaveSubmitted;
    }

    public String getJoinedChallengersIncludingMe() {
        return joinedChallengersIncludingMe;
    }

    public void setJoinedChallengersIncludingMe(String joinedChallengersIncludingMe) {
        this.joinedChallengersIncludingMe = joinedChallengersIncludingMe;
    }

    public String getUserId__challenger() {
        return UserId__challenger;
    }

    public void setUserId__challenger(String userId__challenger) {
        UserId__challenger = userId__challenger;
    }

    public String getName_challenger() {
        return Name_challenger;
    }

    public void setName_challenger(String name_challenger) {
        Name_challenger = name_challenger;
    }

    public String getImage_challenger() {
        return Image_challenger;
    }

    public void setImage_challenger(String image_challenger) {
        Image_challenger = image_challenger;
    }

    public String getThumb_challenger() {
        return Thumb_challenger;
    }

    public void setThumb_challenger(String thumb_challenger) {
        Thumb_challenger = thumb_challenger;
    }

    public String getVideo_challenger() {
        return Video_challenger;
    }

    public void setVideo_challenger(String video_challenger) {
        Video_challenger = video_challenger;
    }

    public String getVotes() {
        return Votes;
    }

    public void setVotes(String votes) {
        Votes = votes;
    }

    public ArrayList<UserModel> getChallenge_users_list() {
        return challenge_users_list;
    }

    public void setChallenge_users_list(ArrayList<UserModel> challenge_users_list) {
        this.challenge_users_list = challenge_users_list;
    }

    public ArrayList<ChallengeUserModel> getChallengeRoundChallengerPairs() {
        return challengeRoundChallengerPairs;
    }

    public void setChallengeRoundChallengerPairs(ArrayList<ChallengeUserModel> challengeRoundChallengerPairs) {
        this.challengeRoundChallengerPairs = challengeRoundChallengerPairs;
    }

    public ArrayList<ChallengeUserModel> getChallengeRoundChallengerPairsTwo() {
        return challengeRoundChallengerPairsTwo;
    }

    public void setChallengeRoundChallengerPairsTwo(ArrayList<ChallengeUserModel> challengeRoundChallengerPairsTwo) {
        this.challengeRoundChallengerPairsTwo = challengeRoundChallengerPairsTwo;
    }

    public ArrayList<ChallengeUserModel> getChallengeRoundChallengerPairsThree() {
        return challengeRoundChallengerPairsThree;
    }

    public void setChallengeRoundChallengerPairsThree(ArrayList<ChallengeUserModel> challengeRoundChallengerPairsThree) {
        this.challengeRoundChallengerPairsThree = challengeRoundChallengerPairsThree;
    }

    public ArrayList<ChallengeUserModel> getChallengeRoundChallengerPairsFour() {
        return challengeRoundChallengerPairsFour;
    }

    public void setChallengeRoundChallengerPairsFour(ArrayList<ChallengeUserModel> challengeRoundChallengerPairsFour) {
        this.challengeRoundChallengerPairsFour = challengeRoundChallengerPairsFour;
    }

    public ArrayList<ChallengeUserModel> getChallengeRoundChallengerPairsFive() {
        return challengeRoundChallengerPairsFive;
    }

    public void setChallengeRoundChallengerPairsFive(ArrayList<ChallengeUserModel> challengeRoundChallengerPairsFive) {
        this.challengeRoundChallengerPairsFive = challengeRoundChallengerPairsFive;
    }

    public ArrayList<ChallengeUserModel> getChallengeRoundChallengerPairsSix() {
        return challengeRoundChallengerPairsSix;
    }

    public void setChallengeRoundChallengerPairsSix(ArrayList<ChallengeUserModel> challengeRoundChallengerPairsSix) {
        this.challengeRoundChallengerPairsSix = challengeRoundChallengerPairsSix;
    }


}
