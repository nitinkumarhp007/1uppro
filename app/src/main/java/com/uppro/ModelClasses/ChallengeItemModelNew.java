package com.uppro.ModelClasses;

public class ChallengeItemModelNew {
    public String created;
    public Integer id;
    public Integer challengeId;
    public Integer round;
    public Integer roundWinnerId;
    public Integer contender1Id;
    public Integer contender2Id;
    public String createdAt;
    public String updatedAt;
    public String contender1Video;
    public String contender2Video;
    public String contender1VideoThumbnail;
    public String contender2VideoThumbnail;
    public Integer votesContender1;
    public Integer votesContender2;
    public Integer isVotedContender1;
    public Integer isVotedContender2;
    public Integer isVotedCreatorInLast24HoursContender1;
    public Integer isVotedCreatorInLast24HoursContender2;
    public Integer isVotedChallengerInLast24HoursContender1;
    public Integer isVotedChallengerInLast24HoursContender2;
    public Contender1Bean contender1;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(Integer challengeId) {
        this.challengeId = challengeId;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }

    public Integer getRoundWinnerId() {
        return roundWinnerId;
    }

    public void setRoundWinnerId(Integer roundWinnerId) {
        this.roundWinnerId = roundWinnerId;
    }

    public Integer getContender1Id() {
        return contender1Id;
    }

    public void setContender1Id(Integer contender1Id) {
        this.contender1Id = contender1Id;
    }

    public Integer getContender2Id() {
        return contender2Id;
    }

    public void setContender2Id(Integer contender2Id) {
        this.contender2Id = contender2Id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getContender1Video() {
        return contender1Video;
    }

    public void setContender1Video(String contender1Video) {
        this.contender1Video = contender1Video;
    }

    public String getContender2Video() {
        return contender2Video;
    }

    public void setContender2Video(String contender2Video) {
        this.contender2Video = contender2Video;
    }

    public String getContender1VideoThumbnail() {
        return contender1VideoThumbnail;
    }

    public void setContender1VideoThumbnail(String contender1VideoThumbnail) {
        this.contender1VideoThumbnail = contender1VideoThumbnail;
    }

    public String getContender2VideoThumbnail() {
        return contender2VideoThumbnail;
    }

    public void setContender2VideoThumbnail(String contender2VideoThumbnail) {
        this.contender2VideoThumbnail = contender2VideoThumbnail;
    }

    public Integer getVotesContender1() {
        return votesContender1;
    }

    public void setVotesContender1(Integer votesContender1) {
        this.votesContender1 = votesContender1;
    }

    public Integer getVotesContender2() {
        return votesContender2;
    }

    public void setVotesContender2(Integer votesContender2) {
        this.votesContender2 = votesContender2;
    }

    public Integer getIsVotedContender1() {
        return isVotedContender1;
    }

    public void setIsVotedContender1(Integer isVotedContender1) {
        this.isVotedContender1 = isVotedContender1;
    }

    public Integer getIsVotedContender2() {
        return isVotedContender2;
    }

    public void setIsVotedContender2(Integer isVotedContender2) {
        this.isVotedContender2 = isVotedContender2;
    }

    public Integer getIsVotedCreatorInLast24HoursContender1() {
        return isVotedCreatorInLast24HoursContender1;
    }

    public void setIsVotedCreatorInLast24HoursContender1(Integer isVotedCreatorInLast24HoursContender1) {
        this.isVotedCreatorInLast24HoursContender1 = isVotedCreatorInLast24HoursContender1;
    }

    public Integer getIsVotedCreatorInLast24HoursContender2() {
        return isVotedCreatorInLast24HoursContender2;
    }

    public void setIsVotedCreatorInLast24HoursContender2(Integer isVotedCreatorInLast24HoursContender2) {
        this.isVotedCreatorInLast24HoursContender2 = isVotedCreatorInLast24HoursContender2;
    }

    public Integer getIsVotedChallengerInLast24HoursContender1() {
        return isVotedChallengerInLast24HoursContender1;
    }

    public void setIsVotedChallengerInLast24HoursContender1(Integer isVotedChallengerInLast24HoursContender1) {
        this.isVotedChallengerInLast24HoursContender1 = isVotedChallengerInLast24HoursContender1;
    }

    public Integer getIsVotedChallengerInLast24HoursContender2() {
        return isVotedChallengerInLast24HoursContender2;
    }

    public void setIsVotedChallengerInLast24HoursContender2(Integer isVotedChallengerInLast24HoursContender2) {
        this.isVotedChallengerInLast24HoursContender2 = isVotedChallengerInLast24HoursContender2;
    }

    public Contender1Bean getContender1() {
        return contender1;
    }

    public void setContender1(Contender1Bean contender1) {
        this.contender1 = contender1;
    }

    public Contender2Bean getContender2() {
        return contender2;
    }

    public void setContender2(Contender2Bean contender2) {
        this.contender2 = contender2;
    }

    public Contender2Bean contender2;

    public static class Contender1Bean {
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(Integer deviceType) {
            this.deviceType = deviceType;
        }

        public Integer getWins() {
            return wins;
        }

        public void setWins(Integer wins) {
            this.wins = wins;
        }

        public Integer getLevel() {
            return level;
        }

        public void setLevel(Integer level) {
            this.level = level;
        }


        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public Integer id;
        public String name;
        public String email;
        public String username;
        public String image;
        public Integer deviceType;
        public Integer wins;
        public Integer level;
        public String deviceToken;
    }

    public static class Contender2Bean {
        public Integer id;
        public String name;
        public String email;
        public String username;
        public String image;
        public Integer deviceType;
        public String deviceToken;
        public Integer wins;
        public Integer level;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(Integer deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public Integer getWins() {
            return wins;
        }

        public void setWins(Integer wins) {
            this.wins = wins;
        }

        public Integer getLevel() {
            return level;
        }

        public void setLevel(Integer level) {
            this.level = level;
        }


    }
}
