package com.uppro.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class WalletModel implements Parcelable {
    String id="";
    String amuntchangeType="";
    String amountchange="";
    String current_amount="";
    String message="";

    public WalletModel()
    {}

    protected WalletModel(Parcel in) {
        id = in.readString();
        amuntchangeType = in.readString();
        amountchange = in.readString();
        current_amount = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(amuntchangeType);
        dest.writeString(amountchange);
        dest.writeString(current_amount);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WalletModel> CREATOR = new Creator<WalletModel>() {
        @Override
        public WalletModel createFromParcel(Parcel in) {
            return new WalletModel(in);
        }

        @Override
        public WalletModel[] newArray(int size) {
            return new WalletModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmuntchangeType() {
        return amuntchangeType;
    }

    public void setAmuntchangeType(String amuntchangeType) {
        this.amuntchangeType = amuntchangeType;
    }

    public String getAmountchange() {
        return amountchange;
    }

    public void setAmountchange(String amountchange) {
        this.amountchange = amountchange;
    }

    public String getCurrent_amount() {
        return current_amount;
    }

    public void setCurrent_amount(String current_amount) {
        this.current_amount = current_amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
