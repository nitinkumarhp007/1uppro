package com.uppro.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class ChallengeListModel implements Parcelable {
    String id = "";
    String type = "";
    String isRules = "";
    String categoryId = "";
    String competitors = "";
    String time = "";
    String video = "";
    boolean isPlaying = false;
    String rules = "";
    String category_name = "";
    String thumb = "";
    String start_date = "";
    String end_date = "";
    String name = "";
    String name_challenger = "";
    String image = "";
    String image_challenger = "";
    String userId = "";
    String userId__challenger = "";
    String created = "";
    String votestocreator = "";
    String votestochallenger = "";
    String isvoted = "";
    String thumb_challenger = "";
    String video_challenger = "";
    String startDate = "";
    String endDate = "";
    String timeLeft = "";

    String days = "";
    String hours = "";
    String minutes = "";
    String seconds = "";
    String comment_count = "";

    public ChallengeListModel() {
    }

    protected ChallengeListModel(Parcel in) {
        id = in.readString();
        type = in.readString();
        isRules = in.readString();
        categoryId = in.readString();
        competitors = in.readString();
        time = in.readString();
        video = in.readString();
        rules = in.readString();
        category_name = in.readString();
        thumb = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        name = in.readString();
        name_challenger = in.readString();
        image = in.readString();
        image_challenger = in.readString();
        userId = in.readString();
        userId__challenger = in.readString();
        created = in.readString();
        votestocreator = in.readString();
        votestochallenger = in.readString();
        isvoted = in.readString();
        thumb_challenger = in.readString();
        video_challenger = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        timeLeft = in.readString();
        days = in.readString();
        hours = in.readString();
        minutes = in.readString();
        seconds = in.readString();
        comment_count = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(type);
        dest.writeString(isRules);
        dest.writeString(categoryId);
        dest.writeString(competitors);
        dest.writeString(time);
        dest.writeString(video);
        dest.writeString(rules);
        dest.writeString(category_name);
        dest.writeString(thumb);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeString(name);
        dest.writeString(name_challenger);
        dest.writeString(image);
        dest.writeString(image_challenger);
        dest.writeString(userId);
        dest.writeString(userId__challenger);
        dest.writeString(created);
        dest.writeString(votestocreator);
        dest.writeString(votestochallenger);
        dest.writeString(isvoted);
        dest.writeString(thumb_challenger);
        dest.writeString(video_challenger);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(timeLeft);
        dest.writeString(days);
        dest.writeString(hours);
        dest.writeString(minutes);
        dest.writeString(seconds);
        dest.writeString(comment_count);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChallengeListModel> CREATOR = new Creator<ChallengeListModel>() {
        @Override
        public ChallengeListModel createFromParcel(Parcel in) {
            return new ChallengeListModel(in);
        }

        @Override
        public ChallengeListModel[] newArray(int size) {
            return new ChallengeListModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsRules() {
        return isRules;
    }

    public void setIsRules(String isRules) {
        this.isRules = isRules;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCompetitors() {
        return competitors;
    }

    public void setCompetitors(String competitors) {
        this.competitors = competitors;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVideo() {
        return video;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setVideo(String video) {
        this.video = video;
    }
    public void setIsPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_challenger() {
        return name_challenger;
    }

    public void setName_challenger(String name_challenger) {
        this.name_challenger = name_challenger;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_challenger() {
        return image_challenger;
    }

    public void setImage_challenger(String image_challenger) {
        this.image_challenger = image_challenger;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId__challenger() {
        return userId__challenger;
    }

    public void setUserId__challenger(String userId__challenger) {
        this.userId__challenger = userId__challenger;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getVotestocreator() {
        return votestocreator;
    }

    public void setVotestocreator(String votestocreator) {
        this.votestocreator = votestocreator;
    }

    public String getVotestochallenger() {
        return votestochallenger;
    }

    public void setVotestochallenger(String votestochallenger) {
        this.votestochallenger = votestochallenger;
    }

    public String getIsvoted() {
        return isvoted;
    }

    public void setIsvoted(String isvoted) {
        this.isvoted = isvoted;
    }

    public String getThumb_challenger() {
        return thumb_challenger;
    }

    public void setThumb_challenger(String thumb_challenger) {
        this.thumb_challenger = thumb_challenger;
    }

    public String getVideo_challenger() {
        return video_challenger;
    }

    public void setVideo_challenger(String video_challenger) {
        this.video_challenger = video_challenger;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getSeconds() {
        return seconds;
    }

    public void setSeconds(String seconds) {
        this.seconds = seconds;
    }


}
