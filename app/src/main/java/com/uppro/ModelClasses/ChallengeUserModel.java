package com.uppro.ModelClasses;


import android.os.Parcel;
import android.os.Parcelable;

public class ChallengeUserModel implements Parcelable {

    public ChallengeUserModel() {

    }

    public String getUser_id_one() {
        return user_id_one;
    }

    public void setUser_id_one(String user_id_one) {
        this.user_id_one = user_id_one;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    String rules="";
    String category="";
    String categoryid="";
    String startdate="";

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    String enddate="";
    String user_id_one="";
    String name_one="";
    String email_one="";
    String profile_pic_one="";
    String video_url_one="";
    String video_thamb_one="";
    String comment_count_one="";

    String user_id_two="";
    String name_two="";
    String email_two="";
    String profile_pic_two="";
    String video_url_two="";
    String video_thamb_two="";
    String comment_count_two="";


    String challengeId="";
    String round="";
    String roundWinnerId="";
    String votesContender1="";
    String votesContender2="";
    String isVotedContender1="";
    String isVotedContender2="";
    String isVotedCreatorInLast24HoursContender1="";
    String isVotedCreatorInLast24HoursContender2="";
    String isVotedChallengerInLast24HoursContender1="";


    String isVotedChallengerInLast24HoursContender2="";

    String winsone="";
    String levelone="";
    String winstwo="";
    String leveltwo="";


    protected ChallengeUserModel(Parcel in) {
        rules = in.readString();
        category = in.readString();
        categoryid = in.readString();
        startdate = in.readString();
        enddate = in.readString();
        user_id_one = in.readString();
        name_one = in.readString();
        email_one = in.readString();
        profile_pic_one = in.readString();
        video_url_one = in.readString();
        video_thamb_one = in.readString();
        comment_count_one = in.readString();
        user_id_two = in.readString();
        name_two = in.readString();
        email_two = in.readString();
        profile_pic_two = in.readString();
        video_url_two = in.readString();
        video_thamb_two = in.readString();
        comment_count_two = in.readString();
        challengeId = in.readString();
        round = in.readString();
        roundWinnerId = in.readString();
        votesContender1 = in.readString();
        votesContender2 = in.readString();
        isVotedContender1 = in.readString();
        isVotedContender2 = in.readString();
        isVotedCreatorInLast24HoursContender1 = in.readString();
        isVotedCreatorInLast24HoursContender2 = in.readString();
        isVotedChallengerInLast24HoursContender1 = in.readString();
        isVotedChallengerInLast24HoursContender2 = in.readString();
        winsone = in.readString();
        levelone = in.readString();
        winstwo = in.readString();
        leveltwo = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rules);
        dest.writeString(category);
        dest.writeString(categoryid);
        dest.writeString(startdate);
        dest.writeString(enddate);
        dest.writeString(user_id_one);
        dest.writeString(name_one);
        dest.writeString(email_one);
        dest.writeString(profile_pic_one);
        dest.writeString(video_url_one);
        dest.writeString(video_thamb_one);
        dest.writeString(comment_count_one);
        dest.writeString(user_id_two);
        dest.writeString(name_two);
        dest.writeString(email_two);
        dest.writeString(profile_pic_two);
        dest.writeString(video_url_two);
        dest.writeString(video_thamb_two);
        dest.writeString(comment_count_two);
        dest.writeString(challengeId);
        dest.writeString(round);
        dest.writeString(roundWinnerId);
        dest.writeString(votesContender1);
        dest.writeString(votesContender2);
        dest.writeString(isVotedContender1);
        dest.writeString(isVotedContender2);
        dest.writeString(isVotedCreatorInLast24HoursContender1);
        dest.writeString(isVotedCreatorInLast24HoursContender2);
        dest.writeString(isVotedChallengerInLast24HoursContender1);
        dest.writeString(isVotedChallengerInLast24HoursContender2);
        dest.writeString(winsone);
        dest.writeString(levelone);
        dest.writeString(winstwo);
        dest.writeString(leveltwo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChallengeUserModel> CREATOR = new Creator<ChallengeUserModel>() {
        @Override
        public ChallengeUserModel createFromParcel(Parcel in) {
            return new ChallengeUserModel(in);
        }

        @Override
        public ChallengeUserModel[] newArray(int size) {
            return new ChallengeUserModel[size];
        }
    };

    public String getName_one() {
        return name_one;
    }

    public void setName_one(String name_one) {
        this.name_one = name_one;
    }

    public String getEmail_one() {
        return email_one;
    }

    public void setEmail_one(String email_one) {
        this.email_one = email_one;
    }

    public String getProfile_pic_one() {
        return profile_pic_one;
    }

    public void setProfile_pic_one(String profile_pic_one) {
        this.profile_pic_one = profile_pic_one;
    }

    public String getVideo_url_one() {
        return video_url_one;
    }

    public void setVideo_url_one(String video_url_one) {
        this.video_url_one = video_url_one;
    }

    public String getVideo_thamb_one() {
        return video_thamb_one;
    }

    public void setVideo_thamb_one(String video_thamb_one) {
        this.video_thamb_one = video_thamb_one;
    }

    public String getComment_count_one() {
        return comment_count_one;
    }

    public void setComment_count_one(String comment_count_one) {
        this.comment_count_one = comment_count_one;
    }

    public String getUser_id_two() {
        return user_id_two;
    }

    public void setUser_id_two(String user_id_two) {
        this.user_id_two = user_id_two;
    }

    public String getName_two() {
        return name_two;
    }

    public void setName_two(String name_two) {
        this.name_two = name_two;
    }

    public String getEmail_two() {
        return email_two;
    }

    public void setEmail_two(String email_two) {
        this.email_two = email_two;
    }

    public String getProfile_pic_two() {
        return profile_pic_two;
    }

    public void setProfile_pic_two(String profile_pic_two) {
        this.profile_pic_two = profile_pic_two;
    }

    public String getVideo_url_two() {
        return video_url_two;
    }

    public void setVideo_url_two(String video_url_two) {
        this.video_url_two = video_url_two;
    }

    public String getVideo_thamb_two() {
        return video_thamb_two;
    }

    public void setVideo_thamb_two(String video_thamb_two) {
        this.video_thamb_two = video_thamb_two;
    }

    public String getComment_count_two() {
        return comment_count_two;
    }

    public void setComment_count_two(String comment_count_two) {
        this.comment_count_two = comment_count_two;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getRoundWinnerId() {
        return roundWinnerId;
    }

    public void setRoundWinnerId(String roundWinnerId) {
        this.roundWinnerId = roundWinnerId;
    }

    public String getVotesContender1() {
        return votesContender1;
    }

    public void setVotesContender1(String votesContender1) {
        this.votesContender1 = votesContender1;
    }

    public String getVotesContender2() {
        return votesContender2;
    }

    public void setVotesContender2(String votesContender2) {
        this.votesContender2 = votesContender2;
    }

    public String getIsVotedContender1() {
        return isVotedContender1;
    }

    public void setIsVotedContender1(String isVotedContender1) {
        this.isVotedContender1 = isVotedContender1;
    }

    public String getIsVotedContender2() {
        return isVotedContender2;
    }

    public void setIsVotedContender2(String isVotedContender2) {
        this.isVotedContender2 = isVotedContender2;
    }

    public String getIsVotedCreatorInLast24HoursContender1() {
        return isVotedCreatorInLast24HoursContender1;
    }

    public void setIsVotedCreatorInLast24HoursContender1(String isVotedCreatorInLast24HoursContender1) {
        this.isVotedCreatorInLast24HoursContender1 = isVotedCreatorInLast24HoursContender1;
    }

    public String getIsVotedCreatorInLast24HoursContender2() {
        return isVotedCreatorInLast24HoursContender2;
    }

    public void setIsVotedCreatorInLast24HoursContender2(String isVotedCreatorInLast24HoursContender2) {
        this.isVotedCreatorInLast24HoursContender2 = isVotedCreatorInLast24HoursContender2;
    }

    public String getIsVotedChallengerInLast24HoursContender1() {
        return isVotedChallengerInLast24HoursContender1;
    }

    public void setIsVotedChallengerInLast24HoursContender1(String isVotedChallengerInLast24HoursContender1) {
        this.isVotedChallengerInLast24HoursContender1 = isVotedChallengerInLast24HoursContender1;
    }

    public String getIsVotedChallengerInLast24HoursContender2() {
        return isVotedChallengerInLast24HoursContender2;
    }

    public void setIsVotedChallengerInLast24HoursContender2(String isVotedChallengerInLast24HoursContender2) {
        this.isVotedChallengerInLast24HoursContender2 = isVotedChallengerInLast24HoursContender2;
    }

    public String getWinsone() {
        return winsone;
    }

    public void setWinsone(String winsone) {
        this.winsone = winsone;
    }

    public String getLevelone() {
        return levelone;
    }

    public void setLevelone(String levelone) {
        this.levelone = levelone;
    }

    public String getWinstwo() {
        return winstwo;
    }

    public void setWinstwo(String winstwo) {
        this.winstwo = winstwo;
    }

    public String getLeveltwo() {
        return leveltwo;
    }

    public void setLeveltwo(String leveltwo) {
        this.leveltwo = leveltwo;
    }


}
