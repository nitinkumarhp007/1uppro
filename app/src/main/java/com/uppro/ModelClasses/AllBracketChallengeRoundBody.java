package com.uppro.ModelClasses;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AllBracketChallengeRoundBody implements Parcelable {

    public final static Creator<AllBracketChallengeRoundBody> CREATOR = new Creator<AllBracketChallengeRoundBody>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AllBracketChallengeRoundBody createFromParcel(android.os.Parcel in) {
            return new AllBracketChallengeRoundBody(in);
        }

        public AllBracketChallengeRoundBody[] newArray(int size) {
            return (new AllBracketChallengeRoundBody[size]);
        }

    };
    @SerializedName("round1")
    @Expose
    private ArrayList<AllBracketChallengeRoundData> round1 = null;
    @SerializedName("round2")
    @Expose
    private ArrayList<AllBracketChallengeRoundData> round2 = null;
    @SerializedName("round3")
    @Expose
    private ArrayList<AllBracketChallengeRoundData> round3 = null;
    @SerializedName("round4")
    @Expose
    private ArrayList<AllBracketChallengeRoundData> round4 = null;
    @SerializedName("round5")
    @Expose
    private ArrayList<AllBracketChallengeRoundData> round5 = null;
    @SerializedName("round6")
    @Expose
    private ArrayList<AllBracketChallengeRoundData> round6 = null;

    protected AllBracketChallengeRoundBody(android.os.Parcel in) {
        in.readList(this.round1, (AllBracketChallengeRoundData.class.getClassLoader()));
        in.readList(this.round2, (AllBracketChallengeRoundData.class.getClassLoader()));
        in.readList(this.round3, (AllBracketChallengeRoundData.class.getClassLoader()));
        in.readList(this.round4, (AllBracketChallengeRoundData.class.getClassLoader()));
        in.readList(this.round5, (AllBracketChallengeRoundData.class.getClassLoader()));
        in.readList(this.round6, (AllBracketChallengeRoundData.class.getClassLoader()));
    }

    public AllBracketChallengeRoundBody() {
    }

    public ArrayList<AllBracketChallengeRoundData> getRound1() {
        return round1;
    }

    public void setRound1(ArrayList<AllBracketChallengeRoundData> round1) {
        this.round1 = round1;
    }

    public ArrayList<AllBracketChallengeRoundData> getRound2() {
        return round2;
    }

    public void setRound2(ArrayList<AllBracketChallengeRoundData> round2) {
        this.round2 = round2;
    }

    public ArrayList<AllBracketChallengeRoundData> getRound3() {
        return round3;
    }

    public void setRound3(ArrayList<AllBracketChallengeRoundData> round3) {
        this.round3 = round3;
    }

    public ArrayList<AllBracketChallengeRoundData> getRound4() {
        return round4;
    }

    public void setRound4(ArrayList<AllBracketChallengeRoundData> round4) {
        this.round4 = round4;
    }

    public ArrayList<AllBracketChallengeRoundData> getRound5() {
        return round5;
    }

    public void setRound5(ArrayList<AllBracketChallengeRoundData> round5) {
        this.round5 = round5;
    }

    public ArrayList<AllBracketChallengeRoundData> getRound6() {
        return round6;
    }

    public void setRound6(ArrayList<AllBracketChallengeRoundData> round6) {
        this.round6 = round6;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeList(round1);
        dest.writeList(round2);
        dest.writeList(round3);
        dest.writeList(round4);
        dest.writeList(round5);
        dest.writeList(round6);
    }

    public int describeContents() {
        return 0;
    }

}
