package com.uppro.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class NotificationModel implements Parcelable {
    String id = "";
    String text = "";
    String created = "";
    String user_id = "";
    String name = "";
    String image = "";
    String type = "";
    String isRead = "";
    String challenge_id = "";
    ChallengeListModel challengeListModel;

    public NotificationModel()
    {}

    protected NotificationModel(Parcel in) {
        id = in.readString();
        text = in.readString();
        created = in.readString();
        user_id = in.readString();
        name = in.readString();
        image = in.readString();
        type = in.readString();
        isRead = in.readString();
        challenge_id = in.readString();
        challengeListModel = in.readParcelable(ChallengeListModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(text);
        dest.writeString(created);
        dest.writeString(user_id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(type);
        dest.writeString(isRead);
        dest.writeString(challenge_id);
        dest.writeParcelable(challengeListModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public ChallengeListModel getChallengeListModel() {
        return challengeListModel;
    }

    public void setChallengeListModel(ChallengeListModel challengeListModel) {
        this.challengeListModel = challengeListModel;
    }
}
