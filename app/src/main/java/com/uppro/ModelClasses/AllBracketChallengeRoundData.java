package com.uppro.ModelClasses;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllBracketChallengeRoundData implements Parcelable {

    public final static Creator<AllBracketChallengeRoundData> CREATOR = new Creator<AllBracketChallengeRoundData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AllBracketChallengeRoundData createFromParcel(android.os.Parcel in) {
            return new AllBracketChallengeRoundData(in);
        }

        public AllBracketChallengeRoundData[] newArray(int size) {
            return (new AllBracketChallengeRoundData[size]);
        }

    };
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("challengeId")
    @Expose
    private Integer challengeId;
    @SerializedName("round")
    @Expose
    private Integer round;
    @SerializedName("roundWinnerId")
    @Expose
    private Integer roundWinnerId;
    @SerializedName("contender1Id")
    @Expose
    private Integer contender1Id;
    @SerializedName("contender2Id")
    @Expose
    private Integer contender2Id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("contender1Video")
    @Expose
    private String contender1Video;
    @SerializedName("contender2Video")
    @Expose
    private String contender2Video;
    @SerializedName("contender1VideoThumbnail")
    @Expose
    private String contender1VideoThumbnail;
    @SerializedName("contender2VideoThumbnail")
    @Expose
    private String contender2VideoThumbnail;
    @SerializedName("votesContender1")
    @Expose
    private Integer votesContender1;
    @SerializedName("votesContender2")
    @Expose
    private Integer votesContender2;
    @SerializedName("isVotedContender1")
    @Expose
    private Integer isVotedContender1;
    @SerializedName("isVotedContender2")
    @Expose
    private Integer isVotedContender2;
    @SerializedName("isVotedCreatorInLast24HoursContender1")
    @Expose
    private Integer isVotedCreatorInLast24HoursContender1;
    @SerializedName("isVotedCreatorInLast24HoursContender2")
    @Expose
    private Integer isVotedCreatorInLast24HoursContender2;
    @SerializedName("isVotedChallengerInLast24HoursContender1")
    @Expose
    private Integer isVotedChallengerInLast24HoursContender1;
    @SerializedName("isVotedChallengerInLast24HoursContender2")
    @Expose
    private Integer isVotedChallengerInLast24HoursContender2;
    @SerializedName("contender1")
    @Expose
    private ChallengeItemModelNew.Contender1Bean contender1;
    @SerializedName("contender2")
    @Expose
    private ChallengeItemModelNew.Contender2Bean contender2;
    @SerializedName("currentRoundExpiryTimestamp")
    @Expose
    private Integer currentRoundExpiryTimestamp;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("startDateTimestamp")
    @Expose
    private Integer startDateTimestamp;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("endDateTimestamp")
    @Expose
    private Integer endDateTimestamp;
    @SerializedName("isRules")
    @Expose
    private Integer isRules;
    @SerializedName("rules")
    @Expose
    private String rules;
    @SerializedName("category")
    @Expose
    private Category category;

    protected AllBracketChallengeRoundData(android.os.Parcel in) {
        this.created = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.challengeId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.round = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.roundWinnerId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contender1Id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contender2Id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.contender1Video = ((String) in.readValue((String.class.getClassLoader())));
        this.contender2Video = ((String) in.readValue((String.class.getClassLoader())));
        this.contender1VideoThumbnail = ((String) in.readValue((String.class.getClassLoader())));
        this.contender2VideoThumbnail = ((String) in.readValue((String.class.getClassLoader())));
        this.votesContender1 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.votesContender2 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isVotedContender1 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isVotedContender2 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isVotedCreatorInLast24HoursContender1 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isVotedCreatorInLast24HoursContender2 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isVotedChallengerInLast24HoursContender1 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isVotedChallengerInLast24HoursContender2 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.contender1 = ((ChallengeItemModelNew.Contender1Bean) in.readValue((ChallengeItemModelNew.Contender1Bean.class.getClassLoader())));
        this.contender2 = ((ChallengeItemModelNew.Contender2Bean) in.readValue((ChallengeItemModelNew.Contender2Bean.class.getClassLoader())));
        this.currentRoundExpiryTimestamp = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.startDate = ((String) in.readValue((String.class.getClassLoader())));
        this.startDateTimestamp = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.endDate = ((String) in.readValue((String.class.getClassLoader())));
        this.endDateTimestamp = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isRules = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.rules = ((String) in.readValue((String.class.getClassLoader())));
        this.category = ((Category) in.readValue((Category.class.getClassLoader())));
    }

    public AllBracketChallengeRoundData() {
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(Integer challengeId) {
        this.challengeId = challengeId;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }

    public Integer getRoundWinnerId() {
        return roundWinnerId;
    }

    public void setRoundWinnerId(Integer roundWinnerId) {
        this.roundWinnerId = roundWinnerId;
    }

    public Integer getContender1Id() {
        return contender1Id;
    }

    public void setContender1Id(Integer contender1Id) {
        this.contender1Id = contender1Id;
    }

    public Integer getContender2Id() {
        return contender2Id;
    }

    public void setContender2Id(Integer contender2Id) {
        this.contender2Id = contender2Id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getContender1Video() {
        return contender1Video;
    }

    public void setContender1Video(String contender1Video) {
        this.contender1Video = contender1Video;
    }

    public String getContender2Video() {
        return contender2Video;
    }

    public void setContender2Video(String contender2Video) {
        this.contender2Video = contender2Video;
    }

    public String getContender1VideoThumbnail() {
        return contender1VideoThumbnail;
    }

    public void setContender1VideoThumbnail(String contender1VideoThumbnail) {
        this.contender1VideoThumbnail = contender1VideoThumbnail;
    }

    public String getContender2VideoThumbnail() {
        return contender2VideoThumbnail;
    }

    public void setContender2VideoThumbnail(String contender2VideoThumbnail) {
        this.contender2VideoThumbnail = contender2VideoThumbnail;
    }

    public Integer getVotesContender1() {
        return votesContender1;
    }

    public void setVotesContender1(Integer votesContender1) {
        this.votesContender1 = votesContender1;
    }

    public Integer getVotesContender2() {
        return votesContender2;
    }

    public void setVotesContender2(Integer votesContender2) {
        this.votesContender2 = votesContender2;
    }

    public Integer getIsVotedContender1() {
        return isVotedContender1;
    }

    public void setIsVotedContender1(Integer isVotedContender1) {
        this.isVotedContender1 = isVotedContender1;
    }

    public Integer getIsVotedContender2() {
        return isVotedContender2;
    }

    public void setIsVotedContender2(Integer isVotedContender2) {
        this.isVotedContender2 = isVotedContender2;
    }

    public Integer getIsVotedCreatorInLast24HoursContender1() {
        return isVotedCreatorInLast24HoursContender1;
    }

    public void setIsVotedCreatorInLast24HoursContender1(Integer isVotedCreatorInLast24HoursContender1) {
        this.isVotedCreatorInLast24HoursContender1 = isVotedCreatorInLast24HoursContender1;
    }

    public Integer getIsVotedCreatorInLast24HoursContender2() {
        return isVotedCreatorInLast24HoursContender2;
    }

    public void setIsVotedCreatorInLast24HoursContender2(Integer isVotedCreatorInLast24HoursContender2) {
        this.isVotedCreatorInLast24HoursContender2 = isVotedCreatorInLast24HoursContender2;
    }

    public Integer getIsVotedChallengerInLast24HoursContender1() {
        return isVotedChallengerInLast24HoursContender1;
    }

    public void setIsVotedChallengerInLast24HoursContender1(Integer isVotedChallengerInLast24HoursContender1) {
        this.isVotedChallengerInLast24HoursContender1 = isVotedChallengerInLast24HoursContender1;
    }

    public Integer getIsVotedChallengerInLast24HoursContender2() {
        return isVotedChallengerInLast24HoursContender2;
    }

    public void setIsVotedChallengerInLast24HoursContender2(Integer isVotedChallengerInLast24HoursContender2) {
        this.isVotedChallengerInLast24HoursContender2 = isVotedChallengerInLast24HoursContender2;
    }

    public ChallengeItemModelNew.Contender1Bean getContender1() {
        return contender1;
    }

    public void setContender1(ChallengeItemModelNew.Contender1Bean contender1) {
        this.contender1 = contender1;
    }

    public ChallengeItemModelNew.Contender2Bean getContender2() {
        return contender2;
    }

    public void setContender2(ChallengeItemModelNew.Contender2Bean contender2) {
        this.contender2 = contender2;
    }

    public Integer getCurrentRoundExpiryTimestamp() {
        return currentRoundExpiryTimestamp;
    }

    public void setCurrentRoundExpiryTimestamp(Integer currentRoundExpiryTimestamp) {
        this.currentRoundExpiryTimestamp = currentRoundExpiryTimestamp;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Integer getStartDateTimestamp() {
        return startDateTimestamp;
    }

    public void setStartDateTimestamp(Integer startDateTimestamp) {
        this.startDateTimestamp = startDateTimestamp;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getEndDateTimestamp() {
        return endDateTimestamp;
    }

    public void setEndDateTimestamp(Integer endDateTimestamp) {
        this.endDateTimestamp = endDateTimestamp;
    }

    public Integer getIsRules() {
        return isRules;
    }

    public void setIsRules(Integer isRules) {
        this.isRules = isRules;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(created);
        dest.writeValue(id);
        dest.writeValue(challengeId);
        dest.writeValue(round);
        dest.writeValue(roundWinnerId);
        dest.writeValue(contender1Id);
        dest.writeValue(contender2Id);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(contender1Video);
        dest.writeValue(contender2Video);
        dest.writeValue(contender1VideoThumbnail);
        dest.writeValue(contender2VideoThumbnail);
        dest.writeValue(votesContender1);
        dest.writeValue(votesContender2);
        dest.writeValue(isVotedContender1);
        dest.writeValue(isVotedContender2);
        dest.writeValue(isVotedCreatorInLast24HoursContender1);
        dest.writeValue(isVotedCreatorInLast24HoursContender2);
        dest.writeValue(isVotedChallengerInLast24HoursContender1);
        dest.writeValue(isVotedChallengerInLast24HoursContender2);
        dest.writeValue(contender1);
        dest.writeValue(contender2);
        dest.writeValue(currentRoundExpiryTimestamp);
        dest.writeValue(startDate);
        dest.writeValue(startDateTimestamp);
        dest.writeValue(endDate);
        dest.writeValue(endDateTimestamp);
        dest.writeValue(isRules);
        dest.writeValue(rules);
        dest.writeValue(category);
    }

    public int describeContents() {
        return 0;
    }

}
