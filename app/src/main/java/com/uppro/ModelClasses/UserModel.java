package com.uppro.ModelClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {
    String user_id="";
    String name="";
    String email="";
    String profile_pic="";
    String video_url="";
    String video_thamb="";
    String comment_count="";

    public UserModel()
    {}

    protected UserModel(Parcel in) {
        user_id = in.readString();
        name = in.readString();
        email = in.readString();
        profile_pic = in.readString();
        video_url = in.readString();
        video_thamb = in.readString();
        comment_count = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(user_id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(profile_pic);
        dest.writeString(video_url);
        dest.writeString(video_thamb);
        dest.writeString(comment_count);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_thamb() {
        return video_thamb;
    }

    public void setVideo_thamb(String video_thamb) {
        this.video_thamb = video_thamb;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }
}


/* @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChallengeUserModel> CREATOR = new Creator<ChallengeUserModel>() {
        @Override
        public ChallengeUserModel createFromParcel(Parcel in) {
            return new ChallengeUserModel(in);
        }

        @Override
        public ChallengeUserModel[] newArray(int size) {
            return new ChallengeUserModel[size];
        }
    };*/