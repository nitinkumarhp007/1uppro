package com.uppro.ModelClasses;

public class ChattingModel
{
    private String message;
    private String chat_type;
    private String opponent_name;
    private String timestamp;
    private String type;
    private String video_thumbnail;
    private String message_id;
    private String image;
    private String user_id;
    private String user_type;
    private String order_id;
    private String product_id;
    private String offer_status;
    private String offer_amount;
    private String product_name;
    private String product_image;
    private String product_owner_id;
    private boolean is_own_message;

    public boolean isIs_own_message() {
        return is_own_message;
    }

    public void setIs_own_message(boolean is_own_message) {
        this.is_own_message = is_own_message;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOffer_status() {
        return offer_status;
    }

    public void setOffer_status(String offer_status) {
        this.offer_status = offer_status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }
    public String getmessage_id() {
        return message_id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public String getProduct_owner_id() {
        return product_owner_id;
    }

    public void setProduct_owner_id(String product_owner_id) {
        this.product_owner_id = product_owner_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getOffer_amount() {
        return offer_amount;
    }

    public void setOffer_amount(String offer_amount) {
        this.offer_amount = offer_amount;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setmessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getOpponent_name() {
        return opponent_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setOpponent_name(String opponent_name) {
        this.opponent_name = opponent_name;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }



    public String getMessage() {
        return message;
    }

    public String getChat_type() {
        return chat_type;
    }

    public void setChat_type(String chat_type) {
        this.chat_type = chat_type;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}