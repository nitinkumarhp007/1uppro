package com.uppro.ModelClasses

class KotlinModelBracketChallengeDetail : ArrayList<KotlinModelBracketChallengeDetailItem>()

data class KotlinModelBracketChallengeDetailItem(
    val category: Category,
    val categoryId: Int,
    val challengeCreator: Any,
    val challengeRoundChallengerPairs: ChallengeRoundChallengerPairs,
    val challengeUsers: List<ChallengeUser>,
    val competitors: Int,
    val created: String,
    val createdAt: String,
    val currentRound: Int,
    val currentRoundExpiryTimestamp: Int,
    val endDate: String,
    val endDateTimestamp: Int,
    val getTimeRemaining: GetTimeRemaining,
    val getTimeRemainingForCurrentRoundToEnd: GetTimeRemainingForCurrentRoundToEnd,
    val iHavePaid: Int,
    val iHaveSubmitted: Int,
    val id: Int,
    val isCancelled: Int,
    val isCancelledTimestamp: Int,
    val isDelayedForMoreVotes: Int,
    val isEnded: Int,
    val isEntered: Int,
    val isPaid: Int,
    val isRules: Int,
    val isVoted: Int,
    val isVotedChallengerInLast24Hours: Int,
    val isVotedCreatorInLast24Hours: Int,
    val joinedChallengersIncludingMe: Int,
    val joiningFee: String,
    val loserId: Int,
    val rules: String,
    val sortTimestamp: Int,
    val startDate: String,
    val startDateTimestamp: Int,
    val status: Int,
    val time: String,
    val timeLeft: String,
    val timeLeftForCurrentRoundToEnd: String,
    val totalRounds: Int,
    val type: Int,
    val updatedAt: String,
    val userId: Int,
    val video: String,
    val videoThumbnail: String,
    val votesToChallenger: Int,
    val votesToCreator: Int,
    val winnerId: Int
)

data class Category(
    val id: Int,
    val image: String,
    val name: String
)

data class ChallengeRoundChallengerPairs(
    val round1: List<Round1>,
    val round2: List<Any>,
    val round3: List<Any>
)

data class ChallengeUser(
    val challengeCreatorId: Int,
    val challengeId: Int,
    val challenger: Challenger,
    val challengerId: Int,
    val created: String,
    val createdAt: String,
    val id: Int,
    val isPaid: Int,
    val isVotedInLast24Hours: Int,
    val lastVoteTimestamp: Long,
    val status: Int,
    val updatedAt: String,
    val video: String,
    val videoThumbnail: String,
    val videoUploadedAt: Int,
    val votes: Int
)

data class GetTimeRemaining(
    val days: Int,
    val hours: Int,
    val minutes: Int,
    val seconds: Int
)

data class GetTimeRemainingForCurrentRoundToEnd(
    val days: Int,
    val hours: Int,
    val minutes: Int,
    val seconds: Int
)

data class Round1(
    val challengeId: Int,
    val contender1: Contender1,
    val contender1Id: Int,
    val contender1Video: String,
    val contender1VideoThumbnail: String,
    val contender2: Contender2,
    val contender2Id: Int,
    val contender2Video: String,
    val contender2VideoThumbnail: String,
    val created: String,
    val createdAt: String,
    val id: Int,
    val isVotedChallengerInLast24HoursContender1: Int,
    val isVotedChallengerInLast24HoursContender2: Int,
    val isVotedContender1: Int,
    val isVotedContender2: Int,
    val isVotedCreatorInLast24HoursContender1: Int,
    val isVotedCreatorInLast24HoursContender2: Int,
    val round: Int,
    val roundWinnerId: Int,
    val updatedAt: String,
    val votesContender1: Int,
    val votesContender2: Int
)

data class Contender1(
    val deviceToken: String,
    val deviceType: Int,
    val email: String,
    val id: Int,
    val image: String,
    val name: String,
    val username: String
)

data class Contender2(
    val deviceToken: String,
    val deviceType: Int,
    val email: String,
    val id: Int,
    val image: String,
    val name: String,
    val username: String
)

data class Challenger(
    val deviceToken: String,
    val deviceType: Int,
    val email: String,
    val id: Int,
    val image: String,
    val name: String,
    val username: String
)