package com.customer.service.valetcarwash.retrofit

interface HttpRequests {

    //stripe methods

//    @FormUrlEncoded
//    @POST("v1/customers")
//    fun createCustomer(@Field("description") description: String): Call<CreateCustomerEntity>
//
//    @FormUrlEncoded
//    @POST("/v1/customers/{CUSTOMER_ID}/sources")
//    fun createCard(
//            @Path("CUSTOMER_ID") cutsomer_id: String,
//            @Field("source") sources: String
//    ): Call<CreateCardModel>
//
//    @FormUrlEncoded
//    @POST("/v1/customers/{CUSTOMER_ID}")
//    fun updateCustomer(
//            @Path("CUSTOMER_ID") cutsomer_id: String,
//            @Field("default_source") default_source: String
//    ): Call<CreateCustomerEntity>
//
//    @GET("/v1/customers/{CUSTOMER_ID}")
//    fun getCustomerInfo(@Path("CUSTOMER_ID") cutsomer_id: String): Call<CreateCustomerEntity>
//
//    @GET("/v1/customers/{ID}/sources")
//    fun getCards(
//            @Path("ID") ID: String,
//            @Query("object") sources: String = "card"
//    ): Call<GetCardsListModel>
//
//    @DELETE("/v1/customers/{CUSTOMER_ID}/sources/{CARD_ID}")
//    fun deleteCard(
//            @Path("CUSTOMER_ID") cutsomer_id: String,
//            @Path("CARD_ID") card_id: String
//    ): Call<DeletedCardEntity>
//
//    @POST("home/placeOrder")
//    fun placeOrder(@Body data: JsonObject): Call<CheckoutModel>
//
//    @FormUrlEncoded
//    @POST("home/myOrders")
//    fun getOrders(
//            @Header("Authorization") token: String,
//            @FieldMap data: HashMap<String, String>
//    ): Call<OrderStatusModel>
//
//    @FormUrlEncoded
//    @POST("home/orderDetails")
//    fun getOrderDetails(@Field("orderId") orderId: String): Call<OrderDetailsResponse>
//
//    @FormUrlEncoded
//    @POST("home/rateOrder")
//    fun rateProvider(@FieldMap data: HashMap<String, String>): Call<OrderDetailsResponse>
//
//    @FormUrlEncoded
//    @POST("order/bookingDetails")
//    fun bookingDetails(@Field("orderId") orderId: String): Call<BookingDetailEntity>
//
//    @FormUrlEncoded
//    @POST("order/updateBookingStatus")
//    fun updateBookingStatus(
//            @Field("orderId") orderId: String,
//            @Field("status") status: String
//    ): Call<OrderDetailsResponse>
//
//    @FormUrlEncoded
//    @POST("order/updateItemBookingStatus")
//    fun updateItemBookingStatus(
//            @Field("orderId") orderId: String,
//            @Field("status") status: String
//    ): Call<OrderDetailsResponse>
//
//    @FormUrlEncoded
//    @POST("order/itemBookingDetails")
//    fun itemBookingDetails(@Field("orderId") orderId: String): Call<ItemBookingDetailEntity>
//
//    @FormUrlEncoded
//    @POST("order/myBookings")
//    fun myBookings(
//            @Field("type") type: String,
//            @Field("page") page: String,
//            @Field("resPerPage") resPerPage: String = "10"
//    ): Call<BookingEntity>
//
//    @FormUrlEncoded
//    @POST("order/myItemBookings")
//    fun myItemBookings(
//            @Field("type") type: String,
//            @Field("page") page: String,
//            @Field("resPerPage") resPerPage: String = "10"
//    ): Call<BookingEntity>
//
//    @FormUrlEncoded
//    @POST("order/sellingItemListing")
//    fun getItems(
//            @Field("page") page: String,
//            @Field("resPerPage") resPerPage: String = "10"
//    ): Call<ItemsListResponse>
//
//    @FormUrlEncoded
//    @POST("home/modifyOrder")
//    fun modifyOrder(@FieldMap data: HashMap<String, String>): Call<OrderDetailsResponse>
//
//    @FormUrlEncoded
//    @POST("order/itemSummary")
//    fun getItemSummary(@Field("itemId") itemId: String): Call<AddItemResponse>
//
//    @FormUrlEncoded
//    @POST("order/deleteItem")
//    fun deleteItem(@Field("itemId") itemId: String): Call<NotifyData>
//
//    @FormUrlEncoded
//    @POST("order/itemStatus")
//    fun enableDisableItem(@FieldMap data: HashMap<String, String>): Call<ChangeItemStatusRes>
//
//    @Multipart
//    @POST("order/addItem")
//    fun addItem(@PartMap data: HashMap<String, RequestBody>): Call<AddItemResponse>
//
//    @Multipart
//    @POST("order/editItem")
//    fun editItemPartApi(@PartMap data: HashMap<String, @JvmSuppressWildcards RequestBody>): Call<ChangeItemStatusRes>
//
//    @FormUrlEncoded
//    @POST("order/editItem")
//    fun editItemApi(@FieldMap data: HashMap<String, String>): Call<ChangeItemStatusRes>
//
//    @FormUrlEncoded
//    @POST("home/itemDetails")
//    fun itemDetailsApi(@Field("itemId") itemId: String): Call<ItemDetailEntity>
//
//    @FormUrlEncoded
//    @POST("home/addItemToCart")
//    fun addItemToCart(
//            @Field("itemId") itemId: String,
//            @Field("quantity") quantity: String
//    ): Call<UpdateCartEntity>
//
//    @FormUrlEncoded
//    @POST("home/getSellingItems")
//    fun getItemsCustomer(@FieldMap data: HashMap<String, String>): Call<CustomerItemsResponse>
//
//    @POST("home/placeItemOrder")
//    fun placeItemOrder(@Body data: JsonObject): Call<ItemOrderPlacedEntity>
//
//    @GET("home/getCartItem")
//    fun getCartItem(): Call<ItemCartEntity>
//
//    @GET("payment/createAccount")
//    fun getAccount(): Call<GetAccountResponse>
//
//    @GET("payment/returnURL/{id}")
//    fun getAccountStatus(@Path("id") id: String): Call<GetAccountResponse>
//
//    @GET("homeProvider/dashboard")
//    fun dashboard(): Call<DashboardEntity>
//
//    @GET("homeProvider/dashboardUpcomingOrder")
//    fun dashboardUpcomingOrder(): Call<DashboardUpcomingEntity>
//
//    @FormUrlEncoded
//    @POST("home/myItemOrders")
//    fun getOrderedItems(@FieldMap data: HashMap<String, String>): Call<OrderedItemsListRes>
//
//    @FormUrlEncoded
//    @POST("homeProvider/updateOnlineStatus")
//    fun updateOnlineStatus(@Field("status") status: String): Call<ProviderOnlineEntity>
//
//    @FormUrlEncoded
//    @POST("home/itemOrderDetails")
//    fun getOrderedItemDetails(@Field("orderId") orderId: String): Call<OrderedItemDetailResponse>
//
//    @FormUrlEncoded
//    @POST("home/rateSellingOrder")
//    fun rateItem(@FieldMap data: HashMap<String, String>): Call<OrderedItemDetailResponse>
//
//    @FormUrlEncoded
//    @POST("auth/deleteAccount")
//    fun deleteCustomerAccount(@Field("cause") reason: String): Call<NotifyData>
//
//    @FormUrlEncoded
//    @POST("authProvider/deleteAccount")
//    fun deleteProviderAccount(@Field("cause") reason: String): Call<NotifyData>
//
//    @FormUrlEncoded
//    @POST("home/getUpdatedCartItem")
//    fun itemReorderCart(@Field("orderId") order_id: String): Call<ItemReorderResponse>
//
//    @GET("paymentAndRefundPolicy")
//    fun getPayRefundPolicy(@Header("Authorization") token: String): Call<PageResponse>
//
//    @GET("termsAndConditionPolicy")
//    fun getTCPolicy(@Header("Authorization") token: String): Call<PageResponse>
}