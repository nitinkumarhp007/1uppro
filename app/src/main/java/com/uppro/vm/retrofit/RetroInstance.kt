package com.customer.service.valetcarwash.retrofit

//import com.customer.service.valetcarwash.util.StripeConfig
import android.os.Build
import com.uppro.BuildConfig
import com.uppro.parser.AllAPIS
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetroInstance {

    var retroInstance: Retrofit? = null
    var placeRetroInstance: Retrofit? = null
    var stripeIns: Retrofit? = null

    fun getInstance(): Retrofit {
        if (retroInstance == null) {
            var versionCode: Int = BuildConfig.VERSION_CODE
            var versionName: String = BuildConfig.VERSION_NAME
            var release = Build.VERSION.RELEASE
            var sdkVersion = Build.VERSION.SDK_INT
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val httpClient = OkHttpClient.Builder()
//            httpClient.addInterceptor { chain ->
//                val original = chain.request()
//                val request = original.newBuilder()
//                        .addHeader("deviceType", "android")
//                        .addHeader("deviceModel", release)
//                        .addHeader("appVersion", "")
//                        .addHeader("osVersion", sdkVersion.toString())
//                        .method(original.method, original.body)
//                        .build()
//                chain.proceed(request)
//            }

            httpClient.interceptors().add(httpLoggingInterceptor)
            httpClient.readTimeout(60, TimeUnit.SECONDS)
            httpClient.connectTimeout(60, TimeUnit.SECONDS)

            val client = httpClient.build()
            retroInstance = Retrofit.Builder()
                .baseUrl(AllAPIS.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()

            return retroInstance as Retrofit
        } else
            return retroInstance as Retrofit
    }

    fun withHeaders(token: String): Retrofit {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        }

        httpClient.interceptors().add(httpLoggingInterceptor)
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)

        val client = httpClient.build()
        return Retrofit.Builder()
            .baseUrl(AllAPIS.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
    }

    fun getPlaceInstance(): Retrofit {
        if (placeRetroInstance == null) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val httpClient = OkHttpClient.Builder()
            httpClient.interceptors().add(httpLoggingInterceptor)
            httpClient.readTimeout(60, TimeUnit.SECONDS)
            httpClient.connectTimeout(60, TimeUnit.SECONDS)

            val client = httpClient.build()
            placeRetroInstance = Retrofit.Builder()
                .baseUrl(AllAPIS.PLACE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
            return placeRetroInstance as Retrofit
        } else
            return placeRetroInstance as Retrofit
    }

    fun stripeInstance(): Retrofit {
        if (stripeIns == null) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()
                    .addHeader("Authorization", "Bearer ${"StripeConfig.SECRET_KEY"}")
                    .method(original.method, original.body)
                    .build()
                chain.proceed(request)
            }
            httpClient.interceptors().add(httpLoggingInterceptor)
            httpClient.readTimeout(60, TimeUnit.SECONDS)
            httpClient.connectTimeout(60, TimeUnit.SECONDS)

            val client = httpClient.build()
            stripeIns = Retrofit.Builder()
                .baseUrl(AllAPIS.STRIPE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
            return stripeIns as Retrofit
        } else
            return stripeIns as Retrofit
    }

}