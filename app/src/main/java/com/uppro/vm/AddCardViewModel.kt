package com.uppro.vm

import androidx.lifecycle.ViewModel

class AddCardViewModel : ViewModel() {

//    var mCreateCardModel = MutableLiveData<CreateCardModel>()
//    var mCreateCustomerEntity = MutableLiveData<CreateCustomerEntity>()
//    var mUpdatedCustomerEntity = MutableLiveData<Pair<Int,CreateCustomerEntity>>()
//    var mUpdatedCusOnServer = MutableLiveData<CustUpdatedProfileData>()
//    var mGetCardsListModel = MutableLiveData<GetCardsListModel>()
//    var mDeleteCardsModel = MutableLiveData<Pair<Int,DeletedCardEntity>>()
//    var mErrorObserver = MutableLiveData<NotifyData>()
//    val orderObserver = MutableLiveData<CheckoutModel>()
//    val mItemOrderPlacedEntity = MutableLiveData<ItemOrderPlacedEntity>()


//    fun createCustomer(userid: String) {
//        RetroInstance().stripeInstance().create(HttpRequests::class.java)
//            .createCustomer("created customer for $userid")
//            .enqueue(object : Callback<CreateCustomerEntity> {
//                override fun onFailure(call: Call<CreateCustomerEntity>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<CreateCustomerEntity>,
//                    response: Response<CreateCustomerEntity>
//                ) {
//                    if (response.isSuccessful) {
//                        mCreateCustomerEntity.value = response.body()!!
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun editCustomer(user_token: String, data: HashMap<String, String>) {
//        RetroInstance().withHeaders(user_token).create(HttpRequests::class.java)
//            .updateCustomerProfile(data).enqueue(object : Callback<CustUpdatedProfileData> {
//                override fun onFailure(call: Call<CustUpdatedProfileData>, t: Throwable) {
//                    mErrorObserver.value = NotifyData(status = 500, message = t.message!!)
//                }
//
//                override fun onResponse(
//                    call: Call<CustUpdatedProfileData>,
//                    response: Response<CustUpdatedProfileData>
//                ) {
//                    if (response.isSuccessful)
//                        mUpdatedCusOnServer.value = response.body()!!
//                    else
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                }
//            })
//    }
//
//    fun createCard(stripeCustomer: String, token: String) {
//        RetroInstance().stripeInstance().create(HttpRequests::class.java)
//            .createCard(stripeCustomer, token)
//            .enqueue(object : Callback<CreateCardModel> {
//                override fun onFailure(call: Call<CreateCardModel>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<CreateCardModel>,
//                    response: Response<CreateCardModel>
//                ) {
//                    if (response.isSuccessful) {
//                        mCreateCardModel.value = response.body()!!
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun getCards(stripeCustomer: String) {
//        RetroInstance().stripeInstance().create(HttpRequests::class.java).getCards(stripeCustomer)
//            .enqueue(object : Callback<GetCardsListModel> {
//                override fun onFailure(call: Call<GetCardsListModel>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<GetCardsListModel>,
//                    response: Response<GetCardsListModel>
//                ) {
//                    if (response.isSuccessful) {
//                        mGetCardsListModel.value = response.body()!!
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun getCustomer(stripeCustomer: String) {
//        RetroInstance().stripeInstance().create(HttpRequests::class.java)
//            .getCustomerInfo(stripeCustomer)
//            .enqueue(object : Callback<CreateCustomerEntity> {
//                override fun onFailure(call: Call<CreateCustomerEntity>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<CreateCustomerEntity>,
//                    response: Response<CreateCustomerEntity>
//                ) {
//                    if (response.isSuccessful) {
//                        mCreateCustomerEntity.value = response.body()!!
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun updateCustomer(pos:Int,stripeCustomer: String, card_id: String) {
//        RetroInstance().stripeInstance().create(HttpRequests::class.java)
//            .updateCustomer(stripeCustomer,card_id)
//            .enqueue(object : Callback<CreateCustomerEntity> {
//                override fun onFailure(call: Call<CreateCustomerEntity>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<CreateCustomerEntity>,
//                    response: Response<CreateCustomerEntity>
//                ) {
//                    if (response.isSuccessful) {
//                        mUpdatedCustomerEntity.value = Pair(pos,response.body()!!)
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun deleteCard(stripeCustomer: String, card_id: String,pos:Int) {
//        RetroInstance().stripeInstance().create(HttpRequests::class.java)
//            .deleteCard(stripeCustomer, card_id)
//            .enqueue(object : Callback<DeletedCardEntity> {
//                override fun onFailure(call: Call<DeletedCardEntity>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<DeletedCardEntity>,
//                    response: Response<DeletedCardEntity>
//                ) {
//                    if (response.isSuccessful) {
//                        mDeleteCardsModel.value = Pair(pos,response.body()!!)
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun addNewCard(token: String, map: HashMap<String, String>) {
//        RetroInstance().withHeaders(token).create(HttpRequests::class.java).addNewCard(map)
//            .enqueue(object : Callback<WorkHourEntity> {
//                override fun onFailure(call: Call<WorkHourEntity>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<WorkHourEntity>,
//                    response: Response<WorkHourEntity>
//                ) {
//                    if (response.isSuccessful) {
////                        mAddedWorkHourEntity.value = Pair(day, response.body()!!)
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun paymentMethods(token: String) {
//        RetroInstance().withHeaders(token).create(HttpRequests::class.java).paymentMethods()
//            .enqueue(object : Callback<GetServiceEntity> {
//                override fun onFailure(call: Call<GetServiceEntity>, t: Throwable) {
//                    mErrorObserver.value =
//                        NotifyData(status = errorCode(t), message = errorMessage(t))
//                }
//
//                override fun onResponse(
//                    call: Call<GetServiceEntity>,
//                    response: Response<GetServiceEntity>
//                ) {
//                    if (response.isSuccessful) {
////                        mAddedWorkHourEntity.value = Pair(day, response.body()!!)
//                    } else {
//                        mErrorObserver.value =
//                            NotifyData(
//                                response.code(),
//                                ErrorExtensions.errorMessage1(response.errorBody()!!),
//                                ""
//                            )
//                    }
//                }
//
//            })
//    }
//
//    fun placeOrder(token : String,data : JsonObject)
//    {
//        RetroInstance().withHeaders(token).create(HttpRequests :: class.java).placeOrder(data).enqueue(
//            object : Callback<CheckoutModel> {
//                override fun onFailure(call: Call<CheckoutModel>, t: Throwable) {
//                    mErrorObserver.value = NotifyData(status = 500,message = t.message!!)
//                }
//
//                override fun onResponse(
//                    call: Call<CheckoutModel>,
//                    response: Response<CheckoutModel>
//                ) {
//                    if(response.isSuccessful)
//                    {
//                        orderObserver.value = response.body()
//                    }
//                    else
//                    {
//                        mErrorObserver.value =
//                            NotifyData(response.code(), ErrorExtensions.errorMessage1(response.errorBody()!!),"")
//                    }
//                }
//            })
//    }
//    fun placeItemOrder(token: String, map: JsonObject) {
//        RetroInstance().withHeaders(token).create(HttpRequests::class.java).placeItemOrder(map)
//            .enqueue(object : Callback<ItemOrderPlacedEntity> {
//                override fun onFailure(call: Call<ItemOrderPlacedEntity>, t: Throwable) {
//                    mErrorObserver.value = NotifyData(status = 500, message = t.message!!)
//                }
//
//                override fun onResponse(
//                    call: Call<ItemOrderPlacedEntity>,
//                    response: Response<ItemOrderPlacedEntity>
//                ) {
//                    if (response.isSuccessful)
//                        mItemOrderPlacedEntity.value = response.body()
//                    else
//                        mErrorObserver.value = NotifyData(
//                            response.code(),
//                            ErrorExtensions.errorMessage1(response.errorBody()!!),
//                            ""
//                        )
//                }
//            })
//    }

}