package com.uppro;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.splashscreen.SplashScreen;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.BracketChallengeNewActivity;
import com.uppro.Activities.ChallengeDetailActivity;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Activities.SignInActivity;
import com.uppro.Activities.SplashActivity;
import com.uppro.Fragments.BracketChallengeFragment;
import com.uppro.Fragments.ChallengeDetailFragment;
import com.uppro.Fragments.ChatFragment;
import com.uppro.Fragments.CreateChallengeFragment;
import com.uppro.Fragments.FollowingFragment;
import com.uppro.Fragments.HappeningNowFragment;
import com.uppro.Fragments.HomeFragment;
import com.uppro.Fragments.HomeNewFragment;
import com.uppro.Fragments.NotificationFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.PastVideosFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.Fragments.SearchFragment;
import com.uppro.Fragments.SettingFragment;
import com.uppro.Fragments.UpcomingFragment;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity {

    public static MainActivity context;
    private SavePref savePref;
    public static BottomNavigationView navigation;

//    @BindView(R.id.bracketdata)
    ImageView bracket;
//    @BindView(R.id.logodata)
    ImageView logo;
//    @BindView(R.id.i_menudata)
    ImageView i_menu;
//    @BindView(R.id.notificationdata)
    ImageView notification;
    public static String count = "";
    public static String count_final = "";
    public static String requests_count = "";
    public String challenge_id = "", user_id = "";
    public static TextView notification_count;
    public static RelativeLayout count_lay;

    public static AHBottomNavigation bottomNavigation;

    AHBottomNavigationItem item1;
    AHBottomNavigationItem item2;
    AHBottomNavigationItem item3;
    AHBottomNavigationItem item4;
    AHBottomNavigationItem item5;
    String notification_code = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        ButterKnife.bind(this);



        savePref = new SavePref(context);
        if (util.from_push) {
            notification_code = getIntent().getStringExtra("notification_code");
            challenge_id = getIntent().getStringExtra("challenge_id");
            user_id = getIntent().getStringExtra("friend_id");


        }

        callHome(false, "", false);
        count_lay = (RelativeLayout) findViewById(R.id.count_lay);
        notification_count = (TextView) findViewById(R.id.notification_count);
        bracket = (ImageView) findViewById(R.id.bracketdata);
        logo = (ImageView) findViewById(R.id.logodata);
        i_menu = (ImageView) findViewById(R.id.i_menudata);
        notification = (ImageView) findViewById(R.id.notificationdata);


        Log.e("auth_key", "user_id" + savePref.getID());
        Log.e("auth_key", "auth_key____" + savePref.getAuthorization_key());
        Log.e("auth_key", "token____" + SavePref.getDeviceToken(this, "token"));
        //  toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        bottomNavigation = (AHBottomNavigation) findViewById(R.id.navigationTwo);

        // Create items
        item1 = new AHBottomNavigationItem("", R.drawable.ic_nine, R.color.colorAccent);
        item2 = new AHBottomNavigationItem("", R.drawable.ic_two, R.color.colorAccent);
        item3 = new AHBottomNavigationItem("", R.drawable.home_iconnew, R.color.colorAccent);
        item4 = new AHBottomNavigationItem("", R.drawable.ic_three, R.color.colorAccent);
        item5 = new AHBottomNavigationItem("", R.drawable.ic_five, R.color.colorAccent);
        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#C601C6"));

        // Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

        // Change colors
        bottomNavigation.setAccentColor(Color.parseColor("#ffffff"));
        bottomNavigation.setInactiveColor(Color.parseColor("#BCB9B7"));

        // Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

        // Display color under navigation bar (API 21+)
        // Don't forget these lines in your style-v21
        // <item name="android:windowTranslucentNavigation">true</item>
        // <item name="android:fitsSystemWindows">true</item>
        bottomNavigation.setTranslucentNavigationEnabled(false);

        //// Manage titles
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(false);

//      bottomNavigation.setBackgroundColor(Color.parseColor("C601C6"));
        // Set current item programmatically
        bottomNavigation.setCurrentItem(2);

        // Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));

        // Add or remove notification for each item
//        bottomNavigation.setNotification("1", 1);
        // OR


// Enable / disable item & set disable color
//        bottomNavigation.enableItemAtPosition(2);
//        bottomNavigation.disableItemAtPosition(2);
//        bottomNavigation.setItemDisableColor(Color.parseColor("#3A000000"));

// Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                if (position == 0) {
                    loadFragment(new FollowingFragment(), false, false);
                    return true;
                } else if (position == 1) {
                    util.is_chat = true;
                    loadFragment(new ChatFragment("1", false), false, false);

                } else if (position == 2) {
                    callHome(false, "", false);

                    return true;
                } else if (position == 3) {
                    loadFragment(new CreateChallengeFragment(), false, false);

                    return true;
                } else if (position == 4) {
                    loadFragment(new ProfileFragment(), false, true);
                    return true;
                }
                return true;
            }
        });
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new NotificationFragment(), true, false);
            }
        });
        i_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new SettingFragment(), true, false);
            }
        });
        bracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new SearchFragment(), false, false);

            }
        });
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new BracketChallengeFragment(), true, false);
                //loadFragment(new HomeFragment(), true, false);
//                startActivity(new Intent(MainActivity.this, BracketChallengeNewActivity.class));

            }
        });



    }

    private void browseDocuments() {

        String[] mimeTypes =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf",
                        "application/zip"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), 1010);

    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadFragment(new FollowingFragment(), false, false);
                    return true;
                case R.id.navigation_message:
                    util.is_chat = true;
                    loadFragment(new ChatFragment("1", false), false, false);
                    return true;
                case R.id.navigation_create:
                    loadFragment(new CreateChallengeFragment(), false, false);
                    return true;
                case R.id.navigation_search:
                    loadFragment(new SearchFragment(), false, false);
                    return true;
                case R.id.navigation_profile:
                    loadFragment(new ProfileFragment(), false, true);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        fragment.onActivityResult(requestCode, resultCode, data);
    }


    private void loadFragment(Fragment fragment, boolean isaddToBackStack, boolean send_data) {

        if (send_data) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("back_on", false);
            fragment.setArguments(bundle);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        if (isaddToBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        shareopencheck();
    }

    private void shareopencheck() {
        //type: 0=>open, 1=>challenger, 2=>bracketChallenge

        try {
            //check_notification_flag = true;
            final Intent intent = getIntent();
            Uri data = intent.getData();
            Log.e("data_share", data.toString());

            String post_id = data.toString().substring(data.toString().lastIndexOf("/") + 1);
            Log.e("post_id", post_id);

            if (data.toString().contains("share_type2")) {
                callHome(true, post_id, true); // for bracket challenge
            } else {
                callHome(true, post_id, false); // for user challenge
            }

        } catch (Exception e) {

        }
    }

    private void callHome(boolean b, String post_id, boolean is_for_bracket) {
        HomeNewFragment homeFragment = new HomeNewFragment();
        Bundle bundle = new Bundle();
        util.from_deep = b;
        util.is_for_bracket = is_for_bracket;
        bundle.putString("post_id", post_id);
        bundle.putString("challenge_id", challenge_id);
        bundle.putString("user_id", user_id);
        bundle.putString("notification_code", notification_code);
        homeFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, homeFragment);
        fragmentTransaction.commit();
    }

    public static void UNREADNOTIFICATIONCOUNT() {
        ProgressDialog progressDialog = util.initializeProgress(context);
        //progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.LONGITUDE, "jiosdjc");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.UNREADNOTIFICATIONCOUNT, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            String count = jsonmainObject.getJSONObject("body").getString("count");

                            Log.e("count___", count);

                            if (count.equals("0") || count.equals("")) {
                                MainActivity.count_lay.setVisibility(View.GONE);
                            } else {
                                MainActivity.count_lay.setVisibility(View.VISIBLE);
                                MainActivity.notification_count.setText(count);
                            }


                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.e("callll", "Yes");
    }

    private void updateView() {
        AHNotification notification = new AHNotification.Builder()
                .setText(count)
                .setBackgroundColor(ContextCompat.getColor(context, R.color.red))
                .setTextColor(ContextCompat.getColor(context, R.color.white))
                .build();
        bottomNavigation.setNotification(notification, 1);
    }

    /* If I understood you well getFragmentManager() is now deprecated

    we will use getChildFragmentManager() Return a private FragmentManager for placing and managing Fragments inside of this Fragment.

    we will use getParentFragmentManager() Return the FragmentManager for interacting with fragments associated with this fragment's activity.

    so, if you deal with fragments inside a fragment you will use the first one and if you deal with fragments inside an activity you will use the second one.

    you can find them here package androidx.fragment.app;*/


    public static void GETCOUNT() {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.MESSAGECOUNT, formBody) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("body");

                        count = String.valueOf(jsonReq.getInt("unreadCount"));
                        requests_count = String.valueOf(jsonReq.getInt("unreadChallengeRequestCount"));
                        count_final=String.valueOf(jsonReq.getInt("unreadCount")+jsonReq.getInt("unreadChallengeRequestCount"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


//                    if (!count.equals("0")) {
                    AHNotification notification = new AHNotification.Builder()
                            .setText(count_final)
                            .setBackgroundColor(ContextCompat.getColor(context, R.color.red))
                            .setTextColor(ContextCompat.getColor(context, R.color.white))
                            .build();
                    bottomNavigation.setNotification(notification, 1);
//                    }else {
//
//                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}