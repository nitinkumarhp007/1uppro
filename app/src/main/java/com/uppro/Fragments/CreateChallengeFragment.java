package com.uppro.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.uppro.Activities.ForgotPasswordActivity;
import com.uppro.Activities.SignUpActivity;
import com.uppro.MainActivity;
import com.uppro.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CreateChallengeFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.open_button)
    RelativeLayout open_button;
    @BindView(R.id.challenger_button)
    RelativeLayout challenger_button;

    public CreateChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_challenge, container, false);

        unbinder = ButterKnife.bind(this, view);

        MainActivity.UNREADNOTIFICATIONCOUNT();
        MainActivity.GETCOUNT();

        return view;
    }

    @OnClick({R.id.open_button, R.id.challenger_button})
    public void onClick(View view) {
        CreateFragment fragment = new CreateFragment();
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.open_button:
                bundle.putBoolean("is_challenger", false);
                fragment.setArguments(bundle);
                break;
            case R.id.challenger_button:
                bundle.putBoolean("is_challenger", true);
                fragment.setArguments(bundle);
                break;
        }
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}