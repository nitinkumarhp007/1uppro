package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Adapters.FeedBracketChallengeAdapter;
import com.uppro.Adapters.HomeAdapter;
import com.uppro.Adapters.PastChallengeAdapter;
import com.uppro.R;
import com.uppro.Util.OnSwipeTouchListener;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class FeedBracketChallengeFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;

    
//    @BindView(R.id.my_recycler_view)
//    RecyclerView myRecyclerView;


    @BindView(R.id.profile_1_up)
    TextView profile_1_up;
    @BindView(R.id.profile_2_up)
    TextView profile_2_up;
    @BindView(R.id.profile_1_score)
    TextView profile_1_score;
    @BindView(R.id.profile_2_score)
    TextView profile_2_score;
    @BindView(R.id.chat)
    ImageView chat;
    @BindView(R.id.profile_1)
    TextView profile_1;
    @BindView(R.id.profile_2)
    TextView profile_2;
    @BindView(R.id.profile_1_pic)
    ImageView profile_1_pic;
    @BindView(R.id.profile_1_thumb)
    ImageView profile_1_thumb;
    @BindView(R.id.profile_2_thumb)
    ImageView profile_2_thumb;
    @BindView(R.id.profile_2_pic)
    ImageView profile_2_pic;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.end_date)
    TextView end_date;
    @BindView(R.id.rules)
    TextView rules;
    @BindView(R.id.profile_1_layout)
    LinearLayout profile_1_layout;
    @BindView(R.id.video_play)
    ImageView video_play;
    @BindView(R.id.video_play_2)
    ImageView video_play_2;
    @BindView(R.id.join_challenge)
    TextView join_challenge;
    @BindView(R.id.reportOne)
    ImageView reportOne;
    @BindView(R.id.reportTwo)
    ImageView reportTwo;
    @BindView(R.id.share)
    ImageView share;
    @BindView(R.id.profile_two_background)
    ImageView profile_two_background;
    @BindView(R.id.challenger_button)
    RelativeLayout challenger_button;

    @BindView(R.id.parent)
    LinearLayout parent;
    @BindView(R.id.layout2profile)
    LinearLayout layout2profile;

    @BindView(R.id.mMainLayout)
    LinearLayout mMainLayout;

    String click_touch="";

    public FeedBracketChallengeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed_bracket_challenge, container, false);

        unbinder = ButterKnife.bind(this, view);


        context = getActivity();
        savePref = new SavePref(context);


        end_date.setText("End Date : "+getArguments().getString("enddate"));
        profile_1.setText(getArguments().getString("useronename") +"\nLevel : "+ getArguments().getString("challengeronelevel"));
        profile_2.setText(getArguments().getString("usertwoname") +"\nLevel : "+ getArguments().getString("challengertwolevel"));

        if (getArguments().getString("rules").isEmpty()){
            rules.setVisibility(View.GONE);
        }else {
            rules.setVisibility(View.VISIBLE);
        }

        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new IOSDialog.Builder(getActivity())
                        .setTitle("Rules")
                        .setCancelable(false)
                        .setMessage(getArguments().getString("rules")).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type2/"+ getArguments().getString("challengeid"));
                context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
            }
        });
        category.setText(getArguments().getString("category_name"));
        profile_1_score.setText(getArguments().getString("useronevote"));
        profile_2_score.setText(getArguments().getString("usertwovote"));

        Glide.with(context).load(getArguments().getString("useronethumb")).into(profile_1_thumb);
        Glide.with(context).load(getArguments().getString("usertwothumb")).into(profile_2_thumb);

        Glide.with(context).load(getArguments().getString("useroneimage")).into(profile_1_pic);
        Glide.with(context).load(getArguments().getString("usertwoimage")).into(profile_2_pic);


//        if (list.get(position).getIsRules().equals("1")) {
//            rules.setText("Rules: View Rules");
//        } else {
//            rules.setText("Rules: No Rules");
//        }

//        FeedBracketChallengeAdapter adapter = new FeedBracketChallengeAdapter(context);
//        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
//        myRecyclerView.setAdapter(adapter);

        video_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GETPATH(getArguments().getString("useronevideo"));
            }
        });

        video_play_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GETPATH(getArguments().getString("usertwovideo"));
            }
        });

        profile_1_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_touch="left";
                LIKEVIDEO("1",0,getArguments().getString("useroneid"));
            }
        });

        profile_2_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_touch="right";
                LIKEVIDEO("1",0,getArguments().getString("usertwoid"));
            }
        });

        parent.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
//                Toast.makeText(getActivity(), "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {
//                Toast.makeText(getActivity(), "right", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeLeft() {
                getParentFragmentManager().popBackStack();
            }
            public void onSwipeBottom() {
//                Toast.makeText(getActivity(), "bottom", Toast.LENGTH_SHORT).show();
            }

        });



        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void LIKEVIDEO(String isLike, int position,String challengerId) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, getArguments().getString("challengeid"));
        formBuilder.addFormDataPart(Parameters.CHALLENGERID, challengerId);
        formBuilder.addFormDataPart(Parameters.ISLIKE, isLike);//( 0=>dislike, 1=>like )
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VOTEBRACKETCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                                //like
                                if (click_touch.equals("left")){
                                    profile_1_score.setText((String.valueOf(Integer.parseInt(getArguments().getString("useronevote")) + 1)));
                                }else {
                                    profile_2_score.setText((String.valueOf(Integer.parseInt(getArguments().getString("usertwovote")) + 1)));
                                }

                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void GETPATH(String id) {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/" + id + "/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata = "";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        }  else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            }  else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                   /* context.startActivity(new Intent(context, ExoPlayerActivity.class)
                            .putExtra("data", pathdata));*/
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}