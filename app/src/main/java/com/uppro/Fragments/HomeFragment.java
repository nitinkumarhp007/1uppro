package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.ChatlistAdapter;
import com.uppro.Adapters.HomeAdapter;
import com.uppro.Adapters.NotificationlistAdapter;
import com.uppro.Adapters.ReportAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.ChattingModel;
import com.uppro.ModelClasses.NotificationModel;
import com.uppro.ModelClasses.ReportTextModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.OnSwipeTouchListener;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;
import com.uppro.parser.GetAsyncPut;
import com.whiteelephant.gifplayer.GifView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class HomeFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.animationViewOne)
    GifView animationViewOne;
    @BindView(R.id.animationViewTwo)
    GifView animationViewTwo;
    @BindView(R.id.parent)
    ConstraintLayout parent;
    private ArrayList<ChallengeListModel> list;
    private ArrayList<ReportTextModel> list_report_text;
    HomeAdapter adapter = null;

    Dialog dialog1 = null;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        if (util.from_deep) {
            util.from_deep = false;

            if(util.is_for_bracket)
            {
                util.is_for_bracket=false;
            }

            String post_id = getArguments().getString("post_id");

            ChallengeDetailFragment fragment = new ChallengeDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_id", post_id);
            fragment.setArguments(bundle);

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else {
            if (ConnectivityReceiver.isConnected())
                CHALLENGELISTING();
            else
                util.IOSDialog(context, util.internet_Connection_Error);
        }


        myRecyclerView.setOnTouchListener(new OnSwipeTouchListener(context) {
            public void onSwipeTop() {
                //Toast.makeText(context, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                // Toast.makeText(context, "right", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeLeft() {
                //  Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", "");
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

            public void onSwipeBottom() {
                //Toast.makeText(context, "bottom", Toast.LENGTH_SHORT).show();
            }

        });




        return view;
    }


    private void CHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CHALLENGELISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                MainActivity.UNREADNOTIFICATIONCOUNT();
                /*try {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, new ChallengeDetailFragment());
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
                catch (Exception e)
                {}*/
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ChallengeListModel challengeListModel = new ChallengeListModel();
                                challengeListModel.setId(object.getString("id"));
                                challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                challengeListModel.setCategoryId(object.optString("categoryId"));
                                challengeListModel.setCompetitors(object.optString("competitors"));
                                challengeListModel.setIsRules(object.optString("isRules"));
                                challengeListModel.setRules(object.optString("rules"));
                                challengeListModel.setTime(object.optString("time"));
                                challengeListModel.setType(object.optString("type"));
                                challengeListModel.setUserId(object.optString("userId"));
                                challengeListModel.setVideo(object.optString("video"));
                                challengeListModel.setStart_date(object.optString("startDate"));
                                challengeListModel.setEnd_date(object.optString("endDate"));
                                challengeListModel.setThumb(object.optString("videoThumbnail"));

                                challengeListModel.setVotestocreator(object.optString("votesToCreator"));
                                challengeListModel.setVotestochallenger(object.optString("votesToChallenger"));
                                challengeListModel.setIsvoted(object.optString("isVoted"));

                                challengeListModel.setName(object.getJSONObject("challengeCreator").optString("name"));
                                challengeListModel.setImage(object.getJSONObject("challengeCreator").optString("image"));

                                if (object.getJSONArray("challengeUsers").length() > 0) {
                                    JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(0);

                                    challengeListModel.setUserId__challenger(obj.getJSONObject("challenger").optString("id"));
                                    challengeListModel.setName_challenger(obj.getJSONObject("challenger").optString("name"));
                                    challengeListModel.setImage_challenger(obj.getJSONObject("challenger").optString("image"));
                                    challengeListModel.setThumb_challenger(obj.optString("videoThumbnail"));
                                    challengeListModel.setVideo_challenger(obj.optString("video"));
                                }

                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                //  adapter = new HomeAdapter(context, list, HomeFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                new IOSDialog.Builder(context)
                                        .setTitle(context.getResources().getString(R.string.app_name))
                                        .setCancelable(false)
                                        .setMessage("Please follow categories to see challenges.").
                                        setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                fragmentTransaction.replace(R.id.frame_container, new FollowingFragment());
                                                fragmentTransaction.commit();
                                            }
                                        })
                                        /* .setNegativeButton("Cancel", null)*/.show();
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void VOTECHALLENGE(String type, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, list.get(position).getId());
        formBuilder.addFormDataPart(Parameters.ISLIKE, "1");//( 0=>dislike, 1=>like )
        formBuilder.addFormDataPart(Parameters.TYPE, type); //( 0=>voteToCreator, 1=>voteToChallenger )
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VOTECHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (type.equals("0")) {
                                animationViewOne.setVisibility(View.VISIBLE);
                                animationViewOne.start();
                                animationViewOne.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewOne.setVisibility(View.INVISIBLE);
                                    }
                                });
                                list.get(position).setVotestocreator
                                        (String.valueOf(Integer.parseInt(list.get(position).getVotestocreator()) + 1));
                            } else {
                                animationViewTwo.setVisibility(View.VISIBLE);
                                animationViewTwo.start();
                                animationViewTwo.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewTwo.setVisibility(View.INVISIBLE);
                                    }
                                });
                                list.get(position).setVotestochallenger
                                        (String.valueOf(Integer.parseInt(list.get(position).getVotestochallenger()) + 1));
                            }

                            if (adapter != null)
                                adapter.notifyDataSetChanged();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void showInformationPop(String challengeId) {
        dialog1 = new Dialog(getActivity());
        DisplayMetrics display = new DisplayMetrics();
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(true);
        dialog1.setContentView(R.layout.report_popup);
        Objects.requireNonNull(dialog1.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog1.getWindow().setGravity(Gravity.BOTTOM);
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog1.getWindow().setBackgroundDrawableResource(R.drawable.shadow_border_bg_more_rounded);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        TextView tvDone = dialog1.findViewById(R.id.tvDone);
        RecyclerView my_recycler_view = dialog1.findViewById(R.id.my_recycler_view);

        my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        // my_recycler_view.setAdapter(new ReportAdapter(context, list_report_text, HomeFragment.this, challengeId));


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
        Window window1 = dialog1.getWindow();
        window1.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void REPORTCHALLENGE_API(String challengeId, String id) {
        if (dialog1 != null)
            dialog1.dismiss();
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challengeId);
        formBuilder.addFormDataPart(Parameters.CHALLENGEREPORTTEXTID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.REPORTCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            //util.showToast(context, jsonMainobject.getString("message"));
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("Challenge Reported Successfully").setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                                    .show();
                        } else {
                            if (jsonMainobject.getString("message").equals(util.Invalid_Authorization)) {

                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void CHALLENGEREPORTTEXTLISTING(String challengeId) {
        final ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.LONGITUDE, "jiosdjc");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CHALLENGEREPORTTEXTLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list_report_text = new ArrayList<>();
                if (list_report_text.size() > 0)
                    list_report_text.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbodyArray = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < jsonbodyArray.length(); i++) {
                                JSONObject jsonbody = jsonbodyArray.getJSONObject(i);
                                ReportTextModel reportTextModel = new ReportTextModel();
                                reportTextModel.setId(jsonbody.getString("id"));
                                reportTextModel.setText(jsonbody.getString("text"));
                                list_report_text.add(reportTextModel);
                            }

                            showInformationPop(challengeId);
                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }



}