package com.uppro.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.uppro.Adapters.BracketBracketNewChallengeAdapter;
import com.uppro.Adapters.HappeningEnterAdapter;
import com.uppro.ModelClasses.ChallengeUserModel;
import com.uppro.R;
import com.uppro.databinding.FragmentBracketChallengeNewBinding;

import java.util.ArrayList;


public class BracketChallengeNewFragment extends Fragment {


    FragmentBracketChallengeNewBinding bind;

    public BracketChallengeNewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_bracket_challenge_new, container, false);
        return bind.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bind.tvRound.setText(getArguments().getString("round"));
        bind.swipeRight.setVisibility(!bind.tvRound.getText().toString().equals(getArguments().getString("last_round"))?View.VISIBLE : View.GONE);

        if(getArguments().containsKey("round_data")){
            ArrayList<ChallengeUserModel> challengeUserModels=getArguments().getParcelableArrayList("round_data");
            bind.rvChallenge.setLayoutManager(new LinearLayoutManager(requireActivity()));
            bind.rvChallenge.setAdapter(new HappeningEnterAdapter(requireActivity(), challengeUserModels
                    , getArguments().getString("category_id"), getArguments().getString("category_name"), this));
            bind.rvChallenge.setVisibility(View.VISIBLE);
        }
//        bind.rvChallenge.setAdapter(new BracketBracketNewChallengeAdapter());
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bind.unbind();
    }
}