package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.CommentAdapter;
import com.uppro.Adapters.MentionUserAdapter;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.ModelClasses.CommentModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncDELETE;
import com.uppro.parser.GetAsyncPut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CommentFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.my_recycler_view_mention)
    RecyclerView my_recycler_view_mention;
    @BindView(R.id.type_message)
    EditText typeMessage;
    @BindView(R.id.send_button)
    ImageView sendButton;

    String challenge_id = "";

    String edit_comment = "";
    String edit_comment_id = "";
    int selectedPostion = 0;
    private ArrayList<CommentModel> list;
    private ArrayList<CategoryModel> user_list;
    CommentAdapter Adapter = null;

    String text_inputted = "";
    ProgressDialog mDialog;
    MentionUserAdapter mentionUserAdapter;

    String tagIds = "";

    public CommentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_comment, container, false);
        unbinder = ButterKnife.bind(this, view);


        context = getActivity();
        mDialog = util.initializeProgress(context);
        savePref = new SavePref(context);
        user_list = new ArrayList<>();
        list = new ArrayList<>();
        Adapter = new CommentAdapter(context, list, CommentFragment.this);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(Adapter);
        init();

        return view;
    }

    private void init() {
        challenge_id = getArguments().getString("challenge_id");

        if (ConnectivityReceiver.isConnected())
            CHALLENGECOMMENTSUGGESTIONLISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityReceiver.isConnected()) {
                    if (edit_comment.isEmpty()) {
                        if (!typeMessage.getText().toString().trim().isEmpty()) {
                            POST_COMMENT();
                        }
                    } else {
                        if (!typeMessage.getText().toString().trim().isEmpty()) {
                            EDIT_COMMENT();
                        }
                    }
                } else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        });

        typeMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
              /*  if (s.toString().length() > 1) {
                    if (s.toString().substring(s.toString().length() - 1).equals("@")) {
                        Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                    }
                }*/
                if (edit_comment.isEmpty()) {
                    if (user_list != null) {
                        if (user_list.size() > 0) {
                            int counter = 0;
                            if (s.toString().length() > 0) {
                                if (s.toString().length() == 1 && String.valueOf(s.toString().charAt(s.toString().length() - 1)).equals("@")) {
                                    //Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                                    text_inputted = s.toString();
                                    setAdapter1();
                                } else if (s.toString().length() > 1) {

                                    if (String.valueOf(s.toString().charAt(s.toString().length() - 1)).equals("@") && String.valueOf(s.toString().charAt(s.toString().length() - 2)).equals(" ")) {
                                        // Toast.makeText(context, "Open mention list", Toast.LENGTH_SHORT).show();
                                        text_inputted = s.toString();
                                        setAdapter1();

                                    }

                                    for (int i = 0; i < s.toString().length(); i++) {
                                        if (s.charAt(i) == '@') {
                                            counter++;
                                            Log.e("Gone_list:", "3");
                                        }
                                    }
                                    Log.e("counter", String.valueOf(counter));
                                    if (counter > 0) {
                                        if (mentionUserAdapter != null)
                                            mentionUserAdapter.filter(typeMessage.getText().toString().substring(s.toString().lastIndexOf("@") + 1, typeMessage.getText().toString().length()));
                                    } else {
                                        //setAdapter_comment();
                                    }
                                }
                            } else {
                                my_recycler_view_mention.setVisibility(View.GONE);
                            }
                        }
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setAdapter1() {
        mentionUserAdapter = new MentionUserAdapter(context, user_list, CommentFragment.this);
        my_recycler_view_mention.setLayoutManager(new LinearLayoutManager(context));
        my_recycler_view_mention.setAdapter(mentionUserAdapter);
        my_recycler_view_mention.setVisibility(View.VISIBLE);

    }

    private void GET_COMMENTAPI(boolean is_show) {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challenge_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHALLENGECOMMENTLIST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();

                if (list.size() > 0)
                    list.clear();


                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);
                                CommentModel commentModel = new CommentModel();
                                commentModel.setComment(object.getString("commentText"));
                                commentModel.setCreated(object.getString("created"));
                                commentModel.setId(object.getString("id"));
                                commentModel.setName(object.getJSONObject("user").getString("name"));
                                commentModel.setImage(object.getJSONObject("user").getString("image"));
                                commentModel.setUser_id(object.getJSONObject("user").getString("id"));
                                list.add(commentModel);
                            }
                            if (list.size() > 0) {

                                Collections.reverse(list);

                                Adapter.notifyDataSetChanged();
                                myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void POST_COMMENT() {
        util.hideKeyboard(getActivity());
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.COMMENTTEXT, typeMessage.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challenge_id);
        formBuilder.addFormDataPart(Parameters.TAGIDS, tagIds);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADDCHALLENGECOMMENT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                typeMessage.setText("");
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject object = jsonmainObject.getJSONObject("body");
                            CommentModel commentModel = new CommentModel();
                            commentModel.setComment(object.getString("commentText"));
                            commentModel.setCreated(object.getString("created"));
                            commentModel.setId(object.getString("id"));
                            commentModel.setName(object.getJSONObject("user").getString("name"));
                            commentModel.setImage(object.getJSONObject("user").getString("image"));
                            commentModel.setUser_id(object.getJSONObject("user").getString("id"));
                            list.add(commentModel);


                            if (list.size() == 1) {
                                Adapter.notifyDataSetChanged();
                                myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);

                            } else {
                                Adapter.notifyDataSetChanged();
                                myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);
                            }



                            /*if (ConnectivityReceiver.isConnected()) {
                                GET_COMMENTAPI(false);
                            } else {
                                util.IOSDialog(context, util.internet_Connection_Error);
                            }
*/
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void EDIT_COMMENT() {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.COMMENTTEXT, typeMessage.getText().toString().trim());
        formBuilder.addFormDataPart("id", edit_comment_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.EDITCOMMENT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            list.get(selectedPostion).setComment(typeMessage.getText().toString().trim());
                            typeMessage.setText("");
                            edit_comment="";
                            Adapter.notifyDataSetChanged();
                        } else {
                            if (jsonMainobject.getString("message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void CHALLENGECOMMENTSUGGESTIONLISTING() {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challenge_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHALLENGECOMMENTSUGGESTIONLISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {

                if (user_list.size() > 0)
                    user_list.clear();

                if (ConnectivityReceiver.isConnected()) {
                    GET_COMMENTAPI(true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                //   categoryModel.setName(object.getString("name"));
                                categoryModel.setName(object.optString(Parameters.USERNAME));
                                categoryModel.setImage(object.optString("image"));
                                categoryModel.setIsFollowed(object.optString("isFollowed"));
                                user_list.add(categoryModel);
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void setclickdata(String text, String id) {

        String text_inputted_new = text_inputted.substring(0, text_inputted.lastIndexOf("@"));
        typeMessage.setText(text_inputted_new + "@" + text.replace(" ", "") + " ");
        typeMessage.setSelection(typeMessage.getText().length());


        myRecyclerView.scrollToPosition(Adapter.getItemCount() - 1);

        my_recycler_view_mention.setVisibility(View.GONE);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void DELETECOMMENT_API(int position) {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.COMMENTID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.DELETECOMMENT, formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response response) {
                mDialog.dismiss();
                try {
                    String result = response.body().string();
                    Log.e("a_result", result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful()) {

                    list.remove(position);
                    Adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void edit(int position) {
        edit_comment = "1";
        selectedPostion=position;
        typeMessage.setText(list.get(position).getComment());
        edit_comment_id=list.get(position).getId();
    }
}