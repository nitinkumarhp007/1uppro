package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Adapters.ChatlistAdapter;
import com.uppro.Adapters.MyChallengesAdapter;
import com.uppro.Adapters.PastVideosAdapter;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class PastVideosFragment extends Fragment {

    Unbinder unbinder;

    Context context;
    private SavePref savePref;

    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.back_button)
    ImageView back_button;
    ArrayList<ChallengeListModel> list;

    String category_id = "";
    PastVideosAdapter adapter = null;

    public PastVideosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_past_videos, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        inti();

        return view;
    }

    private void inti() {
        list=new ArrayList<>();
        category_id = getArguments().getString("category_id");
        adapter = new PastVideosAdapter(context, list, PastVideosFragment.this);
        myRecyclerView.setAdapter(adapter);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });


        if (ConnectivityReceiver.isConnected())
            PASTVIDEOS();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void PASTVIDEOS() {
        ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PAYPALID, "");
        if (!category_id.isEmpty())
            formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.PASTVIDEOS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                progressDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ChallengeListModel challengeListModel = new ChallengeListModel();


                                    challengeListModel.setId(object.getString("challengeId"));
                                    challengeListModel.setUserId(object.getString("userId"));
                                    challengeListModel.setVideo(object.getString("video"));
                                    challengeListModel.setType(object.getString("type"));
                                    challengeListModel.setIsRules(object.getString("rank"));
                                    challengeListModel.setThumb(object.getString("videoThumbnail"));
                                    challengeListModel.setComment_count(object.optString("votes"));
                                    challengeListModel.setName(object.getJSONObject("user").getString("name"));
                                    challengeListModel.setImage(object.getJSONObject("user").getString("image"));
                                    challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                    list.add(challengeListModel);



                            }

                            if (list.size() > 0) {
                                PastVideosAdapter adapter = new PastVideosAdapter(context, list, PastVideosFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("No Videos Found");
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void GETPATH(String id) {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/" + id + "/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata = "";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        }  else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            }  else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                   /* context.startActivity(new Intent(context, ExoPlayerActivity.class)
                            .putExtra("data", pathdata));*/
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}