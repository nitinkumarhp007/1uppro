package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.HomeAdapter;
import com.uppro.Adapters.HomeBracketAdapter;
import com.uppro.Adapters.ReportAdapter;
import com.uppro.Adapters.ReportAdapterBracket;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.BracketChallengeListingModel;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.ReportTextModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;
import com.whiteelephant.gifplayer.GifView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class HomeBracketFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.forswipe)
    RelativeLayout forswipe;
    @BindView(R.id.animationViewOne)
    GifView animationViewOne;
    @BindView(R.id.animationViewTwo)
    GifView animationViewTwo;
    @BindView(R.id.parent)
    ConstraintLayout parent;
    private ArrayList<BracketChallengeListingModel> list;
    private ArrayList<ReportTextModel> list_report_text;
    HomeBracketAdapter adapter;
    private float x1, x2;
    Dialog dialog1 = null;

    private float upX1;
    private float upX2;
    private float upY1;
    private float upY2;
    private boolean isTouchCaptured = false;
    static final int min_distance = 100;

    String challenge_id = "";

    public HomeBracketFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        list = new ArrayList<>();
        challenge_id = getArguments().getString("challenge_id");
        CallAPI();

        return view;
    }

    private void CallAPI() {
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new HomeBracketAdapter(context, list, HomeBracketFragment.this);
        myRecyclerView.setAdapter(adapter);


        if (ConnectivityReceiver.isConnected())
            CHALLENGELISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


        setUpItemTouchHelper();
    }


    private void setUpItemTouchHelper() {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

//            private void init() {
//                background = new ColorDrawable(Color.RED);
//                xMark = ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_clear_24dp);
//                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
//                xMarkMargin = (int) MainActivity.this.getResources().getDimension(R.dimen.ic_clear_margin);
//                initiated = true;
//            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
//                int position = viewHolder.getAdapterPosition();
//                TestAdapter testAdapter = (TestAdapter)recyclerView.getAdapter();
//                if (testAdapter.isUndoOn() && testAdapter.isPendingRemoval(position)) {
//                    return 0;
//                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {

                int swipedPosition = viewHolder.getAdapterPosition();

//                Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", "");
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
//                TestAdapter adapter = (TestAdapter)mRecyclerView.getAdapter();
//                boolean undoOn = adapter.isUndoOn();
//                if (undoOn) {
//                    adapter.pendingRemoval(swipedPosition);
//                } else {
//                    adapter.remove(swipedPosition);
//                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }

//                if (!initiated) {
//                    init();
//                }

//                // draw red background
//                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
//                background.draw(c);
//
//                // draw x mark
//                int itemHeight = itemView.getBottom() - itemView.getTop();
//                int intrinsicWidth = xMark.getIntrinsicWidth();
//                int intrinsicHeight = xMark.getIntrinsicWidth();
//
//                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
//                int xMarkRight = itemView.getRight() - xMarkMargin;
//                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
//                int xMarkBottom = xMarkTop + intrinsicHeight;
//                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
//
//                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(myRecyclerView);
    }

    private void CHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, "73");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BRACKETCHALLENGEDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();

                if (list.size() > 0)
                    list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject dataOne = jsonmainObject.getJSONObject("body");
                            JSONObject dataTwo = dataOne.getJSONObject("standings");


                            JSONArray data = dataTwo.getJSONArray("top8");
                            JSONArray data2 = dataTwo.getJSONArray("top16");
                            JSONArray data3 = dataTwo.getJSONArray("top32");
                            JSONArray data4 = dataTwo.getJSONArray("top64");

                            if (data.length() > 0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject object = data.getJSONObject(i);
                                    BracketChallengeListingModel challengeListModel = new BracketChallengeListingModel();
                                    challengeListModel.setId(dataOne.getString("id"));
                                    challengeListModel.setCategory_name(dataOne.getJSONObject("category").getString("name"));
                                    challengeListModel.setCategoryId(dataOne.optString("categoryId"));
                                    challengeListModel.setCompetitors(dataOne.optString("competitors"));
                                    challengeListModel.setIsRules(dataOne.optString("isRules"));
                                    challengeListModel.setRules(dataOne.optString("rules"));
                                    challengeListModel.setStart_date(object.optString("startDate"));
                                    challengeListModel.setEnd_date(dataOne.optString("endDate"));

                                    challengeListModel.setiHavePaid(dataOne.optString("iHavePaid"));
                                    challengeListModel.setiHaveSubmitted(dataOne.optString("iHaveSubmitted"));
                                    challengeListModel.setJoinedChallengersIncludingMe(dataOne.optString("joinedChallengersIncludingMe"));


//                                    if (object.getJSONArray("challengeUsers").length() > 0) {
//                                        JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(0);

                                    challengeListModel.setUserId(String.valueOf(object.getJSONObject("challengerWithGreaterVotes").optString("id")));
                                    challengeListModel.setName(String.valueOf(object.getJSONObject("challengerWithGreaterVotes").optString("username")));
                                    challengeListModel.setImage(String.valueOf(object.getJSONObject("challengerWithGreaterVotes").optString("image")));
                                    challengeListModel.setThumb(String.valueOf(object.getJSONObject("challengerWithGreaterVotes").optString("videoThumbnail")));
                                    challengeListModel.setVideo(String.valueOf(object.getJSONObject("challengerWithGreaterVotes").optString("video")));
                                    challengeListModel.setVoteOne(String.valueOf(object.getJSONObject("challengerWithGreaterVotes").optString("votes")));
                                    challengeListModel.setSeedOne(String.valueOf(object.getJSONObject("challengerWithGreaterVotes").optInt("seed")));


                                    challengeListModel.setUserId__challenger(String.valueOf(object.getJSONObject("challengerWithLesserVotes").optInt("id")));
                                    challengeListModel.setName_challenger(object.getJSONObject("challengerWithLesserVotes").optString("username"));
                                    challengeListModel.setImage_challenger(object.getJSONObject("challengerWithLesserVotes").optString("image"));
                                    challengeListModel.setThumb_challenger(object.getJSONObject("challengerWithLesserVotes").optString("videoThumbnail"));
                                    challengeListModel.setVideo_challenger(String.valueOf(object.getJSONObject("challengerWithLesserVotes").optInt("video")));
                                    challengeListModel.setVoteTwo(object.getJSONObject("challengerWithLesserVotes").optString("votes"));
                                    challengeListModel.setSeedTwo(String.valueOf(object.getJSONObject("challengerWithLesserVotes").optInt("seed")));

//                                    }

                                    list.add(challengeListModel);
                                }
                            }


                            if (list.size() > 0) {
                                mDialog.dismiss();
                                adapter.notifyDataSetChanged();
                                MainActivity.UNREADNOTIFICATIONCOUNT();
                                MainActivity.GETCOUNT();
                            } else {

//                                        /* .setNegativeButton("Cancel", null)*/.show();
                            }


                        } else {
                            mDialog.dismiss();
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    mDialog.dismiss();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void VOTECHALLENGE(String type, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, list.get(position).getId());

        if (type.equals("0")){
            formBuilder.addFormDataPart(Parameters.CHALLENGERID, list.get(position).getUserId());
        }else {
            formBuilder.addFormDataPart(Parameters.CHALLENGERID, list.get(position).getUserId__challenger());
        }
        formBuilder.addFormDataPart(Parameters.ISLIKE, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VOTEBRACKETCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (type.equals("0")) {
                                animationViewOne.setVisibility(View.VISIBLE);
                                animationViewOne.start();
                                animationViewOne.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewOne.setVisibility(View.INVISIBLE);
                                    }
                                });
                                list.get(position).setVoteOne
                                        (String.valueOf(Integer.parseInt(list.get(position).getVoteOne()) + 1));
                            } else {
                                animationViewTwo.setVisibility(View.VISIBLE);
                                animationViewTwo.start();
                                animationViewTwo.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewTwo.setVisibility(View.INVISIBLE);
                                    }
                                });
                                list.get(position).setVoteTwo
                                        (String.valueOf(Integer.parseInt(list.get(position).getVoteTwo()) + 1));
                            }

                            if (adapter != null)
                                adapter.notifyDataSetChanged();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void showInformationPop(String challengeId) {
        dialog1 = new Dialog(getActivity());
        DisplayMetrics display = new DisplayMetrics();
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(true);
        dialog1.setContentView(R.layout.report_popup);
        Objects.requireNonNull(dialog1.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog1.getWindow().setGravity(Gravity.BOTTOM);
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog1.getWindow().setBackgroundDrawableResource(R.drawable.shadow_border_bg_more_rounded);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        TextView tvDone = dialog1.findViewById(R.id.tvDone);
        RecyclerView my_recycler_view = dialog1.findViewById(R.id.my_recycler_view);

        my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        my_recycler_view.setAdapter(new ReportAdapterBracket(context, list_report_text, HomeBracketFragment.this, challengeId));


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
        Window window1 = dialog1.getWindow();
        window1.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void REPORTCHALLENGE_API(String challengeId, String id) {
        if (dialog1 != null)
            dialog1.dismiss();
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challengeId);
        formBuilder.addFormDataPart(Parameters.CHALLENGEREPORTTEXTID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.REPORTCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            //util.showToast(context, jsonMainobject.getString("message"));
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("Challenge Reported Successfully").setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                                    .show();
                        } else {
                            if (jsonMainobject.getString("message").equals(util.Invalid_Authorization)) {

                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void CHALLENGEREPORTTEXTLISTING(String challengeId) {
        final ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.LONGITUDE, "jiosdjc");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CHALLENGEREPORTTEXTLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list_report_text = new ArrayList<>();
                if (list_report_text.size() > 0)
                    list_report_text.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbodyArray = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < jsonbodyArray.length(); i++) {
                                JSONObject jsonbody = jsonbodyArray.getJSONObject(i);
                                ReportTextModel reportTextModel = new ReportTextModel();
                                reportTextModel.setId(jsonbody.getString("id"));
                                reportTextModel.setText(jsonbody.getString("text"));
                                list_report_text.add(reportTextModel);
                            }

                            showInformationPop(challengeId);
                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void GETPATH(String id) {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/" + id + "/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata = "";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        }  else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            }  else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}