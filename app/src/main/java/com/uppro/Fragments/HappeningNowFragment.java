package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Adapters.HappeningNowAdapter;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class HappeningNowFragment extends Fragment {

    Unbinder unbinder;
    Context context;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.error_message)
    TextView error_message;
    private ArrayList<ChallengeListModel> list;
    HappeningNowAdapter adapter = null;

    String type = "", user_id = "";

    public HappeningNowFragment(String type, String user_id) {
        this.type = type;
        this.user_id = user_id;
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_happening_now, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);
        init();

        return view;
    }

    private void init() {
        // userId = getArguments().getString("userId");

        Log.e("user_id", user_id + "  " + type);

        if (type.equals("1"))
            title.setText(R.string.happening_now);
        else if (type.equals("2"))
            title.setText("Wins");
        else
            title.setText("Losses");

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });


        if (ConnectivityReceiver.isConnected())
            GETHAPPENNINGNOW();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void GETHAPPENNINGNOW() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.USERID, user_id);
        formBuilder.addFormDataPart(Parameters.TYPE, type);//1=Happening now , 2=Wins, 3=looses
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GETHAPPENNINGNOW, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ChallengeListModel challengeListModel = new ChallengeListModel();
                                challengeListModel.setId(object.getString("id"));
                                challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                challengeListModel.setCategoryId(object.optString("categoryId"));
                                challengeListModel.setCompetitors(object.optString("competitors"));
                                challengeListModel.setIsRules(object.optString("isRules"));
                                challengeListModel.setRules(object.optString("rules"));
                                challengeListModel.setTime(object.optString("time"));
                                challengeListModel.setType(object.optString("type"));
                                challengeListModel.setUserId(object.optString("userId"));
                                challengeListModel.setVideo(object.optString("video"));
                                challengeListModel.setStart_date(object.optString("startDate"));
                                challengeListModel.setEnd_date(object.optString("endDate"));
                                challengeListModel.setThumb(object.optString("videoThumbnail"));

                                challengeListModel.setVotestocreator(object.optString("votesToCreator"));
                                challengeListModel.setVotestochallenger(object.optString("votesToChallenger"));
                                challengeListModel.setIsvoted(object.optString("isVoted"));

                                challengeListModel.setName(object.getJSONObject("challengeCreator").optString("name"));
                                challengeListModel.setImage(object.getJSONObject("challengeCreator").optString("image"));

                                if (object.getJSONArray("challengeUsers").length() > 0) {
                                    JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(0);

                                    challengeListModel.setUserId__challenger(obj.getJSONObject("challenger").optString("id"));
                                    challengeListModel.setName_challenger(obj.getJSONObject("challenger").optString("name"));
                                    challengeListModel.setImage_challenger(obj.getJSONObject("challenger").optString("image"));
                                    challengeListModel.setThumb_challenger(obj.optString("videoThumbnail"));
                                    challengeListModel.setVideo_challenger(obj.optString("video"));
                                }

                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                adapter = new HappeningNowAdapter(context, type,HappeningNowFragment.this, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                error_message.setVisibility(View.GONE);

                            } else {
                                error_message.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void VOTECHALLENGE(String type, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, list.get(position).getId());
        formBuilder.addFormDataPart(Parameters.ISLIKE, "1");//( 0=>dislike, 1=>like )
        formBuilder.addFormDataPart(Parameters.TYPE, type); //( 0=>voteToCreator, 1=>voteToChallenger )
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VOTECHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (type.equals("0")) {
                               /* animationViewOne.setVisibility(View.VISIBLE);
                                animationViewOne.start();
                                animationViewOne.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewOne.setVisibility(View.INVISIBLE);
                                    }
                                });*/
                                list.get(position).setVotestocreator
                                        (String.valueOf(Integer.parseInt(list.get(position).getVotestocreator()) + 1));
                            } else {
                               /* animationViewTwo.setVisibility(View.VISIBLE);
                                animationViewTwo.start();
                                animationViewTwo.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewTwo.setVisibility(View.INVISIBLE);
                                    }
                                });*/
                                list.get(position).setVotestochallenger
                                        (String.valueOf(Integer.parseInt(list.get(position).getVotestochallenger()) + 1));
                            }

                            if (adapter != null)
                                adapter.notifyDataSetChanged();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void showInformationPop(String type, Integer id) {
        final Dialog dialog1 = new Dialog(getActivity());
        DisplayMetrics display = new DisplayMetrics();
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(true);
        dialog1.setContentView(R.layout.report_popup);
        Objects.requireNonNull(dialog1.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog1.getWindow().setGravity(Gravity.BOTTOM);
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog1.getWindow().setBackgroundDrawableResource(R.drawable.shadow_border_bg_more_rounded);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        TextView mTvSpam = dialog1.findViewById(R.id.mTvSpam);
      /*  TextView mTvNude = dialog1.findViewById(R.id.mTvNude);
        TextView mTvHate = dialog1.findViewById(R.id.mTvHate);
        TextView mTvViolence = dialog1.findViewById(R.id.mTvViolence);
        TextView mTvPolicy = dialog1.findViewById(R.id.mTvPolicy);*/
        TextView tvDone = dialog1.findViewById(R.id.tvDone);


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
        Window window1 = dialog1.getWindow();
        window1.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void GETPATH(String id) {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/"+id+"/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata="";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        }  else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            }  else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                   /* context.startActivity(new Intent(context, ExoPlayerActivity.class)
                            .putExtra("data",pathdata));*/
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}