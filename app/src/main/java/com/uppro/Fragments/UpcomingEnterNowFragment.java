package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.CardActivity;
import com.uppro.Adapters.UpcomingEnterAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.BracketChallengeListModel;
import com.uppro.ModelClasses.BracketChallengeListingModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class UpcomingEnterNowFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.enter_now)
    TextView enter_now;
    @BindView(R.id.total_entered)
    TextView total_entered;
    @BindView(R.id.category_date)
    TextView category_date;
    @BindView(R.id.rules)
    TextView rules;
    @BindView(R.id.layout_top)
    LinearLayout layout_top;
    @BindView(R.id.share)
    ImageView share;


    BracketChallengeListModel challengeListModel = null;

    String challenge_id = "";


    public UpcomingEnterNowFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_enter_now, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        challenge_id = getArguments().getString("challenge_id");
        CHALLENGELISTING();
        return view;
    }

    private void CHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, challenge_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BRACKETCHALLENGEDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();

                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject object = jsonmainObject.getJSONObject("body");

                            challengeListModel = new BracketChallengeListModel();
                            challengeListModel.setId(object.getString("id"));
                            challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                            challengeListModel.setCategoryId(object.optString("categoryId"));
                            challengeListModel.setCompetitors(object.optString("competitors"));
                            challengeListModel.setIsRules(object.optString("isRules"));
                            challengeListModel.setRules(object.optString("rules"));
                            challengeListModel.setTime(object.optString("time"));
                            challengeListModel.setType(object.optString("type"));
                            challengeListModel.setUserId(object.optString("userId"));
                            challengeListModel.setVideo(object.optString("video"));
                            challengeListModel.setStartDate(object.optString("startDate"));
                            challengeListModel.setEndDate(object.optString("endDate"));
                            challengeListModel.setIsEntered(object.optString("isEntered"));
                            challengeListModel.setVideoThumbnail(object.optString("videoThumbnail"));

                            challengeListModel.setJoiningFee(object.optString("joiningFee"));
                            challengeListModel.setIsPaid(object.optString("isPaid"));
                            challengeListModel.setiHavePaid(object.optString("iHavePaid"));//
                            challengeListModel.setiHaveSubmitted(object.optString("iHaveSubmitted"));//
                            challengeListModel.setJoinedChallengersIncludingMe(object.optString("joinedChallengersIncludingMe"));//

                            ArrayList<UserModel> challenge_users_list = new ArrayList<>();

                            for (int j = 0; j < object.getJSONArray("challengeUsers").length(); j++) {
                                JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(j);
                                UserModel userModel = new UserModel();

                                userModel.setUser_id(obj.getJSONObject("challenger").getString("id"));
                                userModel.setName(obj.getJSONObject("challenger").getString("name"));
                                userModel.setEmail(obj.getJSONObject("challenger").getString("email"));
                                userModel.setProfile_pic(obj.getJSONObject("challenger").getString("image"));
                                challenge_users_list.add(userModel);

                            }
                            challengeListModel.setChallenge_users_list(challenge_users_list);


                            if (challengeListModel != null)
                                setdata();


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    mDialog.dismiss();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setdata() {

        layout_top.setVisibility(View.VISIBLE);

        category_date.setText("Bracket : " + challengeListModel.getCategory_name() + "  " + challengeListModel.getStartDate());

        if (challengeListModel.getIsRules().equals("1")) {
            rules.setText("Rules: View Rules");
        } else {
            rules.setText("Rules: No Rules");
        }

        String total_entered___ = String.valueOf(challengeListModel.getChallenge_users_list().size());

        total_entered.setText(total_entered___ + "/" + challengeListModel.getCompetitors());

        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getIsRules().equals("1")) {
                    new IOSDialog.Builder(context)
                            .setTitle("Rules")
                            .setCancelable(false)
                            .setMessage(challengeListModel.getRules()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                            /* .setNegativeButton("Cancel", null)*/.show();
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type2/"+  challengeListModel.getId());
                context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
            }
        });


        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new UpcomingEnterAdapter(context, challengeListModel.getChallenge_users_list()));

        enter_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                util.is_for_pay = false;

                if (challengeListModel.getiHaveSubmitted().equals("1")) {
                    util.IOSDialog(context, "Already Submitted");
                } else {
                    if (!challengeListModel.isEntered.equals("1")) {

                        if (challengeListModel.getIsPaid().equals("1")) {
                            //paid event
                            Intent intent = new Intent(context, CardActivity.class);
                            intent.putExtra("challengeId", challengeListModel.getId());
                            intent.putExtra("joiningFee", challengeListModel.getJoiningFee());
                            // startActivityForResult(intent,200);
                            someActivityResultLauncher.launch(intent);
                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                        } else {
                            //Submitted Bracket Challenge
                            SubmitChallenge();
                        }
                    } else
                        SubmitChallenge();

                }


            }
        });

        if (challengeListModel.getiHaveSubmitted().equals("1")) {
            enter_now.setVisibility(View.VISIBLE);
            enter_now.setText("Submitted");
        } else {
            if (challengeListModel.isEntered.equals("1")) {
                enter_now.setVisibility(View.VISIBLE);
                enter_now.setText("Submit Now");
            } else {
                enter_now.setText(R.string.enter_now);
                enter_now.setVisibility(View.VISIBLE);
            }
        }


    }

    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        //doSomeOperations();
                        Toast.makeText(context, "someActivityResultLauncher", Toast.LENGTH_SHORT).show();
                    }
                }
            });

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void SubmitChallenge() {
        SubmitBracketFragment fragment = new SubmitBracketFragment();
        Bundle bundle = new Bundle();
        bundle.putString("challenge_id", challengeListModel.getId());
        bundle.putString("categoryname", challengeListModel.getCategory_name());
        bundle.putString("rules", challengeListModel.getRules());
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (util.is_for_pay) {
            util.is_for_pay = false;
            getFragmentManager().popBackStack();
        }
    }
}