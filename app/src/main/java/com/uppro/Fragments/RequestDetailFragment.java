package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.ChattngActivity;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.HomeAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncPut;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RequestDetailFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.accept)
    TextView accept;
    @BindView(R.id.decline)
    TextView decline;
    @BindView(R.id.submit)
    TextView submit;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.rules)
    TextView rules;
    @BindView(R.id.start_date)
    TextView start_date;
    @BindView(R.id.end_date)
    TextView end_date;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.play_video)
    ImageView play_video;
    @BindView(R.id.video_lay)
    LinearLayout video_lay;
    @BindView(R.id.how_long)
    TextView how_long;

    String how_long_text = "7";
    boolean is_from_callout = false;

    ChallengeListModel challengeListModel;

    String challenge_id="";
    String text = "";
    String status = "";
    public RequestDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request_detail, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        is_from_callout = getArguments().getBoolean("is_from_callout");

        MainActivity.UNREADNOTIFICATIONCOUNT();
        MainActivity.GETCOUNT();
        setdata();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void setdata() {
        challengeListModel = getArguments().getParcelable("data");

        challenge_id=challengeListModel.getId();
        if (!is_from_callout) {

            accept.setVisibility(View.VISIBLE);
            decline.setVisibility(View.VISIBLE);
            submit.setVisibility(View.GONE);
            end_date.setVisibility(View.GONE);
            how_long.setEnabled(true);
        } else {
            submit.setVisibility(View.VISIBLE);
            if (!challengeListModel.getTimeLeft().equals("0")) {
                end_date.setText("You have " + challengeListModel.getDays() + " days "
                        + challengeListModel.getHours() + " hours, " + challengeListModel.getMinutes()
                        + " minutes, " + challengeListModel.getSeconds() + " seconds to submit video.");
            } else {
                end_date.setText("You have " + challengeListModel.getHours() + " hours, " + challengeListModel.getMinutes()
                        + " minutes, " + challengeListModel.getSeconds() + " seconds to submit video.");
            }

            end_date.setVisibility(View.VISIBLE);
            accept.setVisibility(View.GONE);
            decline.setVisibility(View.GONE);

            how_long.setText(challengeListModel.getTime() + " Hours");
            how_long.setEnabled(false);

            // Glide.with(context).load(challengeListModel.getThumb()).into(image);


        }


        Glide.with(context).load(challengeListModel.getImage()).into(profile_pic);


        name.setText(challengeListModel.getName());
        start_date.setText("Start Date : " + util.convertTimeStampDate(Long.parseLong(challengeListModel.getCreated())));
        category.setText("Category: " + challengeListModel.getCategory_name());


        if (challengeListModel.getIsRules().equals("1")) {
            rules.setText(challengeListModel.getRules());
        } else {
            rules.setText("No rules");
        }
    }

    @OnClick({R.id.back_button, R.id.accept, R.id.decline, R.id.submit, R.id.how_long})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.accept:
                if (ConnectivityReceiver.isConnected()) {
//                    if (how_long_text.isEmpty()) {
//                        util.IOSDialog(context, "Please Select How Long");
//                    } else
                    status="1";
                    ACCEPTDECLINECHALLENGEREQUEST("1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.decline:
                if (ConnectivityReceiver.isConnected()) {
                    status="2";
                    ACCEPTDECLINECHALLENGEREQUEST("2");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.submit:
                if (ConnectivityReceiver.isConnected()) {
                    CreateFragment fragment = new CreateFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("is_challenger", false);
                    bundle.putBoolean("is_to_submit", true);
                    bundle.putParcelable("data", challengeListModel);
                    bundle.putBoolean("back_on", true);
                    fragment.setArguments(bundle);

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
            case R.id.how_long:
                Popup();
                break;
        }
    }

    private void Popup() {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(context, how_long);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.how_long_menu_challenge, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                how_long.setText(item.getTitle());
                if (item.getTitle().equals("1 Day"))
                    how_long_text = "1";
                else if (item.getTitle().equals("2 Days"))
                    how_long_text = "2";
                else if (item.getTitle().equals("3 Days"))
                    how_long_text = "3";
                else if (item.getTitle().equals("4 Days"))
                    how_long_text = "4";
                else if (item.getTitle().equals("5 Days"))
                    how_long_text = "5";
                else if (item.getTitle().equals("6 Days"))
                    how_long_text = "6";
                else if (item.getTitle().equals("7 Days"))
                    how_long_text = "7";
                return true;
            }
        });
        popup.show();
    }


    public void ACCEPTDECLINECHALLENGEREQUEST(String status) {

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challengeListModel.getId());
        formBuilder.addFormDataPart(Parameters.STATUS, status);//( 1=>accepted, 2=>declined)
        if (status.equals("1")) {
            formBuilder.addFormDataPart(Parameters.TIME, how_long_text);
        }
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.ACCEPTDECLINECHALLENGEREQUEST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {


                            if (status.equals("1"))
                                text = "To see the accepted challenges please go to Call out in your profile page.";
                            else
                                text = getString(R.string.challenge_declined_sucessfully);
                            READMESSAGE(challenge_id);
                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void READMESSAGE(String challenge_id) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.MESSAGEIDS, challenge_id);
        formBuilder.addFormDataPart(Parameters.TYPE, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.READMESSAGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            MainActivity.GETCOUNT();
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage(text).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (status.equals("1")) {
                                       /* FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.frame_container, new CalloutFragment());
                                        fragmentTransaction.commit();*/

                                                Intent intent = new Intent(context, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
//                                        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                                            } else
                                                getActivity().getSupportFragmentManager().popBackStack();
                                        }
                                    }).show();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}