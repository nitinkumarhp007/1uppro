package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uppro.Adapters.HomeAdapter;
import com.uppro.Adapters.PastChallengeAdapter;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.whiteelephant.gifplayer.GifView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class PastChallengeFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;

    @BindView(R.id.my_recycler_view)
    RecyclerView my_recycler_view;
    @BindView(R.id.back_button)
    ImageView back_button;
    PastChallengeAdapter adapter;

    ArrayList<ChallengeListModel> list;

    public PastChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_past_challenge, container, false);

        unbinder = ButterKnife.bind(this, view);


        context = getActivity();
        savePref = new SavePref(context);

        inti();


        return view;
    }

    private void inti() {
        list = getArguments().getParcelableArrayList("list");


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });

        adapter = new PastChallengeAdapter(context, list, PastChallengeFragment.this);
        my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        my_recycler_view.setAdapter(adapter);
    }

    public void LIKEVIDEO(String isLike, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, list.get(position).getId());
        formBuilder.addFormDataPart(Parameters.TYPE, list.get(position).getType()); //( 0=>voteToCreator, 1=>voteToChallenger )
        formBuilder.addFormDataPart(Parameters.ISLIKE, isLike);//( 0=>dislike, 1=>like )
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VOTECHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (isLike.equals("0")) {
                                //dislike

                                /*animationViewOne.setVisibility(View.VISIBLE);
                                animationViewOne.start();
                                animationViewOne.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewOne.setVisibility(View.INVISIBLE);
                                    }
                                });*/
                                list.get(position).setComment_count(
                                        (String.valueOf(Integer.parseInt(list.get(position).getComment_count()) - 1)));
                            } else {

                                //like
                              /*  animationViewTwo.setVisibility(View.VISIBLE);
                                animationViewTwo.start();
                                animationViewTwo.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewTwo.setVisibility(View.INVISIBLE);
                                    }
                                });*/
                                list.get(position).setComment_count(
                                        (String.valueOf(Integer.parseInt(list.get(position).getComment_count()) + 1)));
                            }

                            if (adapter != null)
                                adapter.notifyDataSetChanged();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}