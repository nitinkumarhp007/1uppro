package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uppro.Adapters.FollowingAdapter;
import com.uppro.Adapters.FollowingUsersAdapter;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class FollowingUserListFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    FollowingUsersAdapter adapter;

    ArrayList<CategoryModel> list;

    String userId = "";
    boolean is_following = false;

    public FollowingUserListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_following_user_list, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        init();

        return view;
    }

    private void init() {
        userId = getArguments().getString("userId");
        is_following = getArguments().getBoolean("is_following");


        if (!is_following)
            title_text.setText("Following List");
        else
            title_text.setText("Followers List");


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        if (ConnectivityReceiver.isConnected())
            FOLLOWERSUSERLISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }


    private void FOLLOWERSUSERLISTING() {
        String url = "";

        if (is_following)
            url = AllAPIS.FOLLOWERSUSERLISTING;
        else
            url = AllAPIS.FOLLOWINGUSERLISTING;

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.USERID, userId);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, url, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.optString("image"));
                                categoryModel.setIsFollowed(object.optString("isFollowed"));
                                list.add(categoryModel);
                            }

                            if (list.size() > 0) {
                                adapter = new FollowingUsersAdapter(context, list, FollowingUserListFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                error_message.setVisibility(View.GONE);
                            } else {
                                error_message.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void FOLLOWUNFOLLOWUSER_API(String user_id, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FOLLOWUNFOLLOWUSER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (list.get(position).getIsFollowed().equals("1"))
                                list.get(position).setIsFollowed("0");
                            else
                                list.get(position).setIsFollowed("1");

                            adapter.notifyDataSetChanged();

                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}