package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

//import com.appunite.appunitevideoplayer.PlayerActivity;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.ChattngActivity;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.ReportAdapter;
import com.uppro.Adapters.ReportDetailAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.ReportTextModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;
import com.whiteelephant.gifplayer.GifView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class ChallengeDetailFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;

    @BindView(R.id.profile_1_up)
    TextView profile_1_up;
    @BindView(R.id.profile_2_up)
    TextView profile_2_up;
    @BindView(R.id.profile_1_score)
    TextView profile_1_score;
    @BindView(R.id.profile_2_score)
    TextView profile_2_score;
    @BindView(R.id.chat)
    ImageView chat;
    @BindView(R.id.profile_1)
    TextView profile_1;
    @BindView(R.id.profile_2)
    TextView profile_2;
    @BindView(R.id.profile_1_pic)
    ImageView profile_1_pic;
    @BindView(R.id.profile_1_thumb)
    ImageView profile_1_thumb;
    @BindView(R.id.profile_2_thumb)
    ImageView profile_2_thumb;
    @BindView(R.id.profile_2_pic)
    ImageView profile_2_pic;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.end_date)
    TextView end_date;
    @BindView(R.id.rules)
    TextView rules;
    @BindView(R.id.profile_1_layout)
    LinearLayout profile_1_layout;
    @BindView(R.id.video_play)
    ImageView video_play;
    @BindView(R.id.video_play_2)
    ImageView video_play_2;
    @BindView(R.id.join_challenge)
    TextView join_challenge;
    @BindView(R.id.reportOne)
    ImageView reportOne;
    @BindView(R.id.reportTwo)
    ImageView reportTwo;
    @BindView(R.id.scrollable)
    LinearLayout scrollable;
    @BindView(R.id.share)
    ImageView share;
    @BindView(R.id.back_button)
    ImageView back_button;
    private ArrayList<ReportTextModel> list_report_text;
    String challenge_id = "";
    ChallengeListModel challengeListModel = null;
    Dialog dialog1 = null;

    public ChallengeDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_challenge_detail, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("is_from_push", true);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        CallAPI();
        return view;
    }
    private void CallAPI() {
        challenge_id = getArguments().getString("challenge_id");

        if (util.is_for_comment) {
            util.is_for_comment=false;
            CommentFragment fragment = new CommentFragment();

            Bundle bundle = new Bundle();
            bundle.putString("challenge_id", challenge_id);
            fragment.setArguments(bundle);

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else {
            if (ConnectivityReceiver.isConnected())
                CHALLENGEDETAIL();
            else
                util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void CHALLENGEDETAIL() {

        Log.e("challenge_id__", challenge_id);

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, challenge_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHALLENGEDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                Log.e("challenge_id__", "result" + result);
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject object = jsonmainObject.getJSONObject("body");
                            challengeListModel = new ChallengeListModel();
                            challengeListModel.setId(object.getString("id"));
                            challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                            challengeListModel.setCategoryId(object.optString("categoryId"));
                            challengeListModel.setCompetitors(object.optString("competitors"));
                            challengeListModel.setIsRules(object.optString("isRules"));
                            challengeListModel.setRules(object.optString("rules"));
                            challengeListModel.setTime(object.optString("time"));
                            challengeListModel.setType(object.optString("type"));
                            challengeListModel.setUserId(object.optString("userId"));
                            challengeListModel.setVideo(object.optString("video"));
                            challengeListModel.setStart_date(object.optString("startDate"));
                            challengeListModel.setEnd_date(object.optString("endDate"));
                            challengeListModel.setThumb(object.optString("videoThumbnail"));

                            challengeListModel.setVotestocreator(object.optString("votesToCreator"));
                            challengeListModel.setVotestochallenger(object.optString("votesToChallenger"));
                            challengeListModel.setIsvoted(object.optString("isVoted"));

                            challengeListModel.setName(object.getJSONObject("challengeCreator").optString("name"));
                            challengeListModel.setImage(object.getJSONObject("challengeCreator").optString("image"));

                            if (object.getJSONArray("challengeUsers").length() > 0) {
                                JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(0);

                                challengeListModel.setUserId__challenger(obj.getJSONObject("challenger").optString("id"));
                                challengeListModel.setName_challenger(obj.getJSONObject("challenger").optString("name"));
                                challengeListModel.setImage_challenger(obj.getJSONObject("challenger").optString("image"));
                                challengeListModel.setThumb_challenger(obj.optString("videoThumbnail"));
                                challengeListModel.setVideo_challenger(obj.optString("video"));
                            }

                            SetData();

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        Log.e("challenge_id__", "result" + ex.toString());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Log.e("challenge_id__", "result" + ex.toString());
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void SetData() {

        MainActivity.UNREADNOTIFICATIONCOUNT();
        MainActivity.GETCOUNT();


        profile_1.setText(challengeListModel.getName());
        category.setText(challengeListModel.getCategory_name());
        profile_1_score.setText(challengeListModel.getVotestocreator());
        profile_2_score.setText(challengeListModel.getVotestochallenger());
//        reportOne.setVisibility(View.VISIBLE);

        Glide.with(context).load(challengeListModel.getImage()).into(profile_1_pic);
        Glide.with(context).load(challengeListModel.getThumb()).into(profile_1_thumb);

        if (!challengeListModel.getVideo_challenger().isEmpty()) {
            join_challenge.setVisibility(View.INVISIBLE);
            video_play_2.setVisibility(View.VISIBLE);
            profile_2_thumb.setVisibility(View.VISIBLE);
            profile_2.setText(challengeListModel.getName_challenger());
            Glide.with(context).load(challengeListModel.getImage_challenger()).into(profile_2_pic);
            Glide.with(context).load(challengeListModel.getThumb_challenger()).into(profile_2_thumb);
//            reportTwo.setVisibility(View.VISIBLE);
            if (challengeListModel.getEnd_date().isEmpty()) {
                end_date.setText("");
            } else {
                end_date.setText("Ends : " + util.getDateTime(challengeListModel.getEnd_date()));
            }
        } else {
            reportTwo.setVisibility(View.GONE);
            if (challengeListModel.getUserId().equals(savePref.getID())) {
                join_challenge.setVisibility(View.INVISIBLE);
            } else {
                join_challenge.setVisibility(View.VISIBLE);
            }
            video_play_2.setVisibility(View.INVISIBLE);
            profile_2_thumb.setVisibility(View.INVISIBLE);


            if (challengeListModel.getEnd_date().isEmpty()) {
                end_date.setText("");
            } else {
                end_date.setText("Ends : " + util.getDateTime(challengeListModel.getEnd_date()));
            }
        }


        if (challengeListModel.getIsRules().equals("1")) {
            rules.setText("Rules: View Rules");
        } else {
            rules.setText("Rules: No Rules");
        }

        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getIsRules().equals("1")) {
                    new IOSDialog.Builder(context)
                            .setTitle("Rules")
                            .setCancelable(false)
                            .setMessage(challengeListModel.getRules()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                            /* .setNegativeButton("Cancel", null)*/.show();
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, share);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.share_item, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("Share")) {
                            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type1/"+ challenge_id);
                            context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
                        } else {
                            new IOSDialog.Builder(context)
                                    .setTitle("Report")
                                    .setCancelable(false)
                                    .setMessage("Would you like to report challenge?").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setPositiveButton("Report", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    CHALLENGEREPORTTEXTLISTING(challenge_id);
                                    dialog.dismiss();
                                }
                            })
                                    .setNegativeButton("Cancel", null).show();
                        }

                        return true;
                    }
                });
                popup.show();

            }
        });


        profile_1_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getUserId().equals(savePref.getID())) {
                    Swich_Fragment(new ProfileFragment(), challengeListModel.getUserId());
                } else {
                    Swich_Fragment(new OtherProfileFragment(), challengeListModel.getUserId());
                }

            }
        });
        profile_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!challengeListModel.getUserId__challenger().isEmpty()) {
                    if (challengeListModel.getUserId__challenger().equals(savePref.getID())) {
                        Swich_Fragment(new ProfileFragment(), challengeListModel.getUserId__challenger());
                    } else {
                        Swich_Fragment(new OtherProfileFragment(), challengeListModel.getUserId__challenger());
                    }
                }
            }
        });
        join_challenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreateFragment fragment = new CreateFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("is_challenger", false);
                bundle.putBoolean("is_to_submit", true);
                bundle.putParcelable("data", challengeListModel);
                bundle.putBoolean("back_on", true);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        video_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getVideo() != null) {
                    if (!challengeListModel.getVideo().isEmpty()) {


                        if (ConnectivityReceiver.isConnected()) {
                            GETPATH(challengeListModel.getVideo());//0=>voteToCreator
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }

//                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                                .putExtra("data",challengeListModel.getVideo()));

//                        context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
//                                challengeListModel.getVideo(),
//                                " ", 0));
                    }
                }
            }
        });
        video_play_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getVideo_challenger() != null) {
                    if (!challengeListModel.getVideo_challenger().isEmpty()) {

                        if (ConnectivityReceiver.isConnected()) {
                            GETPATH(challengeListModel.getVideo_challenger());//0=>voteToCreator
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }


//                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                                .putExtra("data",challengeListModel.getVideo_challenger()));

//                        context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
//                                challengeListModel.getVideo_challenger(),
//                                " ", 0));
                    }
                }
            }
        });


        profile_1_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!challengeListModel.getVideo_challenger().isEmpty()) {
                    if (ConnectivityReceiver.isConnected()) {
                        VOTECHALLENGE("0");//0=>voteToCreator
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }

            }
        });


        profile_2_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!challengeListModel.getVideo_challenger().isEmpty()) {
                    if (ConnectivityReceiver.isConnected()) {
                        VOTECHALLENGE("1");//1=>voteToChallenger )
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }
            }


        });
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!challengeListModel.getVideo_challenger().isEmpty()) {
                    CommentFragment fragment = new CommentFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("challenge_id", challengeListModel.getId());
                    fragment.setArguments(bundle);

                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }


        });


        reportOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInformationPop("1");
            }

        });

        reportTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInformationPop("2");
            }

        });

        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", challengeListModel.getCategoryId());
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        Log.e("challenge_id__", "result" + "Done");


        scrollable.setVisibility(View.VISIBLE);
    }

    private void Swich_Fragment(Fragment fragment, String user_id) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    public void VOTECHALLENGE(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challenge_id);
        formBuilder.addFormDataPart(Parameters.ISLIKE, "1");//( 0=>dislike, 1=>like )
        formBuilder.addFormDataPart(Parameters.TYPE, type); //( 0=>voteToCreator, 1=>voteToChallenger )
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VOTECHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (type.equals("0")) {


                                profile_1_score.setText(String.valueOf(Integer.parseInt(challengeListModel.getVotestocreator()) + 1));

                               /* animationViewOne.setVisibility(View.VISIBLE);
                                animationViewOne.start();
                                animationViewOne.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewOne.setVisibility(View.INVISIBLE);
                                    }
                                });
                                list.get(position).setVotestocreator
                                        (String.valueOf(Integer.parseInt(list.get(position).getVotestocreator()) + 1));*/
                            } else {


                                profile_2_score.setText(String.valueOf(Integer.parseInt(challengeListModel.getVotestochallenger()) + 1));

                               /* animationViewTwo.setVisibility(View.VISIBLE);
                                animationViewTwo.start();
                                animationViewTwo.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewTwo.setVisibility(View.INVISIBLE);
                                    }
                                });
                                list.get(position).setVotestochallenger
                                        (String.valueOf(Integer.parseInt(list.get(position).getVotestochallenger()) + 1));*/
                            }


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void showInformationPop(String challengeId) {
        dialog1 = new Dialog(getActivity());
        DisplayMetrics display = new DisplayMetrics();
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(true);
        dialog1.setContentView(R.layout.report_popup);
        Objects.requireNonNull(dialog1.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog1.getWindow().setGravity(Gravity.BOTTOM);
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog1.getWindow().setBackgroundDrawableResource(R.drawable.shadow_border_bg_more_rounded);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        TextView tvDone = dialog1.findViewById(R.id.tvDone);
        RecyclerView my_recycler_view = dialog1.findViewById(R.id.my_recycler_view);

        my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        my_recycler_view.setAdapter(new ReportDetailAdapter(context, list_report_text, ChallengeDetailFragment.this, challengeId));


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
        Window window1 = dialog1.getWindow();
        window1.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void REPORTCHALLENGE_API(String challengeId, String id) {
        if (dialog1 != null)
            dialog1.dismiss();
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challengeId);
        formBuilder.addFormDataPart(Parameters.CHALLENGEREPORTTEXTID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.REPORTCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            //util.showToast(context, jsonMainobject.getString("message"));
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("Challenge Reported Successfully").setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                                    .show();
                        } else {
                            if (jsonMainobject.getString("message").equals(util.Invalid_Authorization)) {

                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void CHALLENGEREPORTTEXTLISTING(String challengeId) {
        final ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.LONGITUDE, "jiosdjc");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CHALLENGEREPORTTEXTLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list_report_text = new ArrayList<>();
                if (list_report_text.size() > 0)
                    list_report_text.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbodyArray = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < jsonbodyArray.length(); i++) {
                                JSONObject jsonbody = jsonbodyArray.getJSONObject(i);
                                ReportTextModel reportTextModel = new ReportTextModel();
                                reportTextModel.setId(jsonbody.getString("id"));
                                reportTextModel.setText(jsonbody.getString("text"));
                                list_report_text.add(reportTextModel);
                            }

                            showInformationPop(challengeId);
                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }




    public void GETPATH(String id) {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/"+id+"/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata="";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        }  else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            }  else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                   /* context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                            .putExtra("data",pathdata));*/
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}