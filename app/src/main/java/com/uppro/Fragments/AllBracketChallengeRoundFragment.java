package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Adapters.TabVPagerAdapter;
import com.uppro.ModelClasses.AllBracketChallengeRoundBody;
import com.uppro.ModelClasses.AllBracketChallengeRoundData;
import com.uppro.ModelClasses.AllBracketChallengeRoundModel;
import com.uppro.ModelClasses.BracketChallengeDetailAPINewModel;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncGet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class AllBracketChallengeRoundFragment extends Fragment {

    Context context;
    Unbinder unbinder;
    @BindView(R.id.vpChallenge)
    ViewPager vpChallenge;
    String challenge_id = "";
    //    BracketChallengeListModel bracketChallengeListModel = null;
    BracketChallengeDetailAPINewModel bracketChallengeListModel = null;
    private Map<String, ArrayList<AllBracketChallengeRoundData>> roundMapList;
    private SavePref savePref;

    public AllBracketChallengeRoundFragment() {
        // Required empty public constructor
    }

    // receiver as a global variable in your Fragment class
    private BroadcastReceiver messagesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getExtras() != null) {
                String smsMessageStr = intent.getExtras().getString("SUCCESS");
                if (smsMessageStr != null) {
                    // do what you need with received data
                    getParentFragmentManager().popBackStack();
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_bracket_challenge_round, container, false);

        unbinder = ButterKnife.bind(this, view);

        init();

        return view;
    }

    private void init() {
        context = getActivity();
        savePref = new SavePref(context);
        roundMapList = new HashMap<>();
        ALLHAPPENINGBRACKETCHALLENGELISTING();
    }

    private void ALLHAPPENINGBRACKETCHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.ALLHAPPENINGNOWBRACKETCHALLENGE, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        AllBracketChallengeRoundModel model = new Gson().fromJson(result, AllBracketChallengeRoundModel.class);
                        AllBracketChallengeRoundBody data = model.getBody();
                        roundMapList.clear();
                        String lastRound = "";
                        if (!data.getRound1().isEmpty()) {
                            roundMapList.put("Round 1", data.getRound1());
                            lastRound = "Round 1";
                        }
                        if (!data.getRound2().isEmpty()) {
                            roundMapList.put("Round 2", data.getRound2());
                            lastRound = "Round 2";
                        }
                        if (!data.getRound3().isEmpty()) {
                            roundMapList.put("Round 3", data.getRound3());
                            lastRound = "Round 3";
                        }
                        if (!data.getRound4().isEmpty()) {
                            roundMapList.put("Round 4", data.getRound4());
                            lastRound = "Round 4";
                        }
                        if (!data.getRound5().isEmpty()) {
                            roundMapList.put("Round 5", data.getRound5());
                            lastRound = "Round 5";
                        }
                        if (!data.getRound6().isEmpty()) {
                            roundMapList.put("Round 6", data.getRound6());
                            lastRound = "Round 6";
                        }
                        if (!roundMapList.isEmpty()) {
                            viewPagerAdapter(lastRound);
                        }else {
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("There are not any Bracket Challenges Happening now please Click the Bracket icon to see Up Coming Bracket Challenges").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            getParentFragmentManager().popBackStack();
//                                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                            fragmentTransaction.replace(R.id.frame_container, new UpcomingFragment()());
//                                            fragmentTransaction.commit();
                                        }
                                    }).show();
                        }
                        Log.e("model", model.getMessage());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    mDialog.dismiss();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    void viewPagerAdapter(String lastRound) {
        ArrayList<Fragment> frag = new ArrayList<>();
//        frag.add(new BracketChallengeNewFragment());
//        for (Iterator<String> it = roundMapList.keySet().iterator(); it.hasNext(); ) {
//            String round = it.next();
        Bundle data = new Bundle();
        data.putString("round", lastRound);
//            data.putString("category_name", list.get(0).getCategory_name());
//            data.putString("category_id", list.get(0).getCategoryId());
        data.putParcelableArrayList("round_data", roundMapList.get(lastRound));
        AllBracketChallengeRoundListFragment fragItem = new AllBracketChallengeRoundListFragment();
        fragItem.setArguments(data);
        frag.add(fragItem);
//        }
        vpChallenge.setAdapter(new TabVPagerAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, frag));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            vpChallenge.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//                @Override
//                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
//
//                }
//            });
//        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(messagesReceiver, new IntentFilter(util.NOTIFICATION_GOBACK));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(messagesReceiver);
        }
    }

}