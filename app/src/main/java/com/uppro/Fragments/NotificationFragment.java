package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.ChatlistAdapter;
import com.uppro.Adapters.NotificationlistAdapter;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.ChattingModel;
import com.uppro.ModelClasses.NotificationModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncDELETE;
import com.uppro.parser.GetAsyncGet;
import com.uppro.parser.GetAsyncPut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NotificationFragment extends Fragment {
    Context context;
    private SavePref savePref;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.myRecyclerView)
    RecyclerView myRecyclerView;
    @BindView(R.id.errorMessage)
    TextView errorMessage;
    Unbinder unbinder;
    ArrayList<NotificationModel> list;

    NotificationlistAdapter adapter = null;


    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        if (ConnectivityReceiver.isConnected())
            NOTIFICATIONLISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });


        return view;
    }

    private void NOTIFICATIONLISTING() {
        ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.LONGITUDE, "jiosdjc");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.NOTIFICATIONLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbodyArray = jsonmainObject.getJSONArray("body");
                            Log.e("DATA", "  " + jsonbodyArray.toString());
                            for (int i = 0; i < jsonbodyArray.length(); i++) {
                                JSONObject jsonbody = jsonbodyArray.getJSONObject(i);
                                NotificationModel notificationModel = new NotificationModel();
                                notificationModel.setId(jsonbody.getString("id"));
                                notificationModel.setText(jsonbody.getString("text"));
                                notificationModel.setChallenge_id(jsonbody.getString("challengeId"));
                                notificationModel.setUser_id(jsonbody.getJSONObject("user").getString("id"));
                                notificationModel.setName(jsonbody.getJSONObject("user").getString("name"));
                                notificationModel.setImage(jsonbody.getJSONObject("user").getString("image"));
                                notificationModel.setType(jsonbody.getString("type"));
                                notificationModel.setIsRead(jsonbody.getString("isRead"));
                                notificationModel.setCreated(jsonbody.getString("created"));

                                ChallengeListModel challengeListModel = new ChallengeListModel();
                                if (jsonbody.getString("type").equals("1")) {
                                    JSONObject object = jsonbody.getJSONObject("notificationData");
                                    challengeListModel.setId(object.getJSONObject("challenge").getString("id"));
                                    challengeListModel.setCreated(object.getJSONObject("challenge").getString("created"));
                                    challengeListModel.setCategory_name(object.getJSONObject("challenge").
                                            getJSONObject("category").optString("name"));
                                    //challengeListModel.setCategory_name("Rap");
                                    challengeListModel.setRules(object.getJSONObject("challenge").optString("rules"));
                                    challengeListModel.setIsRules(object.getJSONObject("challenge").optString("isRules"));
                                    challengeListModel.setName(object.getJSONObject("challenge").getJSONObject("challengeCreator").optString("name"));
                                    challengeListModel.setImage(object.getJSONObject("challenge").getJSONObject("challengeCreator").optString("image"));
                                    challengeListModel.setStart_date(object.getJSONObject("challenge").optString("start_date"));
                                    challengeListModel.setEnd_date(object.getJSONObject("challenge").optString("end_date"));
                                }

                                notificationModel.setChallengeListModel(challengeListModel);

                                list.add(notificationModel);
                            }
                            if (list.size() > 0) {
                                adapter = new NotificationlistAdapter(context, list, NotificationFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void READNOTIFICATION_API(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NOTOFICATIONID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.READNOTIFICATION, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {

                            //list.get(position).setIsRead("1");
                            //adapter.notifyDataSetChanged();
                            util.is_for_comment = false;
                            if (list.get(position).getType().equals("1")) {
                                //1= challenged you (redirect to )
                                Swich_Fragment(new RequestDetailFragment(), position);
                            } else if (list.get(position).getType().equals("2")) {
                                //2= accepted your challenge request (redirect to challenge detail)
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("3")) {
                                //3= declined your challenge request (redirect to challenge detail)
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("4")) {
                                //4= submitted the challenge request (redirect to challenge detail)
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("5")) {
                                //5= voted on the challenge (redirect to challenge detail)
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("6")) {
                                //6= voted on the challenge (redirect to challenge detail)
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("7")) {
                                util.is_for_comment = true;
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("8")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("9")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("10")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("12")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("13")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("14")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("15")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("16")) {
                                CallChallengeDetail(list.get(position).getChallenge_id());
                            } else if (list.get(position).getType().equals("17")) {
                                OtherProfileFragment fragment = new OtherProfileFragment();

                                Bundle bundle = new Bundle();
                                bundle.putString("user_id", list.get(position).getUser_id());
                                bundle.putBoolean("back_on", true);
                                fragment.setArguments(bundle);

                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void CallChallengeDetail(String post_id) {
        ChallengeDetailFragment fragment = new ChallengeDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("challenge_id", post_id);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void Swich_Fragment(Fragment fragment, int position) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", list.get(position).getChallengeListModel());
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void DeleteAlert(int position) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setMessage("Are you sure to Delete Notification?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETENOTIFICATION_API(position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETENOTIFICATION_API(int position) {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NOTOFICATIONID, list.get(position).getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.DELETENOTIFICATION, formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response response) {
                mDialog.dismiss();
                try {
                    String result = response.body().string();
                    Log.e("a_result", result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful()) {
                    new IOSDialog.Builder(context)
                            .setCancelable(false)
                            .setMessage("Notification Deleted Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            list.remove(position);
                            adapter.notifyDataSetChanged();
                        }
                    }).show();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


}