package com.uppro.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uppro.Adapters.AllMemberAdapter;
import com.uppro.Adapters.HomeAdapter;
import com.uppro.R;
import com.uppro.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class AllMemberBracketChallengeFragment extends Fragment {


    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    public AllMemberBracketChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_member_bracket_challenge, container, false);

        unbinder = ButterKnife.bind(this, view);


        context = getActivity();
        savePref = new SavePref(context);

/*        AllMemberAdapter adapter = new AllMemberAdapter(context);
        myRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        myRecyclerView.setAdapter(adapter);*/

        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}