package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.AllMemberBracketChallengeActivity;
import com.uppro.Activities.ChattngActivity;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.FollowingAdapter;
import com.uppro.Adapters.ReportUserDetailAdapter;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.ModelClasses.ReportTextModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class OtherProfileFragment extends Fragment {
    Context context;
    private SavePref savePref;

    Unbinder unbinder;
    @BindView(R.id.following_button)
    TextView following_button;
    @BindView(R.id.fanlinear)
    LinearLayout fanlinear;
    @BindView(R.id.dm)
    TextView dm;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    @BindView(R.id.happening_now)
    TextView happening_now;
    @BindView(R.id.wins)
    TextView wins;
    @BindView(R.id.loss)
    TextView loss;
    @BindView(R.id.mTvOtherWins)
    TextView mTvOtherWins;
    @BindView(R.id.mTvOtherLosses)
    TextView mTvOtherLosses;
    @BindView(R.id.bracket_challenges)
    TextView bracket_challenges;
    @BindView(R.id.bracket_challenges_)
    TextView bracket_challenges_;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.level)
    TextView level;
    @BindView(R.id.fan_count)
    TextView fan_count;
    @BindView(R.id.bio)
    TextView bio;
    @BindView(R.id.bioData)
    TextView bioData;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.tiktok)
    ImageView tiktok;
    @BindView(R.id.ig)
    ImageView ig;
    @BindView(R.id.youtube)
    ImageView youtube;
    @BindView(R.id.website)
    ImageView website;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.bioll)
    LinearLayout bioll;
    @BindView(R.id.option)
    ImageView option;

    @BindView(R.id.optionreport)
    ImageView optionreport;

    Dialog dialog1 = null;
    private ArrayList<ReportTextModel> list_report_text;

    String user_id = "", isFollowed = "", tiktok_url = "", ig_url = "", youtube_url = "", website_url = "";

    public OtherProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_other_profile, container, false);

        unbinder = ButterKnife.bind(this, view);


        init();

        return view;
    }

    private void init() {
        context = getActivity();
        savePref = new SavePref(context);

        util.hideKeyboard(getActivity());
        dm.setVisibility(View.VISIBLE);
        user_id = getArguments().getString("user_id");

        if (user_id.contains("@"))
            user_id = user_id.substring(1);


        if (ConnectivityReceiver.isConnected())
            GETPROFILELISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    @OnClick({R.id.following_button, R.id.option, R.id.optionreport, R.id.fanlinear, R.id.back_button, R.id.wins, R.id.loss, R.id.mTvOtherWins, R.id.mTvOtherLosses, R.id.bracket_challenges, R.id.bracket_challenges_, R.id.tiktok, R.id.ig, R.id.youtube, R.id.website, R.id.happening_now, R.id.dm})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.following_button:
                if (ConnectivityReceiver.isConnected())
                    FOLLOWUNFOLLOWUSER_API();
                else
                    util.IOSDialog(context, util.internet_Connection_Error);
                break;
            case R.id.option:
                ShowDialog();
                break;
            case R.id.optionreport:
                new IOSDialog.Builder(context)
                        .setTitle("Report")
                        .setCancelable(false)
                        .setMessage("Would you like to report user?").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Report", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CHALLENGEREPORTTEXTLISTING(user_id);
                        dialog.dismiss();
                    }
                })
                        .setNegativeButton("Cancel", null).show();
                break;
            case R.id.fanlinear:
                if (fan_count.getText().toString().trim().equals("0")) {
                    util.showToast(getActivity(), name.getText().toString().trim() + " don't have any followers right now.");
                } else {
                    Swich_FragmentFollowers(new FollowingUserListFragment(), user_id, true, true);
                }
                break;
            case R.id.happening_now:
                Swich_Fragment(new HappeningNowFragment("1", user_id));
                break;
            case R.id.wins:
                Swich_Fragment(new HappeningNowFragment("2", user_id));
                break;
            case R.id.mTvOtherWins:
                //util.showToast(getActivity(), name.getText().toString() + " don't have wins for now.");

                Swich_Fragment(new HappeningNowFragment("2", user_id));

                break;
            case R.id.loss:
                Swich_Fragment(new HappeningNowFragment("3", user_id));
                break;
            case R.id.mTvOtherLosses:
                // util.showToast(getActivity(), name.getText().toString() + " don't have any losses for now.");
                Swich_Fragment(new HappeningNowFragment("3", user_id));
                break;
            case R.id.bracket_challenges_:
            case R.id.bracket_challenges:
                util.showToast(getActivity(), name.getText().toString() + " don't have bracket challenges for now.");

//                startActivity(new Intent(getActivity(), BracketChallengeNewActivity.class));
//                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;

            case R.id.tiktok:
                if (tiktok_url != null) {
                    if (!tiktok_url.isEmpty())
                        OpenWebpage(tiktok_url);
                } else {
                    util.showToast(getActivity(), "Don't have attached TikTok profile link");
                }
                break;
            case R.id.ig:
                if (ig_url != null) {
                    if (!ig_url.isEmpty())
                        OpenWebpage(ig_url);
                } else {
                    util.showToast(getActivity(), "Don't have attached Instagram profile link");
                }
                break;
            case R.id.youtube:
                if (youtube_url != null) {
                    if (!youtube_url.isEmpty())
                        OpenWebpage(youtube_url);
                } else {
                    util.showToast(getActivity(), "Don't have attached Youtube profile link");
                }
                break;
            case R.id.website:
                if (website_url != null) {
                    if (!website_url.isEmpty())
                        OpenWebpage(website_url);
                } else {
                    util.showToast(getActivity(), "Don't have attached Website link");
                }
                break;
            case R.id.edit_profile:
                break;
            case R.id.dm:
                Intent intent = new Intent(context, ChattngActivity.class);
                intent.putExtra("friend_name", name.getText().toString().trim());
                intent.putExtra("friend_id", user_id);
                context.startActivity(intent);
                break;
            case R.id.back_button:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void Swich_FragmentFollowers(Fragment fragment, String user_id, boolean is_data_pass, boolean is_following) {

        if (is_data_pass) {
            Bundle bundle = new Bundle();
            bundle.putString("userId", user_id);
            bundle.putBoolean("is_following", is_following);
            fragment.setArguments(bundle);
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void Swich_Fragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void GETPROFILELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GETPROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonmainObject.getJSONObject("body");

                            // happennow.setText(body.getString("happenningNowCount"));
                            name.setText(body.getString("name"));
                            fan_count.setText(body.getString("followersCount"));
                            wins.setText(body.optString("winCount"));
                            level.setText("Level : "+body.optString("level"));
                            loss.setText(body.optString("lostCount"));
                            if (body.optString(Parameters.BIO).isEmpty()){
                                bioll.setVisibility(View.GONE);
                            }else {
                                bioll.setVisibility(View.VISIBLE);
                            }
                            bioData.setText(body.optString(Parameters.BIO));
                            // fan_count.setText(body.getString("followingCount"));
                            Glide.with(context).load(body.getString("image")).error(R.drawable.placeholder).into(profile_pic);

                            tiktok_url = body.optString(Parameters.TIKTOK);
                            ig_url = body.optString(Parameters.INSTAGRAM);
                            youtube_url = body.optString(Parameters.YOUTUBE);
                            website_url = body.optString(Parameters.WEBSITE);

                            isFollowed = body.getString("isFollowed");

                            if (isFollowed.equals("1")) {
                                following_button.setText("Following");
//                                dm.setVisibility(View.VISIBLE);
                            } else {
                                following_button.setText("Become a fan");
//                                dm.setVisibility(View.GONE);
                            }

                            if (body.optString("isActive").equals("1")) {
                                status.setText("Active");
                                status.setTextColor(getResources().getColor(R.color.green));
                            } else {
                                status.setText("Retired");
                                status.setTextColor(getResources().getColor(R.color.red));
                            }


                            scrollView.setVisibility(View.VISIBLE);

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void FOLLOWUNFOLLOWUSER_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FOLLOWUNFOLLOWUSER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {

                            if (isFollowed.equals("1")) {
                                isFollowed = "0";
                                following_button.setText("Become a fan");
                                dm.setVisibility(View.VISIBLE);
                                fan_count.setText(String.valueOf(Integer.parseInt(fan_count.getText().toString()) - 1));

                            } else {
                                isFollowed = "1";
                                following_button.setText("Following");
                                dm.setVisibility(View.VISIBLE);
                                fan_count.setText(String.valueOf(Integer.parseInt(fan_count.getText().toString()) + 1));
                            }


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void OpenWebpage(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void ShowDialog() {
        String text = "";

        text = "Block User";

        new IOSDialog.Builder(context)
                //.setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Would you like to block this user?")
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .setNegativeButton(text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (ConnectivityReceiver.isConnected())
                            BLOCK_USER();
                        else
                            util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }).show();
    }

    public void BLOCK_USER() {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BLOCKEDUSERID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BLOCK_USER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("User Blocked Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getParentFragmentManager().popBackStack();

                                }
                            }).show();

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void showInformationPop(String challengeId) {
        dialog1 = new Dialog(getActivity());
        DisplayMetrics display = new DisplayMetrics();
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(true);
        dialog1.setContentView(R.layout.report_popup);
        Objects.requireNonNull(dialog1.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog1.getWindow().setGravity(Gravity.BOTTOM);
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog1.getWindow().setBackgroundDrawableResource(R.drawable.shadow_border_bg_more_rounded);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        TextView tvDone = dialog1.findViewById(R.id.tvDone);
        RecyclerView my_recycler_view = dialog1.findViewById(R.id.my_recycler_view);

        my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        my_recycler_view.setAdapter(new ReportUserDetailAdapter(context, list_report_text, OtherProfileFragment.this, challengeId));


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
        Window window1 = dialog1.getWindow();
        window1.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void REPORTCHALLENGE_API(String challengeId, String id) {
        if (dialog1 != null)
            dialog1.dismiss();
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REPORTEDUSERID, challengeId);
        formBuilder.addFormDataPart(Parameters.CHALLENGEREPORTTEXTID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.REPORTUSER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            //util.showToast(context, jsonMainobject.getString("message"));
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("User Reported Successfully").setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                                    .show();
                        } else {
                            if (jsonMainobject.getString("message").equals(util.Invalid_Authorization)) {

                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void CHALLENGEREPORTTEXTLISTING(String challengeId) {
        final ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.LONGITUDE, "jiosdjc");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CHALLENGEREPORTTEXTLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list_report_text = new ArrayList<>();
                if (list_report_text.size() > 0)
                    list_report_text.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbodyArray = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < jsonbodyArray.length(); i++) {
                                JSONObject jsonbody = jsonbodyArray.getJSONObject(i);
                                ReportTextModel reportTextModel = new ReportTextModel();
                                reportTextModel.setId(jsonbody.getString("id"));
                                reportTextModel.setText(jsonbody.getString("text"));
                                list_report_text.add(reportTextModel);
                            }

                            showInformationPop(challengeId);
                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}