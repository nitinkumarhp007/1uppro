package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.uppro.Adapters.CallOutlistAdapter;
import com.uppro.Adapters.RequestlistAdapter;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CalloutFragment extends Fragment {
    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    ArrayList<ChallengeListModel> list;
    @BindView(R.id.error_message)
    TextView errorMessage;

    public CalloutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_callout, container, false);
        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            ACCEPTEDCHALLENGES();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void ACCEPTEDCHALLENGES() {
        ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.ACCEPTEDCHALLENGELISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                progressDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ChallengeListModel challengeListModel = new ChallengeListModel();
                                challengeListModel.setId(object.getString("id"));
                                challengeListModel.setCreated(object.getString("created"));
                                // challengeListModel.setCategory_name(object.optString("name"));


                                challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                challengeListModel.setCategoryId(object.optString("categoryId"));
                                challengeListModel.setCompetitors(object.optString("competitors"));
                                challengeListModel.setIsRules(object.optString("isRules"));
                                challengeListModel.setRules(object.optString("rules"));
                                challengeListModel.setTime(object.optString("time"));
                                challengeListModel.setType(object.optString("type"));
                                challengeListModel.setUserId(object.optString("userId"));
                                challengeListModel.setVideo(object.optString("video"));
                                challengeListModel.setThumb(object.optString("videoThumbnail"));
                                challengeListModel.setTimeLeft(object.optString("timeLeft"));

                                challengeListModel.setName(object.getJSONObject("challengeCreator").optString("name"));
                                challengeListModel.setImage(object.getJSONObject("challengeCreator").optString("image"));
                                challengeListModel.setDays(object.getJSONObject("getTimeRemaining").optString("days"));
                                challengeListModel.setHours(object.getJSONObject("getTimeRemaining").optString("hours"));
                                challengeListModel.setMinutes(object.getJSONObject("getTimeRemaining").optString("minutes"));
                                challengeListModel.setSeconds(object.getJSONObject("getTimeRemaining").optString("seconds"));
                                challengeListModel.setStart_date(object.optString("start_date"));
                                challengeListModel.setEnd_date(object.optString("end_date"));
                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                CallOutlistAdapter requestlistAdapter = new CallOutlistAdapter(context, list, CalloutFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(requestlistAdapter);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("No Challenge Found");
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}