package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uppro.Adapters.CallOutlistAdapter;
import com.uppro.Adapters.MyChallengesAdapter;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MyChallengeListingFragment extends Fragment {
    Context context;
    private SavePref savePref;

    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    ArrayList<ChallengeListModel> list;
    @BindView(R.id.error_message)
    TextView errorMessage;

    public MyChallengeListingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_challenge_listing, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            MYCHALLENGELISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void MYCHALLENGELISTING() {
        ProgressDialog progressDialog = util.initializeProgress(context);
        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.MYCHALLENGELISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                progressDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ChallengeListModel challengeListModel = new ChallengeListModel();
                                challengeListModel.setId(object.getString("id"));
                                challengeListModel.setCreated(object.getString("created"));
                                // challengeListModel.setCategory_name(object.optString("name"));

                                if (!object.getString("category").equals("null"))
                                    challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                else
                                    challengeListModel.setCategory_name("All");


                                challengeListModel.setCategoryId(object.optString("categoryId"));
                                challengeListModel.setCompetitors(object.optString("competitors"));
                                challengeListModel.setIsRules(object.optString("isRules"));
                                challengeListModel.setRules(object.optString("rules"));
                                challengeListModel.setTime(object.optString("time"));
                                challengeListModel.setType(object.optString("type"));
                                challengeListModel.setUserId(object.optString("userId"));
                                challengeListModel.setVideo(object.optString("video"));
                                challengeListModel.setThumb(object.optString("videoThumbnail"));

                                challengeListModel.setName(object.getJSONObject("challengeCreator").optString("name"));
                                challengeListModel.setImage(object.getJSONObject("challengeCreator").optString("image"));
                                challengeListModel.setStart_date(object.optString("start_date"));
                                challengeListModel.setEnd_date(object.optString("end_date"));
                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                MyChallengesAdapter myChallengesAdapter = new MyChallengesAdapter(context, list, MyChallengeListingFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(myChallengesAdapter);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("No Challenge Found");
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


}