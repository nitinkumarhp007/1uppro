package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Adapters.PastVideosAdapter;
import com.uppro.Adapters.UpcomingAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.BracketChallengeListModel;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class UpcomingFragment extends Fragment {

    Unbinder unbinder;
    Context context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.back_button)
    ImageView back_button;
    private ArrayList<BracketChallengeListModel> list;

    public UpcomingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        init();

        return view;
    }

    private void init() {
        list = new ArrayList<>();
//        UpcomingAdapter adapter = new UpcomingAdapter(context,UpcomingFragment.this);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
//        myRecyclerView.setAdapter(adapter);


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            BRACKETCHALLENGELISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void BRACKETCHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.UPCOMINGBRACKETCHALLENGE, formBody) {
            @Override
            public void getValueParse(String result) {

                if (list.size() > 0)
                    list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                BracketChallengeListModel challengeListModel = new BracketChallengeListModel();
                                challengeListModel.setId(object.getString("id"));
                                challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                challengeListModel.setCategoryId(object.optString("categoryId"));
                                challengeListModel.setCompetitors(object.optString("competitors"));
                                challengeListModel.setIsRules(object.optString("isRules"));
                                challengeListModel.setRules(object.optString("rules"));
                                challengeListModel.setTime(object.optString("time"));
                                challengeListModel.setType(object.optString("type"));
                                challengeListModel.setUserId(object.optString("userId"));
                                challengeListModel.setVideo(object.optString("video"));
                                challengeListModel.setStartDate(object.optString("startDate"));
                                challengeListModel.setEndDate(object.optString("endDate"));
                                challengeListModel.setIsEntered(object.optString("isEntered"));
                                challengeListModel.setVideoThumbnail(object.optString("videoThumbnail"));

                                challengeListModel.setJoiningFee(object.optString("joiningFee"));
                                challengeListModel.setIsPaid(object.optString("isPaid"));
                                challengeListModel.setiHavePaid(object.optString("iHavePaid"));
                                challengeListModel.setiHaveSubmitted(object.optString("iHaveSubmitted"));
                                challengeListModel.setJoinedChallengersIncludingMe(object.optString("joinedChallengersIncludingMe"));

                                ArrayList<UserModel> challenge_users_list = new ArrayList<>();

                                for (int j = 0; j < object.getJSONArray("challengeUsers").length(); j++) {
                                    JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(j);
                                    UserModel userModel = new UserModel();

                                    userModel.setUser_id(obj.getJSONObject("challenger").getString("id"));
                                    userModel.setName(obj.getJSONObject("challenger").getString("name"));
                                    userModel.setEmail(obj.getJSONObject("challenger").getString("email"));
                                    userModel.setProfile_pic(obj.getJSONObject("challenger").getString("image"));
                                    challenge_users_list.add(userModel);

                                }
                                challengeListModel.setChallenge_users_list(challenge_users_list);
                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                mDialog.dismiss();
                                UpcomingAdapter adapter = new UpcomingAdapter(context, UpcomingFragment.this, list);
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);

                                MainActivity.UNREADNOTIFICATIONCOUNT();
                                MainActivity.GETCOUNT();
                            } else {

//                                        /* .setNegativeButton("Cancel", null)*/.show();
                            }


                        } else {
                            mDialog.dismiss();
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    mDialog.dismiss();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}