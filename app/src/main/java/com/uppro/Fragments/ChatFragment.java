package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.uppro.Activities.ChattngActivity;
import com.uppro.Activities.SignInActivity;
import com.uppro.Adapters.ChatlistAdapter;
import com.uppro.Adapters.RequestlistAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.ChattingModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncDELETE;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;


public class ChatFragment extends Fragment {
    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.chats)
    TextView chats;
    @BindView(R.id.requests)
    TextView requests;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.notification_count)
    TextView notification_count;
    @BindView(R.id.notification_count_chat)
    TextView notification_count_chat;
    @BindView(R.id.parent)
    RelativeLayout parent;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.toolbar_top)
    RelativeLayout toolbar_top;
    @BindView(R.id.count_lay)
    RelativeLayout count_lay;
    @BindView(R.id.count_lay_chat)
    RelativeLayout count_lay_chat;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;
    private ProgressDialog progressDialog;
    ArrayList<ChallengeListModel> list;
    ArrayList<ChattingModel> list_chat;

    ChatlistAdapter adapter = null;
    RequestlistAdapter requestlistAdapter = null;

    boolean is_request = false;
    boolean from_profile = false;
    String type = "";
    String messagecount = "";
    String requestcount = "";


    public ChatFragment(String type, boolean from_profile) {
        this.type = type;
        this.from_profile = from_profile;
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        unbinder = ButterKnife.bind(this, view);

        init();

        return view;
    }

    private void init() {
        context = getActivity();
        savePref = new SavePref(context);
        progressDialog = util.initializeProgress(context);

        if (from_profile) {
            toolbar_top.setVisibility(View.VISIBLE);
            toolbar.setVisibility(View.GONE);
        } else {
            toolbar.setVisibility(View.VISIBLE);
            toolbar_top.setVisibility(View.GONE);
        }

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (is_request) {
                    if (requestlistAdapter != null)
                        requestlistAdapter.filter(s.toString().trim());
                } else {
                    if (adapter != null)
                        adapter.filter(s.toString().trim());
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void LAST_CHAT() {
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.TYPE, type);//1=>all_chats, 2=>those by followers or followed
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHATLISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list_chat = new ArrayList<>();
                if (list_chat.size() > 0)
                    list_chat.clear();
                MainActivity.UNREADNOTIFICATIONCOUNT();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbodyArray = jsonmainObject.getJSONObject("body").getJSONArray("rows");
                            Log.e("DATA", "  " + jsonbodyArray.toString());
                            for (int i = 0; i < jsonbodyArray.length(); i++) {
                                JSONObject jsonbody = jsonbodyArray.getJSONObject(i);
                                ChattingModel chattingModel = new ChattingModel();
                                chattingModel.setmessage_id(jsonbody.getString("id"));
                                chattingModel.setMessage(jsonbody.getString("message"));
                                chattingModel.setTimestamp(jsonbody.getString("created"));
                                chattingModel.setType(jsonbody.getString("type"));
//                                if (jsonbody.getString("otherUser")!=null){
                                    chattingModel.setOpponent_name(jsonbody.getJSONObject("otherUser").getString("name"));
                                    chattingModel.setImage(jsonbody.getJSONObject("otherUser").getString("image"));
                                    chattingModel.setUser_id(jsonbody.getJSONObject("otherUser").getString("id"));
//                                }else {
//                                    chattingModel.setOpponent_name("");
//                                    chattingModel.setImage("");
//                                    chattingModel.setUser_id("");
//                                }

                                list_chat.add(chattingModel);
                            }
                            if (list_chat.size() > 0) {
                                adapter = new ChatlistAdapter(context, list_chat, ChatFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);

                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("No Chat Found");
                            }
                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (util.is_chat) {
            is_request = false;
            chats.setTextColor(context.getResources().getColor(R.color.white));
            requests.setTextColor(context.getResources().getColor(R.color.gray_light));

            if (ConnectivityReceiver.isConnected()) {
                LAST_CHAT();
                GETCOUNT();
            }
            else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        } else {
            is_request = true;
            requests.setTextColor(context.getResources().getColor(R.color.white));
            chats.setTextColor(context.getResources().getColor(R.color.gray_light));

            if (ConnectivityReceiver.isConnected()){
                CHALLENGEREQUETS();
            GETCOUNT();}
            else{
            util.IOSDialog(context, util.internet_Connection_Error);
        }
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.chats, R.id.requests})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chats:
                is_request = false;
                chats.setTextColor(context.getResources().getColor(R.color.white));
                requests.setTextColor(context.getResources().getColor(R.color.gray_light));

                if (ConnectivityReceiver.isConnected()){
                    LAST_CHAT();
                GETCOUNT();}
                else{
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
            case R.id.requests:
                is_request = true;
                requests.setTextColor(context.getResources().getColor(R.color.white));
                chats.setTextColor(context.getResources().getColor(R.color.gray_light));

                if (ConnectivityReceiver.isConnected()){
                    CHALLENGEREQUETS();
                    GETCOUNT();}
                else{
            util.IOSDialog(context, util.internet_Connection_Error);
        }

                break;
        }
    }

    private void CHALLENGEREQUETS() {
        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CHALLENGEREQUETS, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                progressDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                ChallengeListModel challengeListModel = new ChallengeListModel();
                                challengeListModel.setId(object.getJSONObject("challenge").getString("id"));
                                challengeListModel.setCreated(object.getString("created"));
                                challengeListModel.setCategory_name(object.getJSONObject("challenge").
                                        getJSONObject("category").optString("name"));
                                //challengeListModel.setCategory_name("Rap");
                                challengeListModel.setRules(object.getJSONObject("challenge").optString("rules"));
                                challengeListModel.setIsRules(object.getJSONObject("challenge").optString("isRules"));
                                challengeListModel.setName(object.getJSONObject("challengeCreator").optString("name"));
                                challengeListModel.setImage(object.getJSONObject("challengeCreator").optString("image"));
                                challengeListModel.setStart_date(object.optString("start_date"));
                                challengeListModel.setEnd_date(object.optString("end_date"));
                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                requestlistAdapter = new RequestlistAdapter(context, list, ChatFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(requestlistAdapter);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);


                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("No Request Found");
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void READMESSAGE(int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.MESSAGEIDS, list_chat.get(position).getMessage_id());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.READMESSAGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {

                            MainActivity.GETCOUNT();

                            util.is_chat = true;
                            util.from_deep = false;
                            util.is_for_bracket = false;
                            Intent intent = new Intent(context, ChattngActivity.class);
                            intent.putExtra("friend_name", list_chat.get(position).getOpponent_name());
                            intent.putExtra("friend_id", list_chat.get(position).getUser_id());
                            context.startActivity(intent);


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void GETCOUNT() {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.MESSAGECOUNT, formBody) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("body");

                        messagecount = String.valueOf(jsonReq.getInt("unreadCount"));
                        requestcount = String.valueOf(jsonReq.getInt("unreadChallengeRequestCount"));

                        if (!requestcount.equals("0")) {
                            count_lay.setVisibility(View.VISIBLE);
                            notification_count.setText(requestcount);
                        }else {
                            count_lay.setVisibility(View.GONE);
                        }

                       if (!messagecount.equals("0")) {
                            count_lay_chat.setVisibility(View.VISIBLE);
                            notification_count_chat.setText(messagecount);
                        }else {
                            count_lay_chat.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}