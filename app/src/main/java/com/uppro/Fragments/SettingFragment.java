package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.BlockedUsersActivity;
import com.uppro.Activities.ChangePasswordActivity;
import com.uppro.Activities.ForgotPasswordActivity;
import com.uppro.Activities.SignInActivity;
import com.uppro.Activities.TermsActivity;
import com.uppro.Activities.WalletActivity;
import com.uppro.Adapters.ChatlistAdapter;
import com.uppro.Adapters.RequestlistAdapter;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncPut;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.RequestBody;


public class SettingFragment extends Fragment {
    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.change_password)
    TextView change_password;
    @BindView(R.id.wallet)
    TextView wallet;
    @BindView(R.id.blocked_user_list)
    TextView blocked_userList;
    @BindView(R.id.my_challenges)
    TextView my_challenges;
    @BindView(R.id.logout)
    TextView logout;

    @BindView(R.id.mBtnReport)
    Button mBtnReport;

    @BindView(R.id.completed)
    Button completed;

    @BindView(R.id.mBtnHelp)
    Button mBtnHelp;

    @BindView(R.id.mBtnVideo)
    Button mBtnVideo;

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.terms_of_use, R.id.rules,R.id.completed, R.id.mBtnPrivacy, R.id.mBtnReport, R.id.mBtnHelp, R.id.mBtnVideo, R.id.change_password,R.id.wallet, R.id.blocked_user_list, R.id.my_challenges, R.id.logout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_password:
                startActivity(new Intent(context, ChangePasswordActivity.class));
//                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.wallet:
                startActivity(new Intent(context, WalletActivity.class));
//                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.blocked_user_list:
                startActivity(new Intent(context, BlockedUsersActivity.class));
//                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.my_challenges:
                Swich_Fragment(new MyChallengeListingFragment(), false);
                break;
            case R.id.completed:
                BracketHappeningNowFragment fragment = new BracketHappeningNowFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", "completed");
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case R.id.terms_of_use:
                getActivity().startActivity(new Intent(context, TermsActivity.class).putExtra("title", "Terms of Use"));
                break;
            case R.id.rules:
                getActivity().startActivity(new Intent(context, TermsActivity.class).putExtra("title", "About 1Uppro & Rules"));
                break;
            case R.id.mBtnPrivacy:
                getActivity().startActivity(new Intent(context, TermsActivity.class).putExtra("title", "Privacy Policy"));
                break;
            case R.id.mBtnReport:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("*/*");
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"1uppro.app@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Report a Problem");

                getActivity().startActivity(Intent.createChooser(intent, "Email via..."));
                break;
            case R.id.mBtnHelp:

                Intent intent1 = new Intent(Intent.ACTION_SENDTO);
                intent1.setType("text/html");
                intent1.setData(Uri.parse("mailto:"));
                intent1.putExtra(Intent.EXTRA_EMAIL, new String[]{"1uppro.app@gmail.com"});
                intent1.putExtra(Intent.EXTRA_SUBJECT, "Help Center");

                getActivity().startActivity(Intent.createChooser(intent1, "Email via..."));


                break;
            case R.id.mBtnVideo:
                Intent intent3 = null;
                try {
                    intent3 = new Intent(Intent.ACTION_VIEW);
                    intent3.setPackage("com.google.android.youtube");
                    intent3.setData(Uri.parse("https://youtube.com/channel/UCVltwng8AbMIoqxS0496TWg"));
                    getActivity().startActivity(intent3);
                } catch (ActivityNotFoundException e) {
                    intent3 = new Intent(Intent.ACTION_VIEW);
                    intent3.setData(Uri.parse("https://youtube.com/channel/UCVltwng8AbMIoqxS0496TWg"));
                    getActivity().startActivity(intent3);
                }
                break;
            case R.id.logout:
                Logout_Alert();
                break;
        }
    }

    private void Logout_Alert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LOGOUT_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.COUNTRY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.clearPreferences();
                            util.showToast(context, "Logout Successfully!");
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
//                            requireActivity().finishAffinity();
//                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            savePref.clearPreferences();
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
//                            requireActivity().finishAffinity();
//                            getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Swich_Fragment(Fragment fragment, boolean is_data_pass) {

        if (is_data_pass) {
            Bundle bundle = new Bundle();
            bundle.putString("userId", savePref.getID());
            fragment.setArguments(bundle);
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


}