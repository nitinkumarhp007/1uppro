package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Adapters.TabVPagerAdapter;
import com.uppro.ModelClasses.BracketChallengeDetailAPINewModel;
import com.uppro.ModelClasses.ChallengeUserModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class HappeningEnteredListFragment extends Fragment {
    String id_challenge="";
    Context context;
    Unbinder unbinder;
    @BindView(R.id.share)
    ImageView share;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.category_date)
    TextView category_date;
    @BindView(R.id.rules)
    TextView rules;
    @BindView(R.id.round_no)
    Button round_no;
    @BindView(R.id.vpChallenge)
    ViewPager vpChallenge;
    String challenge_id = "";
    //    BracketChallengeListModel bracketChallengeListModel = null;
    BracketChallengeDetailAPINewModel bracketChallengeListModel = null;
    private SavePref savePref;
    private ArrayList<BracketChallengeDetailAPINewModel> list;
    private ArrayList<String> roundList;

    public HappeningEnteredListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appening_enteredlist, container, false);

        unbinder = ButterKnife.bind(this, view);


        init();

       /* view.setOnTouchListener(new OnSwipeTouchListener(requireActivity()) {

            @Override
            public void onSwipeRight() {
                Toast.makeText(requireActivity(), "rtl swipe", Toast.LENGTH_SHORT).show ();
            }

            @Override
            public void onSwipeLeft() {
                Toast.makeText(requireActivity(), "ltr swipe", Toast.LENGTH_SHORT).show ();
            }

            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
            }

            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
            }
        });*/

        return view;
    }

    private void init() {
        context = getActivity();
        savePref = new SavePref(context);
        list = new ArrayList<>();
        roundList = new ArrayList<>();
        challenge_id = getArguments().getString("challenge_id");
        CHALLENGELISTING();

        bracketChallengeListModel = getArguments().getParcelable("data");
        round_no.setText("Round 1 in Progress");

        if (bracketChallengeListModel != null) {

            if (bracketChallengeListModel.getEndDate() != null)
                category_date.setText("Bracket : " + bracketChallengeListModel.getCategory_name() + " " + util.getDateTime(bracketChallengeListModel.getEndDate()));

            if (bracketChallengeListModel.getIsRules().equals("1")) {
                rules.setText(R.string.view_rules);
            } else {
                rules.setText(R.string.no_rules_);
            }

            rules.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bracketChallengeListModel.getIsRules().equals("1")) {
                        new IOSDialog.Builder(context)
                                .setTitle("Rules")
                                .setCancelable(false)
                                .setMessage(bracketChallengeListModel.getRules()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                /* .setNegativeButton("Cancel", null)*/.show();
                    }
                }
            });


            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type2/" +id_challenge);
                    context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
                }
            });
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void CHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, challenge_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BRACKETCHALLENGEDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            list.clear();


                            JSONObject object = jsonmainObject.getJSONObject("body");

                            BracketChallengeDetailAPINewModel challengeListModel = new BracketChallengeDetailAPINewModel();
                            challengeListModel.setId(object.getString("id"));
                            id_challenge=object.getString("id");
                            challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                            challengeListModel.setCategory_image(object.getJSONObject("category").getString("image"));
                            challengeListModel.setCategoryId(object.optString("categoryId"));
                            challengeListModel.setCompetitors(object.optString("competitors"));
                            challengeListModel.setIsRules(object.optString("isRules"));
                            challengeListModel.setRules(object.optString("rules"));
                            challengeListModel.setTime(object.optString("time"));
                            challengeListModel.setType(object.optString("type"));
                            challengeListModel.setUserId(object.optString("userId"));
                            challengeListModel.setVideo(object.optString("video"));
                            challengeListModel.setStartDate(object.optString("startDate"));
                            challengeListModel.setEndDate(object.optString("endDate"));
                            challengeListModel.setIsEntered(object.optString("isEntered"));
                            challengeListModel.setVideoThumbnail(object.optString("videoThumbnail"));

                            challengeListModel.setJoiningFee(object.optString("joiningFee"));
                            challengeListModel.setIsPaid(object.optString("isPaid"));
                            challengeListModel.setiHavePaid(object.optString("iHavePaid"));
                            challengeListModel.setiHaveSubmitted(object.optString("iHaveSubmitted"));
                            challengeListModel.setJoinedChallengersIncludingMe(object.optString("joinedChallengersIncludingMe"));


                            if (object.optString("endDate") != null)
                                category_date.setText("Bracket : " + object.getJSONObject("category").getString("name") + " " + util.getDateTime(object.optString("endDate")));

                            if (object.optString("isRules").equals("1")) {
                                rules.setText(R.string.view_rules);
                            } else {
                                rules.setText(R.string.no_rules_);
                            }


                            rules.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (object.optString("isRules").equals("1")) {
                                        new IOSDialog.Builder(context)
                                                .setTitle("Rules")
                                                .setCancelable(false)
                                                .setMessage(object.optString("rules")).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();}
                                                })
                                                /* .setNegativeButton("Cancel", null)*/.show();}}});





                            ArrayList<UserModel> challenge_users_list = new ArrayList<>();

                            for (int j = 0; j < object.getJSONArray("challengeUsers").length(); j++) {
                                JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(j);
                                UserModel userModel = new UserModel();
                                userModel.setUser_id(obj.getJSONObject("challenger").getString("id"));
                                userModel.setName(obj.getJSONObject("challenger").getString("name"));
                                userModel.setEmail(obj.getJSONObject("challenger").getString("email"));
                                userModel.setProfile_pic(obj.getJSONObject("challenger").getString("image"));
                                userModel.setVideo_url(obj.optString("video"));
                                userModel.setVideo_thamb(obj.optString("videoThumbnail"));
                                userModel.setComment_count(obj.optString("votes"));
                                challenge_users_list.add(userModel);
                            }

                            challengeListModel.setChallenge_users_list(challenge_users_list);

                            ArrayList<ChallengeUserModel> challengeUserModels = new ArrayList<>();
                            ArrayList<ChallengeUserModel> challengeUserModelsTwo = new ArrayList<>();
                            ArrayList<ChallengeUserModel> challengeUserModelsThree = new ArrayList<>();
                            ArrayList<ChallengeUserModel> challengeUserModelsFour = new ArrayList<>();
                            ArrayList<ChallengeUserModel> challengeUserModelsFive = new ArrayList<>();
                            ArrayList<ChallengeUserModel> challengeUserModelsSix = new ArrayList<>();
                            JSONObject objChallenges = object.getJSONObject("challengeRoundChallengerPairs");

                            if (objChallenges.has("round6")) {
                                if (objChallenges.getJSONArray("round6") != null || objChallenges.getJSONArray("round6").length() != 0) {
                                    if (objChallenges.getJSONArray("round6").length() > 0)
                                        roundList.add("Round 6");
                                    for (int k = 0; k < objChallenges.getJSONArray("round6").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round6").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();

                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));

                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));

                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));

                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));

                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));


                                        userModel.setVotesContender2(obj.getString("votesContender2"));


                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));

                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));

                                        userModel.setWinsone(obj.getJSONObject("contender1").getString("wins"));
                                        userModel.setWinstwo(obj.getJSONObject("contender2").getString("wins"));
                                        userModel.setLevelone(obj.getJSONObject("contender1").getString("level"));
                                        userModel.setLeveltwo(obj.getJSONObject("contender2").getString("level"));

                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));

                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));

                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModelsSix.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairsSix(challengeUserModelsSix);
                                }
                            }

                            if (objChallenges.has("round5")) {
                                if (objChallenges.getJSONArray("round5") != null || objChallenges.getJSONArray("round5").length() != 0) {
                                    if (objChallenges.getJSONArray("round5").length() > 0)
                                        roundList.add("Round 5");
                                    for (int k = 0; k < objChallenges.getJSONArray("round5").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round5").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();

                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));

                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));

                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));

                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));

                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));


                                        userModel.setVotesContender2(obj.getString("votesContender2"));


                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));

                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));

                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));

                                        userModel.setWinsone(obj.getJSONObject("contender1").getString("wins"));
                                        userModel.setWinstwo(obj.getJSONObject("contender2").getString("wins"));
                                        userModel.setLevelone(obj.getJSONObject("contender1").getString("level"));
                                        userModel.setLeveltwo(obj.getJSONObject("contender2").getString("level"));

                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));

                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModelsFive.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairsFive(challengeUserModelsFive);
                                }
                            }

                            if (objChallenges.has("round4")) {
                                if (objChallenges.getJSONArray("round4") != null || objChallenges.getJSONArray("round4").length() != 0) {
                                    if (objChallenges.getJSONArray("round4").length() > 0)
                                        roundList.add("Round 4");
                                    for (int k = 0; k < objChallenges.getJSONArray("round4").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round4").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();

                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));

                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));

                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));

                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));

                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));


                                        userModel.setVotesContender2(obj.getString("votesContender2"));


                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));

                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));

                                        userModel.setWinsone(obj.getJSONObject("contender1").getString("wins"));
                                        userModel.setWinstwo(obj.getJSONObject("contender2").getString("wins"));
                                        userModel.setLevelone(obj.getJSONObject("contender1").getString("level"));
                                        userModel.setLeveltwo(obj.getJSONObject("contender2").getString("level"));

                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));

                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));

                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModelsFour.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairsFour(challengeUserModelsFour);
                                }
                            }

                            if (objChallenges.has("round3")) {
                                if (objChallenges.getJSONArray("round3") != null || objChallenges.getJSONArray("round3").length() != 0) {
                                    if (objChallenges.getJSONArray("round3").length() > 0)
                                        roundList.add("Round 3");
                                    for (int k = 0; k < objChallenges.getJSONArray("round3").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round3").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();

                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));

                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));

                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));

                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));

                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));


                                        userModel.setVotesContender2(obj.getString("votesContender2"));


                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));

                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));

                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));

                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));

                                        userModel.setWinsone(obj.getJSONObject("contender1").getString("wins"));
                                        userModel.setWinstwo(obj.getJSONObject("contender2").getString("wins"));
                                        userModel.setLevelone(obj.getJSONObject("contender1").getString("level"));
                                        userModel.setLeveltwo(obj.getJSONObject("contender2").getString("level"));

                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModelsThree.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairsThree(challengeUserModelsThree);
                                }

                            }

                            if (objChallenges.has("round2")) {
                                if (objChallenges.getJSONArray("round2") != null || objChallenges.getJSONArray("round2").length() != 0) {
                                    if (objChallenges.getJSONArray("round2").length() > 0)
                                        roundList.add("Round 2");
                                    for (int k = 0; k < objChallenges.getJSONArray("round2").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round2").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();

                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));

                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));

                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));

                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));

                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));


                                        userModel.setVotesContender2(obj.getString("votesContender2"));


                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));

                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));

                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));

                                        userModel.setWinsone(obj.getJSONObject("contender1").getString("wins"));
                                        userModel.setWinstwo(obj.getJSONObject("contender2").getString("wins"));
                                        userModel.setLevelone(obj.getJSONObject("contender1").getString("level"));
                                        userModel.setLeveltwo(obj.getJSONObject("contender2").getString("level"));

                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));

                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModelsTwo.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairsTwo(challengeUserModelsTwo);
                                }

                            }

                            if (objChallenges.has("round1")) {
                                if (objChallenges.getJSONArray("round1").length() != 0) {
                                    if (objChallenges.getJSONArray("round1").length() > 0)
                                        roundList.add("Round 1");
                                    for (int k = 0; k < objChallenges.getJSONArray("round1").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round1").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();
                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));
                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));
                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));
                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));
                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));
                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));
                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));
                                        userModel.setWinsone(obj.getJSONObject("contender1").getString("wins"));
                                        userModel.setWinstwo(obj.getJSONObject("contender2").getString("wins"));
                                        userModel.setLevelone(obj.getJSONObject("contender1").getString("level"));
                                        userModel.setLeveltwo(obj.getJSONObject("contender2").getString("level"));
                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));
                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModels.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairs(challengeUserModels);
                                }
                            }

                            list.add(challengeListModel);

                            if (list.size() > 0) {
                                mDialog.dismiss();
//                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
//                                myRecyclerView.setAdapter(new HappeningEnterAdapter(context, list.get(0).getChallengeRoundChallengerPairsThree()
//                                        , list.get(0).getId(), list.get(0).getCategory_name(), HappeningEnteredListFragment.this));
//                                myRecyclerView.setVisibility(View.VISIBLE);
                                viewPagerAdapter(list);

                            }
                        }
                        mDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mDialog.dismiss();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void GETPATH(String id) {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/" + id + "/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata = "";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        Log.e("path_v", pathdata);

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    /*context.startActivity(new Intent(context, ExoPlayerActivity.class)
                            .putExtra("data", pathdata));*/
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    void viewPagerAdapter(ArrayList<BracketChallengeDetailAPINewModel> list) {
        ArrayList<Fragment> frag = new ArrayList<>();
//        frag.add(new BracketChallengeNewFragment());
        for (int i = (roundList.size() - 1); i >= 0; i--) {
            Bundle data = new Bundle();
            data.putString("round", roundList.get(i));
            data.putString("last_round", roundList.get(0));
            data.putString("category_name", list.get(0).getCategory_name());
            data.putString("category_id", list.get(0).getCategoryId());
            if (roundList.get(i).equalsIgnoreCase("round 1")) {
                data.putParcelableArrayList("round_data", list.get(0).getChallengeRoundChallengerPairs());
            } else if (roundList.get(i).equalsIgnoreCase("round 2")) {
                data.putParcelableArrayList("round_data", list.get(0).getChallengeRoundChallengerPairsTwo());
            } else if (roundList.get(i).equalsIgnoreCase("round 3")) {
                data.putParcelableArrayList("round_data", list.get(0).getChallengeRoundChallengerPairsThree());
            } else if (roundList.get(i).equalsIgnoreCase("round 4")) {
                data.putParcelableArrayList("round_data", list.get(0).getChallengeRoundChallengerPairsFour());
            } else if (roundList.get(i).equalsIgnoreCase("round 5")) {
                data.putParcelableArrayList("round_data", list.get(0).getChallengeRoundChallengerPairsFive());
            } else {
                data.putParcelableArrayList("round_data", list.get(0).getChallengeRoundChallengerPairsSix());
            }
            BracketChallengeNewFragment fragItem = new BracketChallengeNewFragment();
            fragItem.setArguments(data);
            frag.add(fragItem);
        }
        vpChallenge.setAdapter(new TabVPagerAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, frag));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            vpChallenge.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                }
            });
        }
    }

}