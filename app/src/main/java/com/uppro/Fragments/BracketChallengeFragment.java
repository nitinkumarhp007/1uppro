package com.uppro.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.uppro.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class BracketChallengeFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.upcoming_button)
    RelativeLayout upcoming_button;
    @BindView(R.id.happening_button)
    RelativeLayout happening_button;

    //https://documenter.getpostman.com/view/4151607/TzRSh81j
    public BracketChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bracket_challenge, container, false);

        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @OnClick({R.id.upcoming_button, R.id.happening_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.upcoming_button:
                loadFragment(new UpcomingFragment());
                break;
            case R.id.happening_button:

                BracketHappeningNowFragment fragment = new BracketHappeningNowFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type", "happening");
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

//                loadFragment(new BracketHappeningNowFragment());
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}