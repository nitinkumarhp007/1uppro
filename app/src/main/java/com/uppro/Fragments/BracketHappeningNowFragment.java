package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Adapters.HappeningNowAdapter;
import com.uppro.Adapters.HappeningNowBracketAdapter;
import com.uppro.Adapters.UpcomingAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.BracketChallengeDetailAPINewModel;
import com.uppro.ModelClasses.BracketChallengeListModel;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.ChallengeUserModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class BracketHappeningNowFragment extends Fragment {

    Unbinder unbinder;
    Context context;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.error_message)
    TextView error_message;
    //    private ArrayList<ChallengeListModel> list;
    private ArrayList<BracketChallengeDetailAPINewModel> list;
    HappeningNowBracketAdapter adapter = null;


    String type = "", user_id = "", type_screen = "";

    public BracketHappeningNowFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bracket_happening_now, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);
        init();

        return view;
    }

    private void init() {
        // userId = getArguments().getString("userId");

        type_screen = getArguments().getString("type");

        if (type_screen.equals("completed")){
            title.setText("Completed Brackets");
        }else {
            title.setText("Happening Now");
        }

        list = new ArrayList<>();
        adapter = new HappeningNowBracketAdapter(context, BracketHappeningNowFragment.this, list);
        myRecyclerView.setAdapter(adapter);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });


        if (type_screen.equals("completed")){
            if (ConnectivityReceiver.isConnected())
                COMPLETEEDBRACKETCHALLENGELISTING();
            else
                util.IOSDialog(context, util.internet_Connection_Error);
        }else {
            if (ConnectivityReceiver.isConnected())
                HAPPENINGBRACKETCHALLENGELISTING();
            else
                util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void COMPLETEEDBRACKETCHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.COMPLETEDRACKETCHALLENGE, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (list.size() > 0)
                    list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                BracketChallengeDetailAPINewModel challengeListModel = new BracketChallengeDetailAPINewModel();
                                challengeListModel.setId(object.getString("id"));
                                challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                challengeListModel.setCategory_image(object.getJSONObject("category").getString("image"));
                                challengeListModel.setCategoryId(object.optString("categoryId"));
                                challengeListModel.setCompetitors(object.optString("competitors"));
                                challengeListModel.setIsRules(object.optString("isRules"));
                                challengeListModel.setRules(object.optString("rules"));
                                challengeListModel.setTime(object.optString("time"));
                                challengeListModel.setType(object.optString("type"));
                                challengeListModel.setUserId(object.optString("userId"));
                                challengeListModel.setVideo(object.optString("video"));
                                challengeListModel.setStartDate(object.optString("startDate"));
                                challengeListModel.setEndDate(object.optString("endDate"));
                                challengeListModel.setIsEntered(object.optString("isEntered"));
                                challengeListModel.setVideoThumbnail(object.optString("videoThumbnail"));

                                challengeListModel.setJoiningFee(object.optString("joiningFee"));
                                challengeListModel.setIsPaid(object.optString("isPaid"));
                                challengeListModel.setiHavePaid(object.optString("iHavePaid"));
                                challengeListModel.setiHaveSubmitted(object.optString("iHaveSubmitted"));
                                challengeListModel.setJoinedChallengersIncludingMe(object.optString("joinedChallengersIncludingMe"));

                                ArrayList<UserModel> challenge_users_list = new ArrayList<>();

                                for (int j = 0; j < object.getJSONArray("challengeUsers").length(); j++) {
                                    JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(j);
                                    UserModel userModel = new UserModel();

                                    userModel.setUser_id(obj.getJSONObject("challenger").getString("id"));
                                    userModel.setName(obj.getJSONObject("challenger").getString("name"));
                                    userModel.setEmail(obj.getJSONObject("challenger").getString("email"));
                                    userModel.setProfile_pic(obj.getJSONObject("challenger").getString("image"));
                                    userModel.setVideo_url(obj.optString("video"));
                                    userModel.setVideo_thamb(obj.optString("videoThumbnail"));
                                    userModel.setComment_count(obj.optString("votes"));
                                    challenge_users_list.add(userModel);

                                }

                                challengeListModel.setChallenge_users_list(challenge_users_list);

                                ArrayList<ChallengeUserModel> challengeUserModels = new ArrayList<>();


                                JSONObject objChallenges = object.getJSONObject("challengeRoundChallengerPairs");

                                if (objChallenges.getJSONArray("round1").length()!=0){
                                    for (int k = 0; k < objChallenges.getJSONArray("round1").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round1").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();

                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));

                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));

                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));

                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));

                                        userModel.setWinsone(obj.getJSONObject("contender1").getString("wins"));
                                        userModel.setWinstwo(obj.getJSONObject("contender2").getString("wins"));
                                        userModel.setLevelone(obj.getJSONObject("contender1").getString("level"));
                                        userModel.setLeveltwo(obj.getJSONObject("contender2").getString("level"));

                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));


                                        userModel.setVotesContender2(obj.getString("votesContender2"));



                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));

                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));

                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));

                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));

                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModels.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairs(challengeUserModels);
                                }



                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                mDialog.dismiss();
                                myRecyclerView.setVisibility(View.VISIBLE);
                                adapter.notifyDataSetChanged();

//                                MainActivity.UNREADNOTIFICATIONCOUNT();
//                                MainActivity.GETCOUNT();
                            } else {

//                                        /* .setNegativeButton("Cancel", null)*/.show();
                            }


                        } else {
                            mDialog.dismiss();
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    mDialog.dismiss();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void HAPPENINGBRACKETCHALLENGELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.HAPPENINGNOWBRACKETCHALLENGE, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (list.size() > 0)
                    list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                BracketChallengeDetailAPINewModel challengeListModel = new BracketChallengeDetailAPINewModel();
                                challengeListModel.setId(object.getString("id"));
                                challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                                challengeListModel.setCategory_image(object.getJSONObject("category").getString("image"));
                                challengeListModel.setCategoryId(object.optString("categoryId"));
                                challengeListModel.setCompetitors(object.optString("competitors"));
                                challengeListModel.setIsRules(object.optString("isRules"));
                                challengeListModel.setRules(object.optString("rules"));
                                challengeListModel.setTime(object.optString("time"));
                                challengeListModel.setType(object.optString("type"));
                                challengeListModel.setUserId(object.optString("userId"));
                                challengeListModel.setVideo(object.optString("video"));
                                challengeListModel.setStartDate(object.optString("startDate"));
                                challengeListModel.setEndDate(object.optString("endDate"));
                                challengeListModel.setIsEntered(object.optString("isEntered"));
                                challengeListModel.setVideoThumbnail(object.optString("videoThumbnail"));

                                challengeListModel.setJoiningFee(object.optString("joiningFee"));
                                challengeListModel.setIsPaid(object.optString("isPaid"));
                                challengeListModel.setiHavePaid(object.optString("iHavePaid"));
                                challengeListModel.setiHaveSubmitted(object.optString("iHaveSubmitted"));
                                challengeListModel.setJoinedChallengersIncludingMe(object.optString("joinedChallengersIncludingMe"));

                                ArrayList<UserModel> challenge_users_list = new ArrayList<>();

                                for (int j = 0; j < object.getJSONArray("challengeUsers").length(); j++) {
                                    JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(j);
                                    UserModel userModel = new UserModel();

                                    userModel.setUser_id(obj.getJSONObject("challenger").getString("id"));
                                    userModel.setName(obj.getJSONObject("challenger").getString("name"));
                                    userModel.setEmail(obj.getJSONObject("challenger").getString("email"));
                                    userModel.setProfile_pic(obj.getJSONObject("challenger").getString("image"));
                                    userModel.setVideo_url(obj.optString("video"));
                                    userModel.setVideo_thamb(obj.optString("videoThumbnail"));
                                    userModel.setComment_count(obj.optString("votes"));
                                    challenge_users_list.add(userModel);

                                }

                                challengeListModel.setChallenge_users_list(challenge_users_list);

                                ArrayList<ChallengeUserModel> challengeUserModels = new ArrayList<>();


                                JSONObject objChallenges = object.getJSONObject("challengeRoundChallengerPairs");

                                if (objChallenges.getJSONArray("round1").length()!=0){
                                    for (int k = 0; k < objChallenges.getJSONArray("round1").length(); k++) {
                                        JSONObject obj = objChallenges.getJSONArray("round1").getJSONObject(k);
                                        ChallengeUserModel userModel = new ChallengeUserModel();

                                        userModel.setChallengeId(obj.getString("challengeId"));
                                        userModel.setCategory(object.getJSONObject("category").getString("name"));
                                        userModel.setCategoryid(object.getJSONObject("category").getString("id"));
                                        userModel.setStartdate(object.optString("startDate"));
                                        userModel.setEnddate(object.optString("endDate"));
                                        userModel.setRules(object.optString("rules"));
                                        userModel.setUser_id_one(obj.getJSONObject("contender1").getString("id"));
                                        userModel.setUser_id_two(obj.getJSONObject("contender2").getString("id"));

                                        userModel.setRound(obj.getString("round"));
                                        userModel.setRoundWinnerId(obj.getString("roundWinnerId"));

                                        userModel.setVotesContender1(obj.getString("votesContender1"));
                                        userModel.setVotesContender2(obj.getString("votesContender2"));

                                        userModel.setIsVotedContender1(obj.getString("isVotedContender1"));
                                        userModel.setIsVotedContender1(obj.getString("isVotedContender2"));

                                        userModel.setIsVotedChallengerInLast24HoursContender1(obj.getString("isVotedChallengerInLast24HoursContender1"));
                                        userModel.setIsVotedChallengerInLast24HoursContender2(obj.getString("isVotedChallengerInLast24HoursContender2"));


                                        userModel.setVotesContender2(obj.getString("votesContender2"));



                                        userModel.setName_one(obj.getJSONObject("contender1").getString("name"));
                                        userModel.setName_two(obj.getJSONObject("contender2").getString("name"));

                                        userModel.setEmail_one(obj.getJSONObject("contender1").getString("email"));
                                        userModel.setEmail_two(obj.getJSONObject("contender2").getString("email"));

                                        userModel.setProfile_pic_one(obj.getJSONObject("contender1").getString("image"));
                                        userModel.setProfile_pic_two(obj.getJSONObject("contender2").getString("image"));

                                        userModel.setVideo_url_one(obj.optString("contender1Video"));
                                        userModel.setVideo_url_two(obj.optString("contender2Video"));

                                        userModel.setVideo_thamb_one(obj.optString("contender1VideoThumbnail"));
                                        userModel.setVideo_thamb_two(obj.optString("contender2VideoThumbnail"));

                                        challengeUserModels.add(userModel);

                                    }
                                    challengeListModel.setChallengeRoundChallengerPairs(challengeUserModels);
                                }



                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                mDialog.dismiss();
                                myRecyclerView.setVisibility(View.VISIBLE);
                                adapter.notifyDataSetChanged();

//                                MainActivity.UNREADNOTIFICATIONCOUNT();
//                                MainActivity.GETCOUNT();
                            } else {

//                                        /* .setNegativeButton("Cancel", null)*/.show();
                            }


                        } else {
                            mDialog.dismiss();
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    mDialog.dismiss();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void GETPATH(String id) {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/" + id + "/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata = "";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        }  else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            }  else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                   /* context.startActivity(new Intent(context, ExoPlayerActivity.class)
                            .putExtra("data", pathdata));*/
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}