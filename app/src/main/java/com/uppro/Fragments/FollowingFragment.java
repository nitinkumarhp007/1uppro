package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Adapters.FollowingAdapter;
import com.uppro.Adapters.UpcomingAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;
import com.uppro.parser.GetAsyncPut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FollowingFragment extends Fragment {


    Unbinder unbinder;
    Context context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.follow_all)
    TextView follow_all;
    @BindView(R.id.parent)
    LinearLayout parent;
    private ArrayList<CategoryModel> list;
    FollowingAdapter adapter = null;

    private SavePref savePref;

    boolean all_followed = true;

    public FollowingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_following, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        follow_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    if (all_followed)
                        FOLLOWALLCATEGORY_API("2");//2=Unfollow All
                    else
                        FOLLOWALLCATEGORY_API("1");//1=Follow All
                } else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        });

        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if (ConnectivityReceiver.isConnected())
            CATEGORYLISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

        return view;
    }


    private void CATEGORYLISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CATEGORYLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                MainActivity.GETCOUNT();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                MainActivity.UNREADNOTIFICATIONCOUNT();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {


                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.optString("image"));
                                categoryModel.setIsFollowed(object.optString("isFollowed"));

                                if (object.optString("isFollowed").equals("0"))
                                    all_followed = false;

                                list.add(categoryModel);
                            }

                            if (all_followed) {
                                follow_all.setText("Unfollow All");
                            } else {
                                follow_all.setText("Follow All");
                            }


                            if (list.size() > 0) {
                                adapter = new FollowingAdapter(context, list, FollowingFragment.this);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void FOLLOWUNFOLLOWCATEGORY_API(String categoryId, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, categoryId);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FOLLOWUNFOLLOWCATEGORY, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (list.get(position).getIsFollowed().equals("1"))
                                list.get(position).setIsFollowed("0");
                            else
                                list.get(position).setIsFollowed("1");


                            all_followed = true;

                            for (int i = 0; i < list.size(); i++) {
                                if (list.get(i).getIsFollowed().equals("0")) {
                                    all_followed = false;
                                }
                            }

                            if (all_followed) {
                                follow_all.setText("Unfollow All");
                            } else {
                                follow_all.setText("Follow All");
                            }


                            adapter.notifyDataSetChanged();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void FOLLOWALLCATEGORY_API(String type) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TYPE, type); //1=Follow All , 2=Unfollow All
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.FOLLOWALLCATEGORY, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            for (int i = 0; i < list.size(); i++) {
                                list.get(i).setIsFollowed(type);
                            }

                            if (type.equals("1")) {
                                all_followed=true;
                                follow_all.setText("Unfollow All");
                                util.showToast(context, "All categories followed successfully!");
                            } else {
                                all_followed=false;
                                follow_all.setText("Follow All");
                                util.showToast(context, "All categories unfollowed successfully!");
                            }


                            adapter.notifyDataSetChanged();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}