package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Adapters.BracketChallengeVideosAdapter;
import com.uppro.Adapters.PastVideosAdapter;
import com.uppro.Adapters.UpcomingAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.BracketChallengeListModel;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class BracketMainFragment extends Fragment {

    Unbinder unbinder;

    Context context;
    private SavePref savePref;

    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.cl)
    ConstraintLayout cl;
    @BindView(R.id.error_message)
    TextView errorMessage;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.mTvCategoryName)
    TextView mTvCategoryName;
    @BindView(R.id.mTvStartDate)
    TextView mTvStartDate;
    @BindView(R.id.mTvEndDate)
    TextView mTvEndDate;
    @BindView(R.id.mTvChallengers)
    TextView mTvChallengers;
    ArrayList<BracketChallengeListModel> list;

    String challenge_id = "";
    BracketChallengeVideosAdapter adapter = null;

    BracketChallengeListModel challengeListModel = null;
    public BracketMainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_bracket, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        inti();

        return view;
    }

    private void inti() {
        list=new ArrayList<>();
        challenge_id = getArguments().getString("challenge_id");

        myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        adapter = new BracketChallengeVideosAdapter(context, list, BracketMainFragment.this);
        myRecyclerView.setAdapter(adapter);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentFragmentManager().popBackStack();
            }
        });


        if (ConnectivityReceiver.isConnected())
            CHALLENGEDETAIL();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void CHALLENGEDETAIL() {

        Log.e("challenge_id__", challenge_id);

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, challenge_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHALLENGEDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                Log.e("challenge_id__", "result" + result);
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject objectMain = jsonmainObject.getJSONObject("body");

                            JSONArray objectArray = objectMain.getJSONArray("challengeUsers");

                            mTvCategoryName.setText(objectMain.getJSONObject("category").getString("name"));
                            mTvStartDate.setText(util.getDateTime(objectMain.optString("startDate")));
                            mTvEndDate.setText(util.getDateTime(objectMain.optString("endDate")));
                            mTvChallengers.setText(String.valueOf(objectArray.length()) +" / "+ String.valueOf(objectMain.optInt("competitors")));


                            for (int i = 0; i < objectArray.length(); i++) {

                                JSONObject object = objectArray.getJSONObject(i);
                                challengeListModel = new BracketChallengeListModel();

                                challengeListModel.setId(object.getString("id"));


                                challengeListModel.setUserId__challenger(object.getJSONObject("challenger").optString("id"));
                                challengeListModel.setName_challenger(object.getJSONObject("challenger").optString("name"));
                                challengeListModel.setImage_challenger(object.getJSONObject("challenger").optString("image"));
                                challengeListModel.setThumb_challenger(object.optString("videoThumbnail"));
                                challengeListModel.setVideo_challenger(object.optString("video"));
                                challengeListModel.setVotes(String.valueOf(object.optInt("votes")));

                                list.add(challengeListModel);
                            }

                            if (list.size() > 0) {
                                mDialog.dismiss();

                                cl.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.VISIBLE);
                                adapter.notifyDataSetChanged();


                                MainActivity.UNREADNOTIFICATIONCOUNT();
                                MainActivity.GETCOUNT();
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        Log.e("challenge_id__", "result" + ex.toString());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Log.e("challenge_id__", "result" + ex.toString());
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void GETPATH(String id) {

        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, "https://player.vimeo.com/video/" + id + "/config", formBody) {
            @Override
            public void getValueParse(String result) {

                String pathdata = "";
                if (result != null && !result.equalsIgnoreCase("")) {

                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq = jsonmainObject.getJSONObject("request");
                        JSONObject file = jsonReq.getJSONObject("files");

                        JSONArray progress = file.getJSONArray("progressive");
                        JSONObject getFirst = progress.getJSONObject(0);
                        if (getFirst.getString("quality").equals("540p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("480p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("720p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("1080p")) {
                            pathdata = getFirst.getString("url");
                        } else if (getFirst.getString("quality").equals("360p")) {
                            pathdata = getFirst.getString("url");
                        }  else if (getFirst.getString("quality").equals("240p")) {
                            pathdata = getFirst.getString("url");
                        } else {

                            JSONObject getSec = progress.getJSONObject(1);
                            if (getSec.getString("quality").equals("540p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("480p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("1080p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("720p")) {
                                pathdata = getSec.getString("url");
                            } else if (getSec.getString("quality").equals("360p")) {
                                pathdata = getSec.getString("url");
                            }  else if (getSec.getString("quality").equals("240p")) {
                                pathdata = getSec.getString("url");
                            } else {

                                JSONObject getThird = progress.getJSONObject(2);
                                if (getThird.getString("quality").equals("540p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("480p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("1080p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("720p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("360p")) {
                                    pathdata = getThird.getString("url");
                                } else if (getThird.getString("quality").equals("240p")) {
                                    pathdata = getThird.getString("url");
                                } else {
                                    Toast.makeText(context, "Please wait your video will be live soon.", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }

                        context.startActivity(new Intent(context, ExoPlayerActivity.class)
//                        context.startActivity(new Intent(context, VideoPlayerVimeoActivity.class)
//                                .putExtra("data",list.get(position).getVideo()));
                                .putExtra("data", pathdata));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    /*context.startActivity(new Intent(context, ExoPlayerActivity.class)
                            .putExtra("data", pathdata));*/
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }




    public void LIKEVIDEO(String isLike, int position) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challenge_id);
        formBuilder.addFormDataPart(Parameters.CHALLENGERID, list.get(position).getUserId__challenger()); //( 0=>voteToCreator, 1=>voteToChallenger )
        formBuilder.addFormDataPart(Parameters.ISLIKE, isLike);//( 0=>dislike, 1=>like )
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VOTEBRACKETCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
//                            if (isLike.equals("0")) {
                                //dislike

                                /*animationViewOne.setVisibility(View.VISIBLE);
                                animationViewOne.start();
                                animationViewOne.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewOne.setVisibility(View.INVISIBLE);
                                    }
                                });*/
//                                list.get(position).setVotes(
//                                        (String.valueOf(Integer.parseInt(list.get(position).getVotes()) - 1)));
//                            } else {

                                //like
                              /*  animationViewTwo.setVisibility(View.VISIBLE);
                                animationViewTwo.start();
                                animationViewTwo.addOnCompletionListener(new GifView.GifCompletionListener() {
                                    @Override
                                    public void onGifCompletion() {
                                        animationViewTwo.setVisibility(View.INVISIBLE);
                                    }
                                });*/
                                list.get(position).setVotes(
                                        (String.valueOf(Integer.parseInt(list.get(position).getVotes()) + 1)));
//                            }

                            if (adapter != null)
                                adapter.notifyDataSetChanged();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}