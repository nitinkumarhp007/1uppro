package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.abedelazizshe.lightcompressorlibrary.config.Configuration;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.CameraMainActivity;
import com.uppro.Activities.UserListActivity;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class CreateFragment extends Fragment {

    Context context;
    private SavePref savePref;
    Unbinder unbinder;


    @BindView(R.id.challenger_name_layout)
    LinearLayout challenger_name_layout;
    @BindView(R.id.llHowLong)
    LinearLayout llHowLong;
    @BindView(R.id.play_video)
    ImageView play_video;
    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.titleHowLong)
    TextView titleHowLong;
    @BindView(R.id.how_long)
    TextView how_long;
    @BindView(R.id.challenger_name)
    TextView challenger_name;
    @BindView(R.id.rules)
    RadioButton rules;
    @BindView(R.id.no_rules)
    RadioButton no_rules;
    @BindView(R.id.create)
    Button create;
    @BindView(R.id.layout_rules)
    RelativeLayout layout_rules;
    @BindView(R.id.rules_text)
    EditText rules_text;
    @BindView(R.id.videoView)
    SimpleExoPlayerView videoView;

    private ArrayList<CategoryModel> list;

    boolean is_challenger = false;
    boolean is_to_submit = false;
    String category_id = "", user_id = "", how_long_text = "";

    String selectedvideo = "", thumb = "", type = "", challengedUserId = "";

    ChallengeListModel model;

    public CreateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);

        is_challenger = getArguments().getBoolean("is_challenger", false);
        is_to_submit = getArguments().getBoolean("is_to_submit", false);


        if (!is_challenger) {
            type = "0";
            challenger_name_layout.setVisibility(View.GONE);
            llHowLong.setVisibility(View.VISIBLE);
            title_text.setText("Open");


            if (is_to_submit) {
                model = getArguments().getParcelable("data");

                category.setText(model.getCategory_name());
                how_long.setVisibility(View.GONE);
                titleHowLong.setVisibility(View.GONE);
                how_long.setText(model.getTime() + " days");

                if (model.getIsRules().equals("1")) {
                    rules.setChecked(true);
                    rules_text.setText(model.getRules());
                    layout_rules.setVisibility(View.VISIBLE);
                } else {
                    rules.setChecked(false);
                    layout_rules.setVisibility(View.GONE);
                }

                create.setText("Submit");

                category.setEnabled(false);
                how_long.setEnabled(false);
                rules.setEnabled(false);
                no_rules.setEnabled(false);
                rules_text.setEnabled(false);

                title_text.setText("Submit Challenge");

            }

        } else {
            type = "1";
            challenger_name_layout.setVisibility(View.VISIBLE);
            llHowLong.setVisibility(View.GONE);
            title_text.setText("Challenger");
        }

        if (ConnectivityReceiver.isConnected()) {
            CATEGORIES(false);
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        rules.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    layout_rules.setVisibility(View.VISIBLE);
                else
                    layout_rules.setVisibility(View.GONE);
            }
        });


        return view;
    }

    @OnClick({R.id.back_button, R.id.challenger_name, R.id.play_video, R.id.category, R.id.how_long, R.id.image, R.id.create})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.challenger_name:
                Intent intent = new Intent(context, UserListActivity.class);
                startActivityForResult(intent, 200);

//                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.category:
                if (list != null) {
                    Show_Category();
                } else {
                    if (list == null)
                        list = new ArrayList<>();

                    CATEGORIES(true);
                }
                break;
            case R.id.how_long:
                Popup();
                break;
            case R.id.play_video:
                if (selectedvideo != null) {
                    if (!selectedvideo.isEmpty()) {
                        videoView.setVisibility(View.VISIBLE);
                        SimpleExoPlayer mSimpleExoPlayer = ExoPlayerFactory.newSimpleInstance(requireActivity());
                        //  mParentWithMember.visibility = View.VISIBLE
                        //video view initilization and set data source for video view
                        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                                requireActivity(),
                                Util.getUserAgent(requireActivity(), "exoPlayerSample")
                        );
                        ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                                .createMediaSource(Uri.parse(selectedvideo));
                        mSimpleExoPlayer.prepare(mediaSource);
                        videoView.setPlayer(mSimpleExoPlayer);
                        videoView.hideController();

                        videoView.getPlayer().seekTo(0);
                        videoView.getPlayer().setPlayWhenReady(true);


//                        context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
//                                selectedvideo,
//                                " ", 0));
                    } else {
//                        int currentapiVersion = Build.VERSION.SDK_INT;
//                        if (currentapiVersion >= Build.VERSION_CODES.R) {
//                            if (Environment.isExternalStorageManager()) {
//                                Intent intent1 = new Intent(getActivity(), CameraMainActivity.class);
//                                getActivity().startActivityForResult(intent1, 1000001);
//                            } else { //request for the permission
//                                Intent intent23 = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
//                                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
//                                intent23.setData(uri);
//                                getActivity().startActivity(intent23);
//                            }
//                        } else {
                            //below android 11=======
                            Intent intent1 = new Intent(getActivity(), CameraMainActivity.class);
                            getActivity().startActivityForResult(intent1, 1000001);
//                        }
//                        showVideoChooserDialog();
//                        new VideoPicker.Builder(getActivity())
//                                .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
//                                .directory(VideoPicker.Directory.DEFAULT)
//                                .extension(VideoPicker.Extension.MP4)
//                                .enableDebuggingMode(true)
//                                .build();
                    }
                }

                break;
            case R.id.image:

//                int currentapiVersion = Build.VERSION.SDK_INT;
//                if (currentapiVersion >= Build.VERSION_CODES.R) {
//                    if (Environment.isExternalStorageManager()) {
//                        Intent intent1 = new Intent(getActivity(), CameraMainActivity.class);
//                        getActivity().startActivityForResult(intent1, 1000001);
//                    } else { //request for the permission
//                        Intent intent23 = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
//                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
//                        intent23.setData(uri);
//                        getActivity().startActivity(intent23);
//                    }
//                } else {
                    //below android 11=======
                    Intent intent1 = new Intent(getActivity(), CameraMainActivity.class);
                    getActivity().startActivityForResult(intent1, 1000001);
//                }


//                // Check whether has the write settings permission or not.
//                boolean settingsCanWrite = Settings.System.canWrite(getActivity());
//
//                if (!settingsCanWrite) {
//                    // If do not have write settings permission then open the Can modify system settings panel.
//                    Intent intent234 = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
//                    getActivity().startActivity(intent234);
//                } else {

//                }


//                showVideoChooserDialog();
//                new VideoPicker.Builder(getActivity())
//                        .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
//                        .directory(VideoPicker.Directory.DEFAULT)
//                        .extension(VideoPicker.Extension.MP4)
//                        .enableDebuggingMode(true)
//                        .build();


            break;
            case R.id.create:
                if (is_to_submit) {
                    if (ConnectivityReceiver.isConnected()) {
                        if (selectedvideo.isEmpty()) {
                            util.IOSDialog(context, "Please Select Video");
                            create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                        } else {
                            SUBMITCHALLENGE();
                        }
                    } else
                        util.IOSDialog(context, util.internet_Connection_Error);
                } else {
                    CreateTask();
                }
                break;
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    //super.onActivityResult(requestCode, resultCode, data);

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == 1) {
//                File f = new File(Environment.getExternalStorageDirectory()
//                        .toString());
//                for (File temp : f.listFiles()) {
//                    if (temp.getName().equals("temp.mp4")) {
//                        f = temp;
//                        break;
//                    }
//                }
//                String videoPath = f.getAbsolutePath();
//                Log.d("SelectedVideoPathCamera", videoPath);
//                selectedvideo=videoPath;

                Uri videoUri = data.getData();
//                videoView.setVideoURI(videoUri);

                File videofile = new File(videoUri.getPath());

                selectedvideo = videofile.getAbsolutePath().toString();

                Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedvideo, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                thumb = getInsertedPath(video_bitmap, "");
                image.setImageBitmap(video_bitmap);
                // Glide.with(context).load(thumb1).into(image);
                play_video.setImageDrawable(getResources().getDrawable(R.drawable.video_play));
//                    uploadVideo(videoPath);

            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Video.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String videoPath = c.getString(columnIndex);
                c.close();
                Log.d("SelectedVideoPath", videoPath);
                selectedvideo = videoPath;
                Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                thumb = getInsertedPath(video_bitmap, "");
                image.setImageBitmap(video_bitmap);
                // Glide.with(context).load(thumb1).into(image);
                play_video.setImageDrawable(getResources().getDrawable(R.drawable.video_play));
//                    uploadVideo(videoPath);

            } else if (requestCode == 1000001) {
//                Uri selectedImage = data.getData();
//                String[] filePath = { MediaStore.Video.Media.DATA };
//                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath,
//                        null, null, null);
//                c.moveToFirst();
//                int columnIndex = c.getColumnIndex(filePath[0]);
//                String videoPath = c.getString(columnIndex);
//                c.close();
//                Log.d("SelectedVideoPath", videoPath);


                selectedvideo = data.getStringExtra("path");
                Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedvideo, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                thumb = getInsertedPath(video_bitmap, "");
                image.setImageBitmap(video_bitmap);
                // Glide.with(context).load(thumb1).into(image);
                play_video.setImageDrawable(getResources().getDrawable(R.drawable.video_play));
//                    uploadVideo(videoPath);

            }
        }

//        if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
//            List<String> mPaths = data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
//
//            if (mPaths.size() > 0) {
//                selectedvideo = mPaths.get(0);
//                Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedvideo, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
//                thumb = getInsertedPath(video_bitmap, "");
//                image.setImageBitmap(video_bitmap);
//                // Glide.with(context).load(thumb1).into(image);
//                play_video.setImageDrawable(getResources().getDrawable(R.drawable.video_play));
//
//                Log.e("mPathssss", mPaths.get(0));
//                //Your Code
//            }
//
//        }

//        if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
//            List<String> mPaths = data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
//
//            if (mPaths.size() > 0) {
//                compressVideo(Uri.parse(mPaths.get(0)), mPaths.get(0), new File(requireActivity().getCacheDir(), "COMPRESSED_MP4"));
//                String selectedvideo = mPaths.get(0);
//                Bitmap video_bitmap = ThumbnailUtils.createVideoThumbnail(selectedvideo, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
//                thumb = getInsertedPath(video_bitmap, "");
//                image.setImageBitmap(video_bitmap);
//                // Glide.with(context).load(thumb1).into(image);
//                Log.e("mPathssss", mPaths.get(0));
//                //Your Code
//            }
//
//        }

        if (requestCode == 200) {
            if (resultCode == getActivity().RESULT_OK) {
                user_id = data.getStringExtra("user_id");
                challenger_name.setText(data.getStringExtra("name"));
            }
        }
    }


    // function to show a dialog to select video file
    private void showVideoChooserDialog() {

        final CharSequence[] options = {"From Camera", "From Gallery",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Options");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
//                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

//                    File mediaFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "temp.mp4");

                    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(intent, 1);
                    }

//                    Uri fileUri = Uri.fromFile(mediaFile);

//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

//                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.mp4");
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
//                    startActivityForResult(intent, 1);
                } else if (options[item].equals("From Gallery")) {
                    // Intent intent = new
                    // Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    // intent.setType("image/*");
                    // startActivityForResult(Intent.createChooser(intent,
                    // "Select File"),2);

                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("video/*");
                    startActivityForResult(intent, 2);
                    //
                    // Intent photoPickerIntent = new
                    // Intent(Intent.ACTION_PICK);
                    // photoPickerIntent.setType("image/*");
                    // startActivityForResult(photoPickerIntent, 2);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void CreateTask() {
        if (ConnectivityReceiver.isConnected()) {

            if (!is_challenger) {
                //open

                if (category_id.isEmpty()) {
                    util.IOSDialog(context, "Please Select Category");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else if (how_long_text.isEmpty()) {
                    util.IOSDialog(context, "Please Select How Long");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else if (rules.isChecked() && rules_text.getText().toString().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Rules");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else if (selectedvideo.isEmpty()) {
                    util.IOSDialog(context, "Please Select Video");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else {
                    ADDCHALLENGE_API();
                }
            } else {
                //challenger
                if (category_id.isEmpty()) {
                    util.IOSDialog(context, "Please Select Category");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                }/* else if (how_long_text.isEmpty()) {
                    util.IOSDialog(context, "Please Select How Long");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                }*/ else if (challenger_name.getText().toString().isEmpty()) {
                    util.IOSDialog(context, "Please Select Challenger");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else if (rules.isChecked() && rules_text.getText().toString().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Rules");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else if (rules_text.getText().toString().length() > 60) {
                    util.IOSDialog(context, "Rules text can't be more then 60 words");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else if (selectedvideo.isEmpty()) {
                    util.IOSDialog(context, "Please Select Video");
                    create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                } else {
                    ADDCHALLENGE_API();

                }
            }


        } else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void ADDCHALLENGE_API() {
        util.hideKeyboard(getActivity());
        String is_rules = "";
        if (rules.isChecked())
            is_rules = "1";
        else
            is_rules = "0";

        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedvideo.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedvideo.endsWith("mp4") ?
                    MediaType.parse("video/mp4") : MediaType.parse("video/mp4");

            File file = new File(selectedvideo);
            formBuilder.addFormDataPart(Parameters.VIDEO, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        if (!thumb.isEmpty()) {
            final MediaType MEDIA_TYPE = thumb.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(thumb);
            formBuilder.addFormDataPart(Parameters.VIDEOTHUMBNAIL, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.TYPE, type);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDUSERID, user_id);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);
        formBuilder.addFormDataPart(Parameters.COMPETITORS, user_id);
        formBuilder.addFormDataPart(Parameters.TIME, how_long_text);
        formBuilder.addFormDataPart(Parameters.ISRULES, is_rules);// ( 0 => no, 1 => yes )
        formBuilder.addFormDataPart(Parameters.RULES, rules_text.getText().toString());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADDCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.challenge_added_successfully)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    getActivity().finishAffinity();
//                                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            })
                                    .show();

                        } else {
                            create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void SUBMITCHALLENGE() {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedvideo.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedvideo.endsWith("mp4") ?
                    MediaType.parse("video/mp4") : MediaType.parse("video/mp4");

            File file = new File(selectedvideo);
            formBuilder.addFormDataPart(Parameters.VIDEO, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        if (!thumb.isEmpty()) {
            final MediaType MEDIA_TYPE = thumb.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(thumb);
            formBuilder.addFormDataPart(Parameters.VIDEOTHUMBNAIL, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, model.getId());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SUBMITCHALLENGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.challenge_submitted_successfully)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finishAffinity();
//                                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            })
                                    /* .setNegativeButton("Cancel", null)*/.show();

                        } else {
                            create.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void Popup() {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(context, how_long);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.how_long_menu_challenge, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                how_long.setText(item.getTitle());

                if (item.getTitle().equals("1 Day"))
                    how_long_text = "1";
                else if (item.getTitle().equals("2 Days"))
                    how_long_text = "2";
                else if (item.getTitle().equals("3 Days"))
                    how_long_text = "3";
                else if (item.getTitle().equals("4 Days"))
                    how_long_text = "4";
                else if (item.getTitle().equals("5 Days"))
                    how_long_text = "5";
                else if (item.getTitle().equals("6 Days"))
                    how_long_text = "6";
                else if (item.getTitle().equals("7 Days"))
                    how_long_text = "7";
                return true;
            }
        });
        popup.show();
    }


    private void CATEGORIES(boolean flag) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.CATEGORYLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.optString("image"));
                                list.add(categoryModel);
                            }

                           /* if (list.size() > 0) {
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId("0");
                                categoryModel.setName("Add All");
                                list.add(0, categoryModel);
                            }*/


                            if (flag) {
                                Show_Category();
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Show_Category() {
        // setup the alert builder
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Choose an Category");

        /*ArrayList to Array Conversion */
        String[] array = new String[list.size()];
        for (int j = 0; j < list.size(); j++) {
            array[j] = list.get(j).getName();


        }


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                category.setText(list.get(which).getName());
                category_id = list.get(which).getId();
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public String getInsertedPath(Bitmap bitmap, String path) {
        String strMyImagePath = null;
        File mFolder = new File(Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_name)), "");

        if (!mFolder.exists()) {
            mFolder.mkdir();
        }

        String s = System.currentTimeMillis() / 1000 + ".jpg";
        File f = new File(mFolder.getAbsolutePath(), s);
        Log.e("creating", "name " + f.getName());
        strMyImagePath = f.getAbsolutePath();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;
    }


    void compressVideo(Uri uri, String path, File desFile) {
        final ProgressDialog mDialog = util.initializeProgress2(context, "Compressing video");

        VideoCompressor.start(
                requireActivity(), // => This is required if srcUri is provided. If not, pass null.
                uri, // => Source can be provided as content uri, it requires context.
                path, // => This could be null if srcUri and context are provided.
                desFile.getPath(),
                new CompressionListener() {
                    @Override
                    public void onStart() {
                        mDialog.show();
                        // Compression start
                        Log.e("onStart -> ", "onStart");

                    }

                    @Override
                    public void onSuccess() {
                        // On Compression success
                        mDialog.dismiss();
                        Log.e("Compression -> ", "Completed");
                        int file_size = Integer.parseInt(String.valueOf((desFile.length() / 1024) / 1024));
                        Log.e("Compression -> ", String.valueOf(file_size));
                        selectedvideo = desFile.getAbsolutePath();
                        play_video.setImageDrawable(getResources().getDrawable(R.drawable.video_play));
//                        context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
//                                selectedvideo,
//                                " ", 0));


                        //                        videoView.setVisibility(View.VISIBLE);
//                        SimpleExoPlayer mSimpleExoPlayer = ExoPlayerFactory.newSimpleInstance(requireActivity());
//                        //  mParentWithMember.visibility = View.VISIBLE
//                        //video view initilization and set data source for video view
//                        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
//                                requireActivity(),
//                                Util.getUserAgent(requireActivity(), "exoPlayerSample")
//                        );
//                        ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
//                                .createMediaSource(Uri.parse(selectedvideo));
//                        mSimpleExoPlayer.prepare(mediaSource);
//                        videoView.setPlayer(mSimpleExoPlayer);
//                        videoView.hideController();
//
//                        videoView.getPlayer().seekTo(0);
//                        videoView.getPlayer().setPlayWhenReady(true);
//                        play_video.setImageDrawable(getResources().getDrawable(R.drawable.video_play));

//                        if (file_size > 200) {
//                            showToast(baseActivity!!.getString(R.string.please_select_video_file_upto_200MB))
//                        } else {
//                            sendFutureVideo(desFile.absolutePath)
//                            //  populateVideo(requestCode, data, desFile.absolutePath)
//                        }
                    }

                    @Override
                    public void onFailure(String failureMessage) {
                        // On Failure
                        mDialog.dismiss();
                        Log.e("onFailure -> ", "onFailure");

                    }

                    @Override
                    public void onProgress(float v) {
                        // Update UI with progress value
//                        runOnUiThread(new Runnable() {
//                            public void run() {
//                                progress.setText(progressPercent + "%");
//                                progressBar.setProgress((int) progressPercent);
//                            }
//                        });
                    }

                    @Override
                    public void onCancelled() {
                        mDialog.dismiss();
                        // On Cancelled
                        Log.e("onCancelled -> ", "onCancelled");

                    }
                }, new Configuration(
                        VideoQuality.VERY_HIGH,
                        false,
                        true,
                        null,
                        null,
                        null
                )
        );
    }


}