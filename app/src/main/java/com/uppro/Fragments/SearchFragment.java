package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.uppro.Adapters.FollowingAdapter;
import com.uppro.Adapters.SearchUsersAdapter;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class SearchFragment extends Fragment {

    Unbinder unbinder;
    Context context;
    SavePref savePref;
    @BindView(R.id.search_bar)
    EditText searchBar;

    @BindView(R.id.my_recycler_view)
    RecyclerView my_recycler_view;

    @BindView(R.id.error_message)
    TextView error_message;
    ArrayList<CategoryModel> list;
    SearchUsersAdapter adapter;



    Handler handler = new Handler();


    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        unbinder = ButterKnife.bind(this, view);

        context = getActivity();
        savePref = new SavePref(context);
        list = new ArrayList<>();
        adapter = new SearchUsersAdapter(context, list, SearchFragment.this);
        my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        my_recycler_view.setAdapter(adapter);

        searchBar.addTextChangedListener(new TextWatcher() {
            Handler handler = new Handler();

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                list.clear();
                adapter.notifyDataSetChanged();
                if (searchBar.getText().toString().trim().isEmpty()){

                }else {
                    SEARCH(searchBar.getText().toString().trim());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


//        searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    SEARCH(v.getText().toString().trim());
//                    return true;
//                }
//                return false;
//            }
//        });

        MainActivity.UNREADNOTIFICATIONCOUNT();
        MainActivity.GETCOUNT();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!searchBar.getText().toString().isEmpty()) {
            error_message.setVisibility(View.GONE);
            SEARCH(searchBar.getText().toString().trim());
        }
    }

    private void SEARCH(String user_name) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.KEYWORD, user_name);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SEARCH, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {

                if (list.size() > 0)
                    list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(object.getString("id"));
                                categoryModel.setName(object.getString("name"));
                                categoryModel.setImage(object.optString("image"));
                                categoryModel.setIsFollowed(object.optString("isFollowed"));
                                list.add(categoryModel);
                            }


                            if (list.size() > 0) {
                               adapter.notifyDataSetChanged();

                                my_recycler_view.setVisibility(View.VISIBLE);
                                error_message.setVisibility(View.GONE);
                            } else {
                                error_message.setVisibility(View.VISIBLE);
                                my_recycler_view.setVisibility(View.GONE);

                                error_message.setText(R.string.no_user_found);

                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}