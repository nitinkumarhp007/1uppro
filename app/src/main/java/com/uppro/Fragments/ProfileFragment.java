package com.uppro.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uppro.Activities.AllMemberBracketChallengeActivity;
import com.uppro.Activities.BracketChallengeNewActivity;
import com.uppro.Activities.OTPActivity;
import com.uppro.Activities.UpdateProfileActivity;
import com.uppro.MainActivity;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class ProfileFragment extends Fragment {
    Unbinder unbinder;
    Context context;
    @BindView(R.id.following_button)
    TextView following_button;
    @BindView(R.id.settings)
    TextView settings;
    @BindView(R.id.edit_profile)
    ImageView edit_profile;
    @BindView(R.id.happening_button)
    LinearLayout happening_button;
    @BindView(R.id.bracket_challenges)
    TextView bracket_challenges;
    @BindView(R.id.dm)
    TextView dm;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.profile_pic)
    ImageView profile_pic;
    @BindView(R.id.fan_count)
    TextView fan_count;
    @BindView(R.id.bracket_challenges_)
    TextView bracket_challenges_;
    @BindView(R.id.bio)
    TextView bio;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.fans)
    LinearLayout fans;
    @BindView(R.id.tiktok)
    ImageView tiktok;
    @BindView(R.id.ig)
    ImageView ig;
    @BindView(R.id.youtube)
    ImageView youtube;
    @BindView(R.id.website)
    ImageView website;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.happennow_text)
    TextView happennow_text;

    @BindView(R.id.wins)
    TextView wins;

    @BindView(R.id.llCallOut)
    LinearLayout llCallOut;

    @BindView(R.id.mTvCallOut)
    TextView mTvCallOut;


    @BindView(R.id.mTvWinTitle)
    TextView mTvWinTitle;

    @BindView(R.id.mTvLossTitle)
    TextView mTvLossTitle;

    @BindView(R.id.loss)
    TextView loss;
    @BindView(R.id.level)
    TextView level;
    @BindView(R.id.bioData)
    TextView bioData;


    @BindView(R.id.happennow)
    TextView happennow;


    private SavePref savePref;

    String tiktok_url = "", ig_url = "", youtube_url = "", website_url = "";

    boolean back_on = false;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        unbinder = ButterKnife.bind(this, view);


        context = getActivity();
        savePref = new SavePref(context);

        back_on = getArguments().getBoolean("back_on", false);
        MainActivity.GETCOUNT();
        if (back_on)
            back_button.setVisibility(View.VISIBLE);
        else
            back_button.setVisibility(View.GONE);



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            GETPROFILELISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void GETPROFILELISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.ID, savePref.getID());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GETPROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                MainActivity.UNREADNOTIFICATIONCOUNT();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonmainObject.getJSONObject("body");

                            happennow.setText(body.getString("happenningNowCount"));
                            name.setText(body.getString("name"));
                            fan_count.setText(body.getString("followersCount"));
                            mTvCallOut.setText(body.optString("acceptedChallengesCount"));
                            wins.setText(body.optString("winCount"));
                            loss.setText(body.optString("lostCount"));
                            level.setText("Level : "+body.optString("level"));
                            if (body.optString(Parameters.BIO).isEmpty()){
                                bioData.setText("Not Available");
                            }else {
                                bioData.setText(body.optString(Parameters.BIO));
                            }

                            Glide.with(context).load(body.getString("image")).error(R.drawable.placeholder).into(profile_pic);

                            tiktok_url = body.optString(Parameters.TIKTOK);
                            ig_url = body.optString(Parameters.INSTAGRAM);
                            youtube_url = body.optString(Parameters.YOUTUBE);
                            website_url = body.optString(Parameters.WEBSITE);

                            if (body.optString("isActive").equals("1")) {
                                status.setText("Active");
                                status.setTextColor(getResources().getColor(R.color.green));
                            } else {
                                status.setText("Retired");
                                status.setTextColor(getResources().getColor(R.color.red));


                            }

                            scrollView.setVisibility(View.VISIBLE);

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @OnClick({R.id.back_button, R.id.following_button, R.id.happennow_text, R.id.wins, R.id.mTvWinTitle, R.id.loss, R.id.mTvLossTitle, R.id.llCallOut, R.id.fans, R.id.tiktok, R.id.ig, R.id.youtube, R.id.website, R.id.dm, R.id.bracket_challenges, R.id.bracket_challenges_, R.id.settings, R.id.edit_profile, R.id.happening_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.following_button:
                Swich_Fragment(new FollowingUserListFragment(), true, false);
                break;
            case R.id.wins:
                Swich_Fragment(new HappeningNowFragment("2", savePref.getID()), false, false);
                break;
            case R.id.mTvWinTitle:
                // util.showToast(getActivity(),"You don't have any wins right now.");
                Swich_Fragment(new HappeningNowFragment("2", savePref.getID()), false, false);
                break;
            case R.id.loss:
                Swich_Fragment(new HappeningNowFragment("3", savePref.getID()), false, false);
                break;
            case R.id.mTvLossTitle:
                // util.showToast(getActivity(),"You don't have any losses right now.");
                Swich_Fragment(new HappeningNowFragment("3", savePref.getID()), false, false);
                break;
            case R.id.happennow_text:
                Swich_Fragment(new HappeningNowFragment("1", savePref.getID()), false, false);
                break;
            case R.id.llCallOut:
                //util.showToast(getActivity(),"You don't have any call out right now.");
                Swich_Fragment(new CalloutFragment(), false, false);
                break;
            case R.id.fans:
                Swich_Fragment(new FollowingUserListFragment(), true, true);
                break;
            case R.id.settings:
                Swich_Fragment(new SettingFragment(), false, false);
                break;
            case R.id.tiktok:
                if (tiktok_url != null) {
                    if (!tiktok_url.isEmpty())
                        OpenWebpage(tiktok_url);
                }
                break;
            case R.id.ig:
                if (ig_url != null) {
                    if (!ig_url.isEmpty())
                        OpenWebpage(ig_url);
                }
                break;
            case R.id.youtube:
                if (youtube_url != null) {
                    if (!youtube_url.isEmpty())
                        OpenWebpage(youtube_url);
                }
                break;
            case R.id.website:
                if (website_url != null) {
                    if (!website_url.isEmpty())
                        OpenWebpage(website_url);
                }
                break;
            case R.id.edit_profile:
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
//                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.bracket_challenges:
            case R.id.bracket_challenges_:
                util.showToast(getActivity(), "You don't have any bracket challenges right now.");

//                startActivity(new Intent(getActivity(), BracketChallengeNewActivity.class));
//                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            /*case R.id.bracket_challenges_:
                startActivity(new Intent(getActivity(), BracketChallengeNewActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;*/
            case R.id.dm:
                util.is_chat=true;
                Swich_Fragment(new ChatFragment("2",true), false, false);
                //MainActivity.navigation.setSelectedItemId(R.id.navigation_message);
                break;
            case R.id.happening_button:
                util.showToast(getActivity(), "There are not any competitions happening now.");
//                Swich_Fragment(new HappeningNowFragment(), false, false);
                break;
        }
    }

    private void Swich_Fragment(Fragment fragment, boolean is_data_pass, boolean is_following) {

        if (is_data_pass) {
            Bundle bundle = new Bundle();
            bundle.putString("userId", savePref.getID());
            bundle.putBoolean("is_following", is_following);
            fragment.setArguments(bundle);
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void OpenWebpage(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

}