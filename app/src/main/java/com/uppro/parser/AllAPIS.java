package com.uppro.parser;


public class AllAPIS {

    /* For Sanbox DB*/
//    public static final String BASE_URL = "http://3.20.218.227:9200/api/";

    /* For Live DB*/
//    public static final String BASE_URL = "http://3.20.218.227:9300/api/";
//    public static final String BASE_URL = "http://api.1uppro.com/api/";
    public static final String BASE_URL = "http://3.20.218.227:9200/api/";
    public static final String STRIPE_URL = "http://api.1uppro.com/api/";
    public static final String PLACE_URL = "https://maps.googleapis.com/maps/api/";


    public static final String USERLOGIN = BASE_URL + "login";
    public static final String USER_SIGNUP = BASE_URL + "signup";
    public static final String VERIFY_OTP = BASE_URL + "verifyOtp";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static final String LOGOUT = BASE_URL + "logout";
    public static final String CHANGEPASSWORD = BASE_URL + "changePassword";
    public static final String EDIT_PROFILE = BASE_URL + "editProfile";
    public static final String PRIVACYPOLICY = BASE_URL + "privacyPolicy";
    public static final String TERMSANDCONDITIONS = BASE_URL + "termsAndConditions";

    public static final String CATEGORYLISTING = BASE_URL + "CategoryListing";
    public static final String GETPROFILE = BASE_URL + "getProfile";
    public static final String ADDCHALLENGE = BASE_URL + "addChallenge";
    public static final String CHALLENGELISTING = BASE_URL + "challengeListing";
    public static final String MYCHALLENGELISTING = BASE_URL + "myChallengeListing";
    public static final String PASTVIDEOS = BASE_URL + "pastVideos";
    public static final String CHALLENGEVIDEOS = BASE_URL + "challengeVideo";
    public static final String USERLISTING = BASE_URL + "userListing";
    public static final String CHALLENGEREQUETS = BASE_URL + "challengeRequests";
    public static final String ACCEPTDECLINECHALLENGEREQUEST = BASE_URL + "acceptDeclineChallengeRequest";
    public static final String ACCEPTEDCHALLENGELISTING = BASE_URL + "acceptedChallengeListing";
    public static final String FOLLOWUNFOLLOWCATEGORY = BASE_URL + "followUnfollowCategory";
    public static final String FOLLOWALLCATEGORY = BASE_URL + "followAllCategory";
    public static final String FOLLOWINGUSERLISTING = BASE_URL + "followingListing";
    public static final String FOLLOWERSUSERLISTING = BASE_URL + "followerListing";
    public static final String CHALLENGECOMMENTSUGGESTIONLISTING = BASE_URL + "challengeCommentSuggestionListing";
    public static final String FOLLOWUNFOLLOWUSER = BASE_URL + "followUnfollowUser";

    public static final String UPCOMINGBRACKETCHALLENGE = BASE_URL + "upcomingBracketChallengeListing";
    public static final String HAPPENINGNOWBRACKETCHALLENGE = BASE_URL + "getHappeningNowBracketChallengeListing";
    public static final String COMPLETEDRACKETCHALLENGE = BASE_URL + "getCompletedBracketChallengeListing";
    public static final String ALLHAPPENINGNOWBRACKETCHALLENGE = BASE_URL + "getHappeningNowBracketChallengeListingByRound";
    public static final String SUBMITBRACKETCHALLENGE = BASE_URL + "submitBracketChallenge";
    public static final String BRACKETCHALLENGEDETAIL = BASE_URL + "bracketChallengeDetail";
    public static final String BRACKETCHALLENGELISTING = BASE_URL + "bracketChallengeListing";
    public static final String SUBMITCHALLENGE = BASE_URL + "submitChallenge";
    public static final String VOTECHALLENGE = BASE_URL + "voteChallenge";
    public static final String VOTEBRACKETCHALLENGE = BASE_URL + "voteBracketChallenge";
    public static final String PAIDBRACKETCHALLENGEPAYMENT = BASE_URL + "paidBracketChallengePayment";

    public static final String CHATLISTING = BASE_URL + "chatListing";
    public static final String CHATMESSAGELIST = BASE_URL + "chatMessageListing";
    public static final String SEND_MESSAGE = BASE_URL + "sendMessage";
    public static final String DELETE_MESSAGE = BASE_URL + "sendMessage";
    public static final String DELETE_CHAT_MESSAGE = BASE_URL + "deleteChatMessage";//chatMessageId/delete method

    public static final String ADDCHALLENGECOMMENT = BASE_URL + "addChallengeComment";
    public static final String CHALLENGECOMMENTLIST = BASE_URL + "challengeCommentListing";

    public static final String BLOCK_USER = BASE_URL + "blockUser";
    public static final String UNBLOCK_USER = BASE_URL + "unblockUser";
    public static final String BLOCK_LISTING = BASE_URL + "blockedUsersListing";
    public static final String SEARCH = BASE_URL + "searchUserListing";
    public static final String GETHAPPENNINGNOW = BASE_URL + "getHappenningNow";

    public static final String CHALLENGEDETAIL = BASE_URL + "challengeDetail";
    public static final String CHALLENGEREPORTTEXTLISTING = BASE_URL + "challengeReportTextListing";
    public static final String REPORTCHALLENGE = BASE_URL + "reportChallenge";
    public static final String REPORTUSER = BASE_URL + "reportUser";

    public static final String NOTIFICATIONLISTING = BASE_URL + "notificationListing";
    public static final String READNOTIFICATION = BASE_URL + "readNotification";
    public static final String UNREADNOTIFICATIONCOUNT = BASE_URL + "unreadNotificationCount";
    public static final String DELETENOTIFICATION = BASE_URL + "deleteNotification";
    public static final String DELETECOMMENT = BASE_URL + "deleteChallengeComment";
    public static final String EDITCOMMENT = BASE_URL + "editChallengeComment";

    public static final String MESSAGECOUNT = BASE_URL + "getUnreadMessageCount";
    public static final String READMESSAGE = BASE_URL + "readMessage";
    public static final String TERMS = BASE_URL + "termsAndConditions";
    public static final String PRIVACY = BASE_URL + "privacyPolicy";
    public static final String ABOUT = BASE_URL + "aboutUs";

    public static final String GETSTRIPESECERT = BASE_URL + "getStripeSecret";
    public static final String WITHDRAWFROMWALLET = BASE_URL + "withdrawFromWallet";
    public static final String USERWALLET = BASE_URL + "userWallet";
    public static final String PAIDBRACKETCHALLENGEPAYMENTFROMWALLET = BASE_URL + "paidBracketChallengePaymentFromWallet";
}
