package com.uppro.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.uppro.ModelClasses.WalletModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncPut;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class WalletActivity extends AppCompatActivity {
    WalletActivity context;
    @BindView(R.id.withdraw)
    Button withdraw;
    @BindView(R.id.no_thanks)
    TextView noThanks;
    @BindView(R.id.amount)
    TextView amount;

    private SavePref savePref;
    private String auth_key;
    String amt = "";
    ArrayList<WalletModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);

        context = WalletActivity.this;
        savePref = new SavePref(this);

        if (ConnectivityReceiver.isConnected())
            USERWALLET();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


    }

    private void USERWALLET() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERWALLET, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonObject.getJSONObject("body");
                            amt = body.getString("walletAmount");
                            amount.setText("$" + amt);

                            for (int i = 0; i < body.getJSONArray("walletTransactions").length(); i++) {
                                JSONObject object = body.getJSONArray("walletTransactions").getJSONObject(i);
                                WalletModel walletModel = new WalletModel();
                                walletModel.setAmountchange(object.optString("amountChange"));
                                walletModel.setCurrent_amount(object.optString("currentAmount"));
                                walletModel.setMessage(object.optString("message"));
                                walletModel.setAmuntchangeType(object.optString("amountChangeType"));
                                walletModel.setId(object.optString("id"));
                                list.add(walletModel);
                            }

                           /* param: amountChangeType
                            comment: 0=>noChange, 1=>positive, 2=>negative*/


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick({R.id.back_button, R.id.withdraw, R.id.no_thanks})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.withdraw:
                if (amt.equals("0.00") || amt.equals("0") || amt.equals("")) {
                    util.IOSDialog(context, "Insufficient Amount");
                } else {
                    if (!savePref.getStringLatest(Parameters.STRIPEID).isEmpty()) {
                        new IOSDialog.Builder(context)
                                .setTitle(context.getResources().getString(R.string.app_name))
                                .setMessage("Are you sure? This action can't be undone")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        WITHDRAWFROMWALLET_API();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).show();

                    } else {
                        Stripe_Connect();
                    }
                }

                break;
            case R.id.no_thanks:
                finish();
                break;

        }
    }

    private void WITHDRAWFROMWALLET_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AMOUNT, amt);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.WITHDRAWFROMWALLET, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {


                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("Amount withdraw Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Stripe_Connect() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to connect you stripe account to withdraw!")
                .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, WebViewActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

}