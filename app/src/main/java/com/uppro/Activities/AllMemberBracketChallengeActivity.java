package com.uppro.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.uppro.Adapters.AllMemberAdapter;
import com.uppro.R;
import com.uppro.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllMemberBracketChallengeActivity extends AppCompatActivity {
    AllMemberBracketChallengeActivity context;
    private SavePref savePref;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_member_bracket_challenge);

        ButterKnife.bind(this);

        context = AllMemberBracketChallengeActivity.this;
        savePref = new SavePref(context);

        AllMemberAdapter adapter = new AllMemberAdapter(context);
        myRecyclerView.setLayoutManager(new GridLayoutManager(context, 5));
        myRecyclerView.setAdapter(adapter);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}