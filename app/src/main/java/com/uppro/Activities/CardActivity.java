package com.uppro.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.ModelClasses.WalletModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.FourDigitCardFormatWatcher;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncPost;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CardActivity extends AppCompatActivity {

    CardActivity context;
    @BindView(R.id.name_on_card)
    EditText name_on_card;
    @BindView(R.id.card_number)
    EditText card_number;
    @BindView(R.id.expiry_month_mm)
    TextView expiry_month_mm;
    @BindView(R.id.expiry_year_yyyy)
    TextView expiry_year_yyyy;
    @BindView(R.id.amount_to_pay)
    TextView amount_to_pay;
    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.cvv)
    EditText cvv;
    @BindView(R.id.checkbox_wallet)
    CheckBox checkbox_wallet;
    @BindView(R.id.pay)
    Button pay;

    private SavePref savePref;

    String amt = "", amount_to_pay_text = "", challengeId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        ButterKnife.bind(this);

        context = CardActivity.this;
        savePref = new SavePref(context);


        init();
        setToolbar();
    }

    private void init() {
        amount_to_pay_text = getIntent().getStringExtra("joiningFee");
        challengeId = getIntent().getStringExtra("challengeId");

        amount_to_pay.setText(getString(R.string.amount_to_pay) + " $" + amount_to_pay_text);
        card_number.addTextChangedListener(new FourDigitCardFormatWatcher());

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ConnectivityReceiver.isConnected()) {

                    if (!checkbox_wallet.isChecked()) {
                        PAY_BY_CARD();
                    } else {
                        PAIDBRACKETCHALLENGEPAYMENTFROMWALLET();
                    }


                } else
                    util.IOSDialog(context, util.internet_Connection_Error);


            }
        });


        expiry_month_mm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Show_Month();
            }
        });

        expiry_year_yyyy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Show_Year();
            }
        });

        checkbox_wallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    pay.setText("Pay by Wallet");
                else
                    pay.setText("Pay");
            }
        });


        if (ConnectivityReceiver.isConnected())
            USERWALLET();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void PAY_BY_CARD() {
        if (name_on_card.getText().toString().trim().isEmpty()) {
            util.IOSDialog(context, getString(R.string.enter_name_on_card));
            pay.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
        } else if (card_number.getText().toString().trim().isEmpty()) {
            util.IOSDialog(context, getString(R.string.enter_card_num));
            pay.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
        } else if (expiry_month_mm.getText().toString().trim().isEmpty()) {
            util.IOSDialog(context, getString(R.string.enter_expire_month));
            pay.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
        } else if (expiry_year_yyyy.getText().toString().trim().isEmpty()) {
            util.IOSDialog(context, getString(R.string.enter_expiry_year));
            pay.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
        } else if (cvv.getText().toString().trim().isEmpty()) {
            util.IOSDialog(context, getString(R.string.enter_cvv));
            pay.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
        } else {
            PAIDBRACKETCHALLENGEPAYMENT();
        }
    }


    private void USERWALLET() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERWALLET, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject body = jsonObject.getJSONObject("body");
                            amt = body.getString("walletAmount");
                            amount.setText("Use Wallet amount to pay.  Available Balance: $" + amt);

                            double amt_b = Double.parseDouble(amt);
                            double amount_to_pay_text_b = Double.parseDouble(amount_to_pay_text);
                            if (amt_b >= amount_to_pay_text_b) {
                                checkbox_wallet.setEnabled(true);
                                amount.setTextColor(context.getResources().getColor(R.color.black));
                            } else {
                                checkbox_wallet.setEnabled(false);
                                amount.setTextColor(context.getResources().getColor(R.color.gray));
                            }


                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void Show_Year() {
        // setup the alert builder
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle(R.string.choose);

        String current_year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        /*ArrayList to Array Conversion */
        String array[] = new String[50];
        for (int j = 0; j < 50; j++) {
            array[j] = String.valueOf(Integer.parseInt(current_year) + j);
        }


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                expiry_year_yyyy.setText(array[which]);
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void Show_Month() {
        // setup the alert builder
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle(R.string.choose);

        String current_year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        /*ArrayList to Array Conversion */
        String array[] = new String[12];
        array[0] = "01";
        array[1] = "02";
        array[2] = "03";
        array[3] = "04";
        array[4] = "05";
        array[5] = "06";
        array[6] = "07";
        array[7] = "08";
        array[8] = "09";
        array[9] = "10";
        array[10] = "11";
        array[11] = "12";


        builder.setItems(array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                expiry_month_mm.setText(array[which]);
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void PAIDBRACKETCHALLENGEPAYMENT() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challengeId);
        formBuilder.addFormDataPart(Parameters.CARDNUMBER, card_number.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CARDEXPMONTH, expiry_month_mm.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CARDEXPYEAR, expiry_year_yyyy.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.CARDCVC, cvv.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.PAIDBRACKETCHALLENGEPAYMENT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.is_for_pay=true;
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.pay_done)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.showToast(context, jsonMainobject.getString("message"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void PAIDBRACKETCHALLENGEPAYMENTFROMWALLET() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, challengeId);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.PAIDBRACKETCHALLENGEPAYMENTFROMWALLET, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.is_for_pay=true;
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.pay_done)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }


                    } catch (Exception e) {
                    }
                }

            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(R.string.payment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

}