package com.uppro.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.uppro.Adapters.FollowingAdapter;
import com.uppro.Adapters.UsersAdapter;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UserListActivity extends AppCompatActivity {
    UserListActivity context;
    private SavePref savePref;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.search_bar)
    EditText search_bar;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    UsersAdapter adapter = null;
    ArrayList<UserModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        ButterKnife.bind(this);

        context = UserListActivity.this;
        savePref = new SavePref(context);

        search_bar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (adapter != null)
                    adapter.filter(s.toString().trim());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        if (ConnectivityReceiver.isConnected())
            USERLISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


    }

    @OnClick({R.id.back_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
        }
    }

    private void USERLISTING() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.USERLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);
                                UserModel userModel = new UserModel();
                                userModel.setUser_id(object.getString("id"));
                                userModel.setName(object.getString("name"));
                                userModel.setEmail(object.optString("email"));
                                userModel.setProfile_pic(object.optString("image"));
                                if (!object.getString("id").equals(savePref.getID()))
                                    list.add(userModel);
                            }

                            if (list.size() > 0) {
                                adapter = new UsersAdapter(UserListActivity.this, list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                            }


                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void BackTask(String user_id, String name) {
        Intent intent = getIntent();
        intent.putExtra("user_id", user_id);
        intent.putExtra("name", name);
        setResult(RESULT_OK, intent);
        finish();
    }

}