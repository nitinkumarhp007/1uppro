package com.uppro.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerErrorListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerReadyListener;
import com.ct7ct7ct7.androidvimeoplayer.listeners.VimeoPlayerStateListener;
import com.ct7ct7ct7.androidvimeoplayer.model.TextTrack;
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView;
import com.uppro.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlayerVimeoActivity extends AppCompatActivity {



    @BindView(R.id.back_button)
    ImageView back_button;


    VimeoPlayerView vimeoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player_vimeo);
        ButterKnife.bind(this);
        vimeoPlayer=(VimeoPlayerView)findViewById(R.id.vimeoPlayer);
        getLifecycle().addObserver(vimeoPlayer);


        vimeoPlayer.initialize(true,57542671);
        vimeoPlayer.play();

        vimeoPlayer.addErrorListener(new VimeoPlayerErrorListener() {
            @Override
            public void onError(String message, String method, String name) {

                Toast.makeText(VideoPlayerVimeoActivity.this,message,Toast.LENGTH_SHORT).show();
            }
        });

        vimeoPlayer.addReadyListener(new VimeoPlayerReadyListener() {
            @Override
            public void onReady(String title, float duration, TextTrack[] textTrackArray) {

                Toast.makeText(VideoPlayerVimeoActivity.this,title,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInitFailed() {

            }
        });

        vimeoPlayer.addStateListener(new VimeoPlayerStateListener() {
            @Override
            public void onPlaying(float duration) {

            }

            @Override
            public void onPaused(float seconds) {

            }

            @Override
            public void onEnded(float duration) {

            }
        });

    }
}