package com.uppro.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.loader.content.CursorLoader;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.customPhotoPicker.interfaces.PermissionCallback;
import com.uppro.customPhotoPicker.utils.PermissionUtils;
import com.uppro.customPhotoPicker.utils.PickerOptions;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncPut;
import com.uppro.customPhotoPicker.Picker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateProfileActivity extends AppCompatActivity {
    UpdateProfileActivity context;
    private SavePref savePref;
    @BindView(R.id.profile_pic)
    RoundedImageView profilePic;
    @BindView(R.id.update)
    Button update;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.user_name)
    EditText user_name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.website)
    EditText website;
    @BindView(R.id.tiktok)
    EditText tiktok;
    @BindView(R.id.youtube)
    EditText youtube;
    @BindView(R.id.ig)
    EditText ig;
    @BindView(R.id.bio)
    EditText bio;
    @BindView(R.id.deactivate_account_switch)
    SwitchCompat deactivate_account_switch;

    private String selectedimage = "";
    Uri fileUri;
    private PickerOptions pickerOptions;
    private Button btnPickImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        context = UpdateProfileActivity.this;
        savePref = new SavePref(context);
        pickerOptions = new PickerOptions();
        pickerOptions.setMaxCount(1);
        pickerOptions.setExcludeVideos(true);
        pickerOptions.setMaxVideoDuration(10);

        deactivate_account_switch.setChecked(savePref.getStringLatest(Parameters.ISACTIVE).equals("1"));


        setdata();

    }

    private void setdata() {
        name.setText(savePref.getName());
        user_name.setText(savePref.getStringLatest(Parameters.USERNAME));
        email.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());
        website.setText(savePref.getStringLatest(Parameters.WEBSITE));
        tiktok.setText(savePref.getStringLatest(Parameters.TIKTOK));
        ig.setText(savePref.getStringLatest(Parameters.INSTAGRAM));
        youtube.setText(savePref.getStringLatest(Parameters.YOUTUBE));
        bio.setText(savePref.getStringLatest(Parameters.BIO));
        Glide.with(context).load(savePref.getImage()).error(R.drawable.placeholder).into(profilePic);
    }

    @OnClick({R.id.back_button, R.id.profile_pic, R.id.update})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.profile_pic:
//                CropImage.activity(fileUri)
//                        .setAspectRatio(2, 2)
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .start(this);
                PermissionUtils.INSTANCE.checkForCameraWritePermissions(this, new PermissionCallback() {
                    @Override
                    public void onPermission(boolean approved) {
                        if (pickerOptions != null)
                            Picker.startPicker(UpdateProfileActivity.this, pickerOptions);
                    }
                });
                break;
            case R.id.update:
                EDIT_PROFILE_Process();
                break;
        }
    }

    private void EDIT_PROFILE_Process() {
        if (ConnectivityReceiver.isConnected()) {
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Name");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (user_name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter User Name");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (email.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(email.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else
                EDIT_PROFILE_API();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void EDIT_PROFILE_API() {

        String isActive = "";

        if (deactivate_account_switch.isChecked())
            isActive = "1";
        else
            isActive = "0";

        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.USERNAME, user_name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.COUNTRY_CODE, "1");
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.BIO, bio.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.INSTAGRAM, ig.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.WEBSITE, website.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.TIKTOK, tiktok.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.YOUTUBE, youtube.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.ISACTIVE, isActive);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setName(body.optString("name"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setImage(body.getString("image"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setStringLatest(Parameters.COUNTRY_CODE, body.optString(Parameters.COUNTRY_CODE));
                            savePref.setStringLatest(Parameters.ISACTIVE, body.optString(Parameters.ISACTIVE));
                            savePref.setStringLatest(Parameters.USERNAME, body.optString(Parameters.USERNAME));

                            savePref.setStringLatest(Parameters.BIO, body.optString(Parameters.BIO));
                            savePref.setStringLatest(Parameters.TIKTOK, body.optString(Parameters.TIKTOK));
                            savePref.setStringLatest(Parameters.INSTAGRAM, body.optString(Parameters.INSTAGRAM));
                            savePref.setStringLatest(Parameters.YOUTUBE, body.optString(Parameters.YOUTUBE));
                            savePref.setStringLatest(Parameters.WEBSITE, body.optString(Parameters.WEBSITE));

                            finish();
                            util.showToast(context, "Profile Updated Successfully!");

                        } else {
                            update.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if (requestCode == Picker.REQUEST_CODE_PICKER) {
                {
                    ArrayList<String> pathList = data.getStringArrayListExtra(Picker.PICKED_MEDIA_LIST);
                    String realPath = getRealPath(this,Uri.parse(pathList.get(0)));
                    Glide.with(this).load(realPath).error(R.drawable.placeholder).into(profilePic);
                    selectedimage=realPath;

                }
            }
        }

    }




    public static String getRealPath(Context context, Uri fileUri) {
        String realPath;
        // SDK < API11
        if (Build.VERSION.SDK_INT < 11) {
            realPath = getRealPathFromURI_BelowAPI11(context, fileUri);
        }
        // SDK >= 11 && SDK < 19
        else if (Build.VERSION.SDK_INT < 19) {
            realPath = getRealPathFromURI_API11to18(context, fileUri);
        }
        // SDK > 19 (Android 4.4) and up
        else {
            realPath = getRealPathFromURI_API19(context, fileUri);
        }
        return realPath;
    }


    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();
        }
        return result;
    }

    public static String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = 0;
        String result = "";
        if (cursor != null) {
            column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();
            return result;
        }
        return result;
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

}
