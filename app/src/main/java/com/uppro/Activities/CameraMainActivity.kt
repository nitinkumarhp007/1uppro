package com.uppro.Activities

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.webkit.MimeTypeMap
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import com.lassi.common.utils.KeyUtils
import com.lassi.data.media.MiMedia
import com.lassi.domain.media.LassiOption
import com.lassi.domain.media.MediaType
import com.lassi.presentation.builder.Lassi
import com.lassi.presentation.common.decoration.GridSpacingItemDecoration
import com.uppro.Adapters.SelectedMediaAdapter
import com.uppro.R
import kotlinx.android.synthetic.main.activity_main_camera.*
import java.io.File
import java.util.*

class CameraMainActivity : AppCompatActivity() {

    private val selectedMediaAdapter by lazy { SelectedMediaAdapter(this::onItemClicked) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_camera)
        rvSelectedMedia.adapter = selectedMediaAdapter
        rvSelectedMedia.addItemDecoration(GridSpacingItemDecoration(2, 10))

        val intent = Lassi(this)
            .with(LassiOption.CAMERA_AND_GALLERY)
            .setMaxCount(1)
            .setGridSize(10)
            .setMinTime(5)
            .setMaxTime(180)
            .setMinFileSize(0)
            .setMaxFileSize(30000)
            .setMediaType(MediaType.VIDEO)
            .setStatusBarColor(R.color.colorPrimaryDark)
            .setToolbarColor(R.color.colorPrimary)
            .setToolbarResourceColor(android.R.color.white)
            .setProgressBarColor(R.color.colorAccent)
            .setPlaceHolder(R.drawable.ic_video_placeholder)
            .setErrorDrawable(R.drawable.ic_video_placeholder)
            .setSelectionDrawable(R.drawable.ic_checked_media)
            .setSupportedFileTypes("mp4", "mkv", "webm", "avi", "flv", "3gp")
            .build()
        receiveData.launch(intent)
    }


    private val receiveData =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val selectedMedia =
                    it.data?.getSerializableExtra(KeyUtils.SELECTED_MEDIA) as ArrayList<MiMedia>
                if (!selectedMedia.isNullOrEmpty()) {
                    ivEmpty.isVisible = selectedMedia.isEmpty()
                    val returnIntent = Intent()
                    returnIntent.putExtra("path", selectedMedia[0].path)
                    setResult(RESULT_OK, returnIntent)
                    finish()
//                    selectedMediaAdapter.setList(selectedMedia)
                }else
                {
                    finish()
                }
            }
        }

    private fun getMimeType(uri: Uri): String? {
        return if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            contentResolver.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
            MimeTypeMap.getSingleton()
                .getMimeTypeFromExtension(fileExtension.lowercase(Locale.getDefault()))
        }
    }

    private fun onItemClicked(miMedia: MiMedia) {
        miMedia.path?.let {
            val intent = Intent(Intent.ACTION_VIEW)
            if (miMedia.doesUri) {
                val uri = Uri.parse(it)
                intent.setDataAndType(uri, getMimeType(uri))
            } else {
                val file = File(it)
                val fileUri = FileProvider.getUriForFile(
                    this,
                    applicationContext.packageName + ".fileprovider", file
                )
                intent.setDataAndType(fileUri, getMimeType(fileUri))
            }

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivity(Intent.createChooser(intent, "Open file"))
        }
    }

    /**
     *   If Android device SDK is >= 30 and wants to access document (only for choose the non media file)
     *   then ask for "android.permission.MANAGE_EXTERNAL_STORAGE" permission
     */
//    private fun requestPermissionForDocument() {
//        when {
//            Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
//                if (Environment.isExternalStorageManager()) {
//
//                } else {
////                    try {
////                        val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
////                        intent.addCategory("android.intent.category.DEFAULT")
////                        intent.data = Uri.parse(
////                            String.format("package:%s", applicationContext?.packageName)
////                        )
////                        mPermissionSettingResult.launch(intent)
////                    } catch (e: Exception) {
////                        val intent = Intent()
////                        intent.action = Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
////                        mPermissionSettingResult.launch(intent)
////                    }
//                }
//            }
//            else -> {
//
//            }
//        }
//    }

    private val mPermissionSettingResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
//            requestPermissionForDocument()
        }

}
