package com.uppro.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Adapters.HomeAdapter;
import com.uppro.Fragments.CommentFragment;
import com.uppro.Fragments.CreateFragment;
import com.uppro.Fragments.FollowingFragment;
import com.uppro.Fragments.HomeFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.PastVideosFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ChallengeDetailActivity extends AppCompatActivity {
    ChallengeDetailActivity context;
    private SavePref savePref;

    @BindView(R.id.profile_1_up)
    TextView profile_1_up;
    @BindView(R.id.profile_2_up)
    TextView profile_2_up;
    @BindView(R.id.profile_1_score)
    TextView profile_1_score;
    @BindView(R.id.profile_2_score)
    TextView profile_2_score;
    @BindView(R.id.chat)
    ImageView chat;
    @BindView(R.id.profile_1)
    TextView profile_1;
    @BindView(R.id.profile_2)
    TextView profile_2;
    @BindView(R.id.profile_1_pic)
    ImageView profile_1_pic;
    @BindView(R.id.profile_1_thumb)
    ImageView profile_1_thumb;
    @BindView(R.id.profile_2_thumb)
    ImageView profile_2_thumb;
    @BindView(R.id.profile_2_pic)
    ImageView profile_2_pic;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.end_date)
    TextView end_date;
    @BindView(R.id.rules)
    TextView rules;
    @BindView(R.id.profile_1_layout)
    LinearLayout profile_1_layout;
    @BindView(R.id.video_play)
    ImageView video_play;
    @BindView(R.id.video_play_2)
    ImageView video_play_2;
    @BindView(R.id.join_challenge)
    TextView join_challenge;
    @BindView(R.id.reportOne)
    ImageView reportOne;
    @BindView(R.id.reportTwo)
    ImageView reportTwo;
    @BindView(R.id.share)
    ImageView share;

    String challenge_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_detail);
        ButterKnife.bind(this);

        context = ChallengeDetailActivity.this;
        savePref = new SavePref(context);

        challenge_id = getIntent().getStringExtra("challenge_id");

        if (ConnectivityReceiver.isConnected())
            CHALLENGEDETAIL();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void CHALLENGEDETAIL() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, challenge_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHALLENGEDETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONObject object = jsonmainObject.getJSONObject("body");
                            ChallengeListModel challengeListModel = new ChallengeListModel();
                            challengeListModel.setId(object.getString("id"));
                            challengeListModel.setCategory_name(object.getJSONObject("category").getString("name"));
                            challengeListModel.setCategoryId(object.optString("categoryId"));
                            challengeListModel.setCompetitors(object.optString("competitors"));
                            challengeListModel.setIsRules(object.optString("isRules"));
                            challengeListModel.setRules(object.optString("rules"));
                            challengeListModel.setTime(object.optString("time"));
                            challengeListModel.setType(object.optString("type"));
                            challengeListModel.setUserId(object.optString("userId"));
                            challengeListModel.setVideo(object.optString("video"));
                            challengeListModel.setStart_date(object.optString("startDate"));
                            challengeListModel.setEnd_date(object.optString("endDate"));
                            challengeListModel.setThumb(object.optString("videoThumbnail"));

                            challengeListModel.setVotestocreator(object.optString("votesToCreator"));
                            challengeListModel.setVotestochallenger(object.optString("votesToChallenger"));
                            challengeListModel.setIsvoted(object.optString("isVoted"));

                            challengeListModel.setName(object.getJSONObject("challengeCreator").optString("name"));
                            challengeListModel.setImage(object.getJSONObject("challengeCreator").optString("image"));

                            if (object.getJSONArray("challengeUsers").length() > 0) {
                                JSONObject obj = object.getJSONArray("challengeUsers").getJSONObject(0);

                                challengeListModel.setUserId__challenger(obj.getJSONObject("challenger").optString("id"));
                                challengeListModel.setName_challenger(obj.getJSONObject("challenger").optString("name"));
                                challengeListModel.setImage_challenger(obj.getJSONObject("challenger").optString("image"));
                                challengeListModel.setThumb_challenger(obj.optString("videoThumbnail"));
                                challengeListModel.setVideo_challenger(obj.optString("video"));
                            }

                        } else {
                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

   /* private void SetData(ChallengeListModel challengeListModel) {
        profile_1.setText(challengeListModel.getName());
        category.setText(challengeListModel.getCategory_name());
        profile_1_score.setText(challengeListModel.getVotestocreator());
        profile_2_score.setText(challengeListModel.getVotestochallenger());
//        reportOne.setVisibility(View.VISIBLE);

        Glide.with(context).load(challengeListModel.getImage()).into(profile_1_pic);
        Glide.with(context).load(challengeListModel.getThumb()).into(profile_1_thumb);

        if (!challengeListModel.getUserId__challenger().isEmpty()) {
            join_challenge.setVisibility(View.INVISIBLE);
            video_play_2.setVisibility(View.VISIBLE);
            profile_2_thumb.setVisibility(View.VISIBLE);
            profile_2.setText(challengeListModel.getName_challenger());
            Glide.with(context).load(challengeListModel.getImage_challenger()).into(profile_2_pic);
            Glide.with(context).load(challengeListModel.getThumb_challenger()).into(profile_2_thumb);
//            reportTwo.setVisibility(View.VISIBLE);
            if (challengeListModel.getEnd_date().isEmpty()) {
                end_date.setText("");
            } else {
                end_date.setText("Ends : " + util.getDateTime(challengeListModel.getEnd_date()));
            }
        } else {
            reportTwo.setVisibility(View.GONE);
            if (challengeListModel.getUserId().equals(savePref.getID())) {
                join_challenge.setVisibility(View.INVISIBLE);
            } else {
                join_challenge.setVisibility(View.VISIBLE);
            }
            video_play_2.setVisibility(View.INVISIBLE);
            profile_2_thumb.setVisibility(View.INVISIBLE);


            if (challengeListModel.getEnd_date().isEmpty()) {
                end_date.setText("");
            } else {
                end_date.setText("Ends : " + util.getDateTime(challengeListModel.getEnd_date()));
            }
        }


        if (challengeListModel.getIsRules().equals("1")) {
            rules.setText( "Rules: View Rules");
        } else {
            rules.setText("Rules: No Rules");
        }

        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getIsRules().equals("1")) {
                    new IOSDialog.Builder(context)
                            .setTitle("Rules")
                            .setCancelable(false)
                            .setMessage(challengeListModel.getRules()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                            *//* .setNegativeButton("Cancel", null)*//*.show();
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://3.20.218.227:9200/api/share/" + challengeListModel.getId());
                context.startActivity(Intent.createChooser(sharingIntent, "Share Product External"));

               *//* new IOSDialog.Builder(context)
                        .setTitle("Report")
                        .setCancelable(false)
                        .setMessage("Would you like to report challenge?").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Report", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        homeFragment.showInformationPop("1", position);
                        dialog.dismiss();
                    }
                })
                        *//**//* .setNegativeButton("Cancel", null)*//**//*.show();*//*
            }
        });


        profile_1_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getUserId().equals(savePref.getID())) {
                    Swich_Fragment(new ProfileFragment(), challengeListModel.getUserId());
                } else {
                    Swich_Fragment(new OtherProfileFragment(), challengeListModel.getUserId());
                }

            }
        });
        profile_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!challengeListModel.getUserId__challenger().isEmpty()) {
                    if (challengeListModel.getUserId__challenger().equals(savePref.getID())) {
                        Swich_Fragment(new ProfileFragment(), challengeListModel.getUserId__challenger());
                    } else {
                        Swich_Fragment(new OtherProfileFragment(), challengeListModel.getUserId__challenger());
                    }
                }
            }
        });
        join_challenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreateFragment fragment = new CreateFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("is_challenger", false);
                bundle.putBoolean("is_to_submit", true);
                bundle.putParcelable("data", challengeListModel);
                bundle.putBoolean("back_on", true);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        video_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getVideo() != null) {
                    if (!challengeListModel.getVideo().isEmpty()) {
                        context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                                challengeListModel.getVideo(),
                                " ", 0));
                    }
                }
            }
        });
        video_play_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (challengeListModel.getVideo_challenger() != null) {
                    if (!challengeListModel.getVideo_challenger().isEmpty()) {
                        context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                                challengeListModel.getVideo_challenger(),
                                " ", 0));
                    }
                }
            }
        });


        profile_1_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!challengeListModel.getUserId__challenger().isEmpty()) {
                    if (ConnectivityReceiver.isConnected()) {
                        VOTECHALLENGE("0");//0=>voteToCreator
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }

            }
        });


        profile_2_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!challengeListModel.getUserId__challenger().isEmpty()) {
                    if (ConnectivityReceiver.isConnected()) {
                        homeFragment.VOTECHALLENGE("1", position);//1=>voteToChallenger )
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }
            }


        });
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!challengeListModel.getUserId__challenger().isEmpty()) {
                    CommentFragment fragment = new CommentFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("challenge_id", challengeListModel.getId());
                    fragment.setArguments(bundle);

                    FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }


        });


        reportOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.showInformationPop("1", position);
            }

        });

        reportTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.showInformationPop("2", position);
            }

        });

        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", challengeListModel.getCategoryId());
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }*/
}