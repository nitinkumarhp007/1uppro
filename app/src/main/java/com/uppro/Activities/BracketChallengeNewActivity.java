package com.uppro.Activities;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.uppro.Adapters.TabVPagerAdapter;
import com.uppro.Fragments.BracketChallengeNewFragment;
import com.uppro.R;
import com.uppro.databinding.ActivityBracketChallengeNewBinding;

import java.util.ArrayList;

public class BracketChallengeNewActivity extends AppCompatActivity {

    ActivityBracketChallengeNewBinding bind;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_bracket_challenge_new);
        ArrayList<Fragment> frag=new ArrayList<>();
        frag.add(new BracketChallengeNewFragment());
        frag.add(new BracketChallengeNewFragment());
        bind.vpChallenge.setAdapter(new TabVPagerAdapter(getSupportFragmentManager(),BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,frag));
    }
}