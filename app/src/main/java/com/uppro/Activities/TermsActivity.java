package com.uppro.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TermsActivity extends AppCompatActivity {

    @BindView(R.id.mIvBack)
    ImageView mIvBack;
    @BindView(R.id.mTvTitleScreen)
    TextView mTvTitleScreen;
    @BindView(R.id.mTvText)
    TextView mTvText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mTvTitleScreen.setText(getIntent().getStringExtra("title"));

        if (getIntent().getStringExtra("title").equals("Terms of Use")){
            GETTERM();
        }else if (getIntent().getStringExtra("title").equals("Privacy Policy")){
            GETPOLICY();
        }else {
            GETABOUR();
        }



    }
    public void GETTERM() {
        ProgressDialog progressDialog = util.initializeProgress(TermsActivity.this);
        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(TermsActivity.this, AllAPIS.TERMS, formBody) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {
                    progressDialog.hide();
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq =  jsonmainObject.getJSONObject("body");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            mTvText.setText(Html.fromHtml(jsonReq.getString("content"), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            mTvText.setText(Html.fromHtml(jsonReq.getString("content")));
                        }


                    } catch (JSONException e) {
                        progressDialog.hide();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void GETABOUR() {
        ProgressDialog progressDialog = util.initializeProgress(TermsActivity.this);
        //progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(TermsActivity.this, AllAPIS.ABOUT, formBody) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {
                    progressDialog.hide();
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq =  jsonmainObject.getJSONObject("body");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            mTvText.setText(Html.fromHtml(jsonReq.getString("content"), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            mTvText.setText(Html.fromHtml(jsonReq.getString("content")));
                        }


                    } catch (JSONException e) {
                        progressDialog.hide();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void GETPOLICY() {
        ProgressDialog progressDialog = util.initializeProgress(TermsActivity.this);
        progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(TermsActivity.this, AllAPIS.PRIVACYPOLICY, formBody) {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {
                    progressDialog.hide();
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        JSONObject jsonReq =  jsonmainObject.getJSONObject("body");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            mTvText.setText(Html.fromHtml(jsonReq.getString("content"), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            mTvText.setText(Html.fromHtml(jsonReq.getString("content")));
                        }


                    } catch (JSONException e) {
                        progressDialog.hide();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}