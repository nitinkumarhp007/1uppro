package com.uppro.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Adapters.AdapterChatting;
import com.uppro.Adapters.ReportUserDetailAdapter;
import com.uppro.Adapters.ReportUserDetailChatAdapter;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.ChattingModel;
import com.uppro.ModelClasses.ReportTextModel;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncDELETE;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.uppro.R;
import com.uppro.parser.GetAsyncGet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChattngActivity extends AppCompatActivity {
    ChattngActivity context;
    SavePref savePref;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.type_message)
    EditText typeMessage;
    AdapterChatting adapter;
    @BindView(R.id.send_button)
    ImageView sendButton;
    @BindView(R.id.optionreport)
    ImageView optionreport;
    @BindView(R.id.option)
    ImageView option;
    @BindView(R.id.title)
    TextView title;
    Vibrator v;
    private ArrayList<ChattingModel> chat_list;

    boolean is_from_push = false;
    String  friend_id = "";
    private String selectedimage = "", thumb1 = "",user_id;
    private ProgressDialog progressDialog;

    Dialog dialog1 = null;
    private ArrayList<ReportTextModel> list_report_text;

    Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        ButterKnife.bind(this);

        context = ChattngActivity.this;
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        savePref = new SavePref(context);
        progressDialog = util.initializeProgress(context);

        friend_id = getIntent().getStringExtra("friend_id");
        user_id = getIntent().getStringExtra("friend_id");
        title.setText(getIntent().getStringExtra("friend_name"));
        is_from_push = getIntent().getBooleanExtra("is_from_push", false);


        Log.e("id___", savePref.getID() + " friend_id " + friend_id);

        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                new IntentFilter(util.NOTIFICATION_MESSAGE));


//        if (getIntent().hasExtra("messageID")){
//            if (ConnectivityReceiver.isConnected())
//                READMESSAGE(getIntent().getStringExtra("messageID"));
//            else
//                util.IOSDialog(context, util.internet_Connection_Error);
//        }
        if (ConnectivityReceiver.isConnected())
            GET_MESSAGE();
        else
            util.IOSDialog(context, util.internet_Connection_Error);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                selectedimage = getAbsolutePath(this, resultUri);

                SEND_MESSAGE("1");


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    private void GET_MESSAGE() {
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.OTHERUSERID, friend_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.CHATMESSAGELIST, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                chat_list = new ArrayList<>();
                if (chat_list.size() > 0)
                    chat_list.clear();

                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");

                            JSONArray jsonbody1 = body.getJSONArray("rows");
                            for (int i = 0; i < jsonbody1.length(); i++) {
                                JSONObject jsonbody = jsonbody1.getJSONObject(i);

                                ChattingModel chattingModel = new ChattingModel();
                                chattingModel.setChat_type(jsonbody.getString("type"));
                                chattingModel.setmessage_id(jsonbody.getString("id"));
                                chattingModel.setMessage(jsonbody.getString("message"));
                                chattingModel.setTimestamp(jsonbody.getString("created"));


                                if (savePref.getID().equalsIgnoreCase(jsonbody.getString("senderId")))
                                {
                                    chattingModel.setIs_own_message(true);
                                    chattingModel.setOpponent_name(savePref.getName());
                                }
                                else {
                                    chattingModel.setIs_own_message(false);
                                    chattingModel.setOpponent_name(jsonbody.getJSONObject("sender").getString("name"));
                                }

                                chat_list.add(chattingModel);
                            }
                            if (chat_list.size() > 0) {
                                Collections.reverse(chat_list);
                                adapter = new AdapterChatting(context, chat_list);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
                                myRecyclerView.setAdapter(adapter);
                            }
                        } else {

                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void SEND_MESSAGE(String type) {
        // progressDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);


        if (type.equals("2")) {
            if (!selectedimage.isEmpty()) {
                MediaType MEDIA_TYPE = null;
                MEDIA_TYPE = selectedimage.endsWith("mp4") ? MediaType.parse("/mp4") : MediaType.parse("/mp4");
                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.MESSAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }

            if (!thumb1.isEmpty()) {
                final MediaType MEDIA_TYPE = thumb1.endsWith("png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpeg");

                File file = new File(thumb1);
                formBuilder.addFormDataPart(Parameters.VIDEOTHUMBNAIL, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
        } else if (type.equals("1")) {
            progressDialog.show();
            if (!selectedimage.isEmpty()) {
                MediaType MEDIA_TYPE = null;
                MEDIA_TYPE = selectedimage.endsWith("png") ? MediaType.parse("image/png") : MediaType.parse("image/jpeg");
                File file = new File(selectedimage);
                formBuilder.addFormDataPart(Parameters.MESSAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
            }
        } else {
            formBuilder.addFormDataPart(Parameters.MESSAGE, typeMessage.getText().toString().trim());
        }
        formBuilder.addFormDataPart(Parameters.OTHERUSERID, friend_id);
        formBuilder.addFormDataPart(Parameters.TYPE, type);//0->text 1-> image
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SEND_MESSAGE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                typeMessage.setText("");
                selectedimage = "";
                thumb1 = "";
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject jsonbody = jsonmainObject.getJSONObject("body");

                            ChattingModel chattingModel = new ChattingModel();
                            chattingModel.setChat_type(jsonbody.getString("type"));
                            chattingModel.setmessage_id(jsonbody.getString("id"));
                            chattingModel.setMessage(jsonbody.getString("message"));
                            chattingModel.setTimestamp(jsonbody.getString("created"));
                            //chattingModel.setVideo_thumbnail(jsonbody.optString("thumb_image"));
                            chattingModel.setOpponent_name(savePref.getName());
                            chattingModel.setIs_own_message(true);

                            chat_list.add(chattingModel);
                            if (chat_list.size() > 1) {
                                adapter.notifyDataSetChanged();
                                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
                            } else {
                                adapter = new AdapterChatting(context, chat_list);
                                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(adapter);
                            }
                        } else {

                            util.IOSDialog(context, jsonmainObject.getString("message"));
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DeleteAlert(int position) {
        new IOSDialog.Builder(context)
                .setCancelable(false)
                .setMessage("Are you sure to Delete Message?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DELETE_MESSAGE_API(position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void DELETE_MESSAGE_API(int position) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHAT_ID, chat_list.get(position).getmessage_id());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.DELETE_MESSAGE, formBody, mDialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response response) {
                mDialog.dismiss();
                String result = response.body().toString();
                if (response.isSuccessful()) {
                    new IOSDialog.Builder(context)
                            .setCancelable(false)
                            .setMessage("Message Deleted Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            chat_list.remove(position);
                            adapter.notifyDataSetChanged();
                        }
                    }).show();
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    protected void onResume() {
        super.onResume();
        savePref.isChatScreen(true);
        Log.e("Check", "true");
        savePref.setCHAT_ID(friend_id);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        savePref.isChatScreen(false);
        Log.e("Check", "false");
        savePref.setCHAT_ID("");
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mMessageReceiver);
    }

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
                // Vibrate for 400 milliseconds
                v.vibrate(400);
                ChattingModel chattingModel = new ChattingModel();
                chattingModel.setmessage_id(intent.getStringExtra("message_id"));
                chattingModel.setMessage(intent.getStringExtra("message"));
                chattingModel.setTimestamp(intent.getStringExtra("created"));
                chattingModel.setOpponent_name(intent.getStringExtra("username"));
                chattingModel.setChat_type(intent.getStringExtra("type"));
                chattingModel.setIs_own_message(false);
                chat_list.add(chattingModel);
                adapter.notifyDataSetChanged();
                myRecyclerView.scrollToPosition(adapter.getItemCount() - 1);
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    @OnClick({R.id.attachment, R.id.back_button, R.id.send_button, R.id.optionreport, R.id.option})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.attachment:
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(context);
                break;
            case R.id.back_button:
                if (!is_from_push) {
                    finish();
                } else {
                    Intent intent = new Intent(ChattngActivity.this, MainActivity.class);
                    intent.putExtra("is_from_push", true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

                break;
            case R.id.send_button:
                if (ConnectivityReceiver.isConnected()) {
                    if (!typeMessage.getText().toString().isEmpty()) {
                        SEND_MESSAGE("0");
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
         case R.id.optionreport:
             new IOSDialog.Builder(this)
                     .setTitle("Report")
                     .setCancelable(false)
                     .setMessage("Would you like to report user?").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             dialog.dismiss();
                         }
                     }).setPositiveButton("Report", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             CHALLENGEREPORTTEXTLISTING(user_id);
                             dialog.dismiss();
                         }
                     })
                     .setNegativeButton("Cancel", null).show();
                break;
         case R.id.option:
             ShowDialog();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (!is_from_push) {
            super.onBackPressed();
        } else {
            Intent intent = new Intent(ChattngActivity.this, MainActivity.class);
            intent.putExtra("is_from_push", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getInsertedPath(Bitmap bitmap, String path) {
        String strMyImagePath = null;
        File mFolder = new File(context.getExternalCacheDir() + "/2udezz");

        if (!mFolder.exists()) {
            mFolder.mkdir();
        }

        String s = System.currentTimeMillis() / 1000 + ".jpg";
        File f = new File(mFolder.getAbsolutePath(), s);
        Log.e("creating", "name " + f.getName());
        strMyImagePath = f.getAbsolutePath();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;
    }




    public void CHALLENGEREPORTTEXTLISTING(String challengeId) {
        final ProgressDialog progressDialog = util.initializeProgress(this);
        progressDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.LONGITUDE, "jiosdjc");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(this, AllAPIS.CHALLENGEREPORTTEXTLISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                progressDialog.dismiss();
                list_report_text = new ArrayList<>();
                if (list_report_text.size() > 0)
                    list_report_text.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray jsonbodyArray = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < jsonbodyArray.length(); i++) {
                                JSONObject jsonbody = jsonbodyArray.getJSONObject(i);
                                ReportTextModel reportTextModel = new ReportTextModel();
                                reportTextModel.setId(jsonbody.getString("id"));
                                reportTextModel.setText(jsonbody.getString("text"));
                                list_report_text.add(reportTextModel);
                            }

                            showInformationPop(challengeId);
                        } else {
                            if (jsonmainObject.getString("message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }




    private void ShowDialog() {
        String text = "";

        text = "Block User";

        new IOSDialog.Builder(context)
                //.setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Would you like to block this user?")
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .setNegativeButton(text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (ConnectivityReceiver.isConnected())
                            BLOCK_USER();
                        else
                            util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }).show();
    }

    public void BLOCK_USER() {
        util.hideKeyboard(this);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BLOCKEDUSERID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.BLOCK_USER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("User Blocked Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                           finish();

                                        }
                                    }).show();

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void showInformationPop(String challengeId) {
        dialog1 = new Dialog(this);
        DisplayMetrics display = new DisplayMetrics();
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(true);
        dialog1.setContentView(R.layout.report_popup);
        Objects.requireNonNull(dialog1.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog1.getWindow().setGravity(Gravity.BOTTOM);
        this.getWindowManager().getDefaultDisplay().getMetrics(display);
        dialog1.getWindow().setBackgroundDrawableResource(R.drawable.shadow_border_bg_more_rounded);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        TextView tvDone = dialog1.findViewById(R.id.tvDone);
        RecyclerView my_recycler_view = dialog1.findViewById(R.id.my_recycler_view);

        my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
        my_recycler_view.setAdapter(new ReportUserDetailChatAdapter(context, list_report_text, this, challengeId));


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();
        Window window1 = dialog1.getWindow();
        window1.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void REPORTCHALLENGE_API(String challengeId, String id) {
        if (dialog1 != null)
            dialog1.dismiss();
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.REPORTEDUSERID, challengeId);
        formBuilder.addFormDataPart(Parameters.CHALLENGEREPORTTEXTID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.REPORTUSER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            //util.showToast(context, jsonMainobject.getString("message"));
                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("User Reported Successfully").setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            finish();
                                        }
                                    })
                                    .show();
                        } else {
                            if (jsonMainobject.getString("message").equals(util.Invalid_Authorization)) {

                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
