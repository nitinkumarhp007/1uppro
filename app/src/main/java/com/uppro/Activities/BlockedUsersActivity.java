package com.uppro.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uppro.Adapters.BlockedUserlistAdapter;
import com.uppro.ModelClasses.CommentModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;
import com.uppro.parser.GetAsyncDELETE;
import com.uppro.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class BlockedUsersActivity extends AppCompatActivity {
    BlockedUsersActivity context;
    private SavePref savePref;
    @BindView(R.id.my_recycler_view)
    RecyclerView my_recycler_view;
    @BindView(R.id.error_message)
    TextView errorMessage;
    BlockedUserlistAdapter adapter = null;
    ArrayList<CommentModel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked_users);
        ButterKnife.bind(this);
        context = BlockedUsersActivity.this;
        savePref = new SavePref(context);


        if (ConnectivityReceiver.isConnected())
            BLOCK_LISTINGAPI();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

        setToolbar();
    }

    private void BLOCK_LISTINGAPI() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CHALLENGEDID, "200");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.BLOCK_LISTING, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i).getJSONObject("blockedUser");
                                CommentModel commentModel = new CommentModel();
                                commentModel.setName(object.getString("name"));
                                commentModel.setImage(object.getString("image"));
                                commentModel.setUser_id(object.getString("id"));
                                list.add(commentModel);
                            }
                            if (list.size() > 0) {
                                adapter = new BlockedUserlistAdapter(context, list);
                                my_recycler_view.setLayoutManager(new LinearLayoutManager(context));
                                my_recycler_view.setAdapter(adapter);
                                errorMessage.setVisibility(View.GONE);
                                my_recycler_view.setVisibility(View.VISIBLE);
                            } else {
                                my_recycler_view.setVisibility(View.GONE);
                                errorMessage.setVisibility(View.VISIBLE);

                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent); 
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Blocked Users");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void Alert(String user_id, int position) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BLOCK_USER(user_id, position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }


    private void BLOCK_USER(String user_id, int position) {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.BLOCKEDUSERID, user_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.UNBLOCK_USER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    JSONObject jsonMainobject = null;
                    try {
                        jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("User Unblocked Successfully!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    list.remove(position);

                                    if (list.size() == 0) {
                                        my_recycler_view.setVisibility(View.GONE);
                                        errorMessage.setVisibility(View.VISIBLE);
                                    } else
                                        adapter.notifyDataSetChanged();
                                }
                            }).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


}