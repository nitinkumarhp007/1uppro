package com.uppro.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.uppro.MainActivity;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignInActivity extends AppCompatActivity {

    private SparseIntArray mErrorString;
    private static final int REQUEST_PERMISSIONS = 20;

    SignInActivity context;
    private SavePref savePref;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgot_password)
    Button forgotPassword;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.sign_up)
    Button signUp;
    
    String token="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        ButterKnife.bind(this);

        context = SignInActivity.this;
        savePref = new SavePref(context);
        mErrorString = new SparseIntArray();
        FirebaseApp.initializeApp(this);




        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                if (!task.isComplete()) {
////                    Log.w(applicationContext, "getInstanceId failed", task.exception)
//                    return
//                }
                // 3
                 token = task.getResult().getToken();
                SavePref.setDeviceToken(getApplicationContext(), "token", token);
            }
        });
    }

    @OnClick({R.id.forgot_password, R.id.sign_in, R.id.sign_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_password:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
//                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.sign_in:
                int currentapiVersion = Build.VERSION.SDK_INT;
                // if current version is M o sar greater than M
//                if (currentapiVersion >= Build.VERSION_CODES.M) {
//                    String[] array = {Manifest.permission.READ_EXTERNAL_STORAGE,
//                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            Manifest.permission.VIBRATE,
//                            Manifest.permission.ACCESS_FINE_LOCATION,
//                            Manifest.permission.ACCESS_COARSE_LOCATION,
//                            Manifest.permission.ACCESS_NETWORK_STATE,
//                            Manifest.permission.CAMERA};
//                    requestAppPermissions(array, R.string.permission, REQUEST_PERMISSIONS);
//                } else {
                    if (ConnectivityReceiver.isConnected()) {
                        if (email.getText().toString().trim().isEmpty()) {
                            util.showToast(context, "Please Enter Email Address");
                            signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                        } else if (!util.isValidEmail(email.getText().toString().trim())) {
                            util.showToast(context, "Please Enter a Vaild Email Address");
                            signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                        } else if (password.getText().toString().trim().isEmpty()) {
                            util.showToast(context, "Please Enter Password");
                            signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                        } else {
                            if (token.isEmpty()){
                                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                if (!task.isComplete()) {
////                    Log.w(applicationContext, "getInstanceId failed", task.exception)
//                    return
//                }
                                        // 3
                                        token = task.getResult().getToken();
                                        SavePref.setDeviceToken(getApplicationContext(), "token", token);

                                        LOGIN_API();
                                    }
                                });
                            }else {
                                LOGIN_API();
                            }

                        }
                    } else {
                        util.showToast(context, util.internet_Connection_Error);
                    }
//                }

                break;
            case R.id.sign_up:
                startActivity(new Intent(this, SignUpActivity.class));
//                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (email.getText().toString().trim().isEmpty()) {
                util.showToast(context, "Please Enter Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(email.getText().toString().trim())) {
                util.showToast(context, "Please Enter a Vaild Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.showToast(context, "Please Enter Password");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (token.isEmpty()){
                    FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                if (!task.isComplete()) {
////                    Log.w(applicationContext, "getInstanceId failed", task.exception)
//                    return
//                }
                            // 3
                            token = task.getResult().getToken();
                            SavePref.setDeviceToken(getApplicationContext(), "token", token);

                            LOGIN_API();
                        }
                    });
                }else {
                    LOGIN_API();
                }

            }
        } else {
            util.showToast(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        //Log.e("device_token__", SavePref.getDeviceToken(LoginActivity.this, "token"));
        //String s = SavePref.getDeviceToken(LoginActivity.this, "token");
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, email.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "0");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, token);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERLOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("token"));
                            savePref.setID(body.getString("id"));
                            savePref.setName(body.optString("name"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setImage(body.getString("image"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setStringLatest(Parameters.COUNTRY_CODE, body.optString(Parameters.COUNTRY_CODE));
                            savePref.setStringLatest(Parameters.ISACTIVE, body.optString(Parameters.ISACTIVE));
                            savePref.setStringLatest(Parameters.USERNAME, body.optString(Parameters.USERNAME));

                            savePref.setStringLatest(Parameters.BIO, body.optString(Parameters.BIO));
                            savePref.setStringLatest(Parameters.TIKTOK, body.optString(Parameters.TIKTOK));
                            savePref.setStringLatest(Parameters.INSTAGRAM, body.optString(Parameters.INSTAGRAM));
                            savePref.setStringLatest(Parameters.YOUTUBE, body.optString(Parameters.YOUTUBE));
                            savePref.setStringLatest(Parameters.WEBSITE, body.optString(Parameters.WEBSITE));
                            savePref.setStringLatest(Parameters.STRIPEID, body.optString(Parameters.STRIPEID));

                            Intent intent = new Intent(context, MainActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finishAffinity();
//                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Login Sucessfully!!!");


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }



    // check requested permissions are on or off
    public void requestAppPermissions(final String[] requestedPermissions, final int stringId, final int requestCode) {
        mErrorString.put(requestCode, stringId);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        boolean shouldShowRequestPermissionRationale = false;
        for (String permission : requestedPermissions) {
            permissionCheck = permissionCheck + ContextCompat.checkSelfPermission(this, permission);
            shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale || ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale) {
                Snackbar snack = Snackbar.make(findViewById(android.R.id.content), stringId, Snackbar.LENGTH_INDEFINITE);
                View view = snack.getView();
                TextView tv = view.findViewById(R.id.snackbar_text);
                tv.setTextColor(Color.WHITE);
                snack.setAction("GRANT", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityCompat.requestPermissions(SignInActivity.this, requestedPermissions, requestCode);
                    }
                }).show();
            } else {
                ActivityCompat.requestPermissions(this, requestedPermissions, requestCode);
            }
        } else {
            onPermissionsGranted(requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (int permission : grantResults) {
            permissionCheck = permissionCheck + permission;
        }
        if ((grantResults.length > 0) && permissionCheck == PackageManager.PERMISSION_GRANTED) {
            onPermissionsGranted(requestCode);
        } else {
            // onPermissionsGranted(requestCode);
        }
    }

    // if permissions granted succesfully
    private void onPermissionsGranted(int requestcode) {
        SigninProcess();
    }


}