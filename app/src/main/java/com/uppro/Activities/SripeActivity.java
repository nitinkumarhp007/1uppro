package com.uppro.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;
import com.uppro.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SripeActivity extends AppCompatActivity {
    public SripeActivity context;
    private String amount_to_pay_text = "";

    private static SavePref savePref;
    @BindView(R.id.cardInputWidget)
    CardInputWidget cardInputWidget;
    @BindView(R.id.payButton)
    Button payButton;
    @BindView(R.id.amount_to_pay)
    TextView amount_to_pay;
    //   private String paymentIntentClientSecret = "sk_test_51HCIJFCo7ME5zcC3KkkTt65f0ynJETEcXUjQhC7nPAN7w4Luruz6rIpwHgu999mLtNsykrhKbDoVTrGQXefVppG900npfSVJ3E";
    private String paymentIntentClientSecret = "";

    Stripe stripe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sripe);
        ButterKnife.bind(this);
        setToolbar();

        context = SripeActivity.this;
        savePref = new SavePref(context);

        amount_to_pay_text = getIntent().getStringExtra("amount");
        amount_to_pay.setText("Amount to pay $" + amount_to_pay_text);

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paymentIntentClientSecret != null)
                    startCheckout();
            }
        });

        if (ConnectivityReceiver.isConnected())
            STRIPE_SECERT();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void STRIPE_SECERT() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AMOUNT, amount_to_pay_text);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.GETSTRIPESECERT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            paymentIntentClientSecret = jsonObject.getJSONObject("body").getString("secret");
                            Log.e("IntentClientSecret", paymentIntentClientSecret);
                        } else {
                            util.IOSDialog(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void startCheckout() {
        PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
        if (params != null) {
            ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                    .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
            final Context context = getApplicationContext();
            stripe = new Stripe(
                    context,
                    PaymentConfiguration.getInstance(context).getPublishableKey()
            );
            stripe.confirmPayment(this, confirmParams);
        }
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Payment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(SripeActivity.this));
    }

    // ...

    private final class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull
        private final WeakReference<SripeActivity> activityRef;

        PaymentResultCallback(@NonNull SripeActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {
            final SripeActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                // Payment completed successfully
                Gson gson = new GsonBuilder().setPrettyPrinting().create();


                //util.IOSDialog(context, "Payment completed" +gson.toJson(paymentIntent));

                Toast.makeText(activity, "Payment completed", Toast.LENGTH_SHORT).show();

                Log.e("TRANSACTIONNO", gson.toString());

                //BOOKPRODUCT(gson.toJson(paymentIntent), "2");

            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed


                util.IOSDialog(context, "Payment failed" +
                        Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage());

                Log.e("errorrrrrr", Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage());
            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            final SripeActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            // Payment request failed – allow retrying using the same payment method
            util.IOSDialog(context, "Error" + e.toString());
        }
    }

}