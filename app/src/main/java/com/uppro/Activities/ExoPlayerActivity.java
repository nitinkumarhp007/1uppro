package com.uppro.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.uppro.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExoPlayerActivity extends AppCompatActivity {


    @BindView(R.id.videoView)
    SimpleExoPlayerView videoView;
    @BindView(R.id.mProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.back_button)
    ImageView back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player);
        ButterKnife.bind(this);

        SimpleExoPlayer absPlayerInternal = ExoPlayerFactory.newSimpleInstance(this);

        String userAgent = Util.getUserAgent(this, "exoPlayerSample");

        DefaultDataSourceFactory defdataSourceFactory = new DefaultDataSourceFactory(this, userAgent);
        Uri uriOfContentUrl = Uri.parse(getIntent().getStringExtra("data"));
//        Uri uriOfContentUrl = Uri.parse("https://vod-progressive.akamaized.net/exp=1626534637~acl=%2Fvimeo-prod-skyfire-std-us%2F01%2F85%2F23%2F575426715%2F2718401997.mp4~hmac=06542a8084165b666e42d2e9934077f60ad339009e9c835bff5f84cfa12a39ac/vimeo-prod-skyfire-std-us/01/85/23/575426715/2718401997.mp4");
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(defdataSourceFactory).createMediaSource(uriOfContentUrl);

        absPlayerInternal.prepare(mediaSource);
        videoView.setPlayer(absPlayerInternal);
        videoView.hideController();
        videoView.getPlayer().seekTo(0);
        videoView.getPlayer().setPlayWhenReady(true);

        if (videoView.getPlayer() != null) {
            videoView.getPlayer().addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == PlaybackStateCompat.STATE_PLAYING) {
                        //do something
                        mProgressBar.setVisibility(View.GONE);

                    } else if (playbackState == PlaybackStateCompat.STATE_STOPPED) {
                        //do something
                        mProgressBar.setVisibility(View.GONE);
                    } else if (playbackState == PlaybackStateCompat.STATE_BUFFERING) {
                        //do something
                        mProgressBar.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {

                }
            });
        }
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoView.getPlayer() != null) {
            videoView.getPlayer().stop();
            videoView.getPlayer().release();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}