package com.uppro.customPhotoPicker.interfaces

import com.uppro.customPhotoPicker.gallery.MediaModel

interface MediaClickInterface {
    fun onMediaClick(media: MediaModel)
    fun onMediaLongClick(media: MediaModel, intentFrom: String)
}