package com.uppro.customPhotoPicker.interfaces

interface PermissionCallback {
    fun onPermission(approved: Boolean)
}