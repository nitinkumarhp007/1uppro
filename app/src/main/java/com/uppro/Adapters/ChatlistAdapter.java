package com.uppro.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Activities.ChattngActivity;
import com.uppro.Activities.SplashActivity;
import com.uppro.Fragments.ChatFragment;
import com.uppro.ModelClasses.ChattingModel;
import com.uppro.R;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class ChatlistAdapter extends RecyclerView.Adapter<ChatlistAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ChattingModel> list;
    ArrayList<ChattingModel> tempList;
    ChatFragment chatFragment;

    public ChatlistAdapter(Context context, ArrayList<ChattingModel> list_chat, ChatFragment chatFragment) {
        this.context = context;
        this.list = list_chat;
        this.tempList = list_chat;
        this.chatFragment = chatFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.chat_list_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.date.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getTimestamp())));

        holder.name.setText(list.get(position).getOpponent_name());

        if (list.get(position).getType().equals("0"))
            holder.message.setText(list.get(position).getMessage());
        else if (list.get(position).getType().equals("1"))
            holder.message.setText("Media File");


//        if (list.get(position).getMessage().contains("http://3.20.218.227:9200/api/share/")){
//            holder.message.setTextColor(R.color.blue);
//
//            holder.message.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context, SplashActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    context.startActivity(intent);
//                }
//            });
//
//        }else {
//
//            holder.message.setTextColor(R.color.gray);
//
//        }

        Glide.with(context).load(list.get(position).getImage()).into(holder.profilePic);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatFragment.READMESSAGE(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        RoundedImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.message)
        TextView message;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }
}
