package com.uppro.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Fragments.BracketMainFragment;
import com.uppro.Fragments.CommentFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.PastChallengeFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.ModelClasses.BracketChallengeListModel;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BracketChallengeVideosAdapter extends RecyclerView.Adapter<BracketChallengeVideosAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private SavePref savePref;
    ArrayList<BracketChallengeListModel> list;
    private View view;
    BracketMainFragment pastChallengeFragment;

    public BracketChallengeVideosAdapter(Context context, ArrayList<BracketChallengeListModel> list, BracketMainFragment pastChallengeFragment) {
        this.context = context;
        this.list = list;
        savePref=new SavePref(context);
        this.pastChallengeFragment = pastChallengeFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.bracket_challenge_videos_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.profile_1.setText(list.get(position).getName_challenger());
        holder.comment_count.setText(list.get(position).getVotes());

        Glide.with(context).load(list.get(position).getImage_challenger()).into(holder.profile_1_pic);
        Glide.with(context).load(list.get(position).getThumb_challenger()).into(holder.image);

        holder.profile_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUserId().equals(savePref.getID())) {
                    Swich_Fragment(new ProfileFragment(), savePref.getID());
                } else {
                    Swich_Fragment(new OtherProfileFragment(), list.get(position).getUserId__challenger());
                }

            }
        });
        holder.profile_1_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUserId().equals(savePref.getID())) {
                    Swich_Fragment(new ProfileFragment(), savePref.getID());
                } else {
                    Swich_Fragment(new OtherProfileFragment(), list.get(position).getUserId__challenger());
                }

            }
        });


        holder.video_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getVideo() != null) {
                    if (!list.get(position).getVideo_challenger().isEmpty()) {

                        pastChallengeFragment.GETPATH(list.get(position).getVideo_challenger());
//                        context.startActivity(new Intent(context, ExoPlayerActivity.class).putExtra("data",list.get(position).getVideo()));

//                        context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
//                                list.get(position).getVideo(),
//                                " ", 0));
                    }
                }
            }
        });




//        holder.profile_1_up.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (ConnectivityReceiver.isConnected()) {
////                    pastChallengeFragment.LIKEVIDEO("1", position);//1=>like )
//                } else {
//                    util.IOSDialog(context, util.internet_Connection_Error);
//                }
//            }
//        });
        holder.profile_1_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
//                    pastChallengeFragment.LIKEVIDEO("0", position);//1=>like )
                    pastChallengeFragment.LIKEVIDEO("1", position);//1=>like )
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        });
//        holder.comment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CommentFragment fragment = new CommentFragment();
//
//                Bundle bundle = new Bundle();
//                bundle.putString("challenge_id", list.get(position).getId());
//                fragment.setArguments(bundle);
//
//                FragmentManager fragmentManager = pastChallengeFragment.getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.frame_container, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name)
        TextView category_name;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.video_play)
        ImageView video_play;
        @BindView(R.id.comment)
        ImageView comment;
        @BindView(R.id.profile_1_pic)
        ImageView profile_1_pic;
        @BindView(R.id.profile_1)
        TextView profile_1;
        @BindView(R.id.profile_1_down)
        TextView profile_1_down;
        @BindView(R.id.comment_count)
        TextView comment_count;
        @BindView(R.id.share)
        ImageView share;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/

    private void Swich_Fragment(Fragment fragment, String user_id) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = pastChallengeFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
