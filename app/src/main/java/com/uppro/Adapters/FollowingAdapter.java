package com.uppro.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Fragments.FollowingFragment;
import com.uppro.Fragments.PastVideosFragment;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    FollowingFragment followingFragment;
    ArrayList<CategoryModel> list;

    public FollowingAdapter(Context context, ArrayList<CategoryModel> list, FollowingFragment followingFragment) {
        this.context = context;
        this.list = list;
        this.followingFragment = followingFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.following_list_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.logo).into(holder.profilePic);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", list.get(position).getId());
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = followingFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", list.get(position).getId());
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = followingFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        if (list.get(position).getIsFollowed().equals("1"))
            holder.follow.setText("Following");
        else
            holder.follow.setText("Follow");


        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected())
                    followingFragment.FOLLOWUNFOLLOWCATEGORY_API(list.get(position).getId(), position);
                else
                    util.IOSDialog(context, util.internet_Connection_Error);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        RoundedImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.follow)
        TextView follow;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/
}
