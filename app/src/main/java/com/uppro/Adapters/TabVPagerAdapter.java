package com.uppro.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class TabVPagerAdapter extends FragmentPagerAdapter {
        FragmentManager fm;
        ArrayList<Fragment> frags;

    public TabVPagerAdapter(@NonNull FragmentManager fm, int behavior,ArrayList<Fragment> frags) {
        super(fm, behavior);
        this.fm = fm;
        this.frags = frags;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return frags.get(position);
    }


    @Override
    public int getCount() {
        return frags.size();
    }
}