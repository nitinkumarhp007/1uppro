package com.uppro.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Activities.VideoPlayerVimeoActivity;
import com.uppro.Fragments.CommentFragment;
import com.uppro.Fragments.CreateFragment;
import com.uppro.Fragments.HomeNewFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.PastVideosFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    HomeNewFragment homeFragment;
    private View view;
    SavePref savePref;

    ArrayList<ChallengeListModel> list;

    public HomeAdapter(Context context, ArrayList<ChallengeListModel> list, HomeNewFragment homeFragment) {
        this.context = context;
        this.homeFragment = homeFragment;
        this.list = list;
        Inflater = LayoutInflater.from(context);
        savePref = new SavePref(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.home_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.profile_1.setText(list.get(position).getName());
        holder.category.setText(list.get(position).getCategory_name());
        holder.profile_1_score.setText(list.get(position).getVotestocreator());
        holder.profile_2_score.setText(list.get(position).getVotestochallenger());

        Glide.with(context).load(list.get(position).getImage()).into(holder.profile_1_pic);
        Glide.with(context).load(list.get(position).getThumb()).into(holder.profile_1_thumb);

        if (!list.get(position).getUserId__challenger().isEmpty()) {
            holder.join_challenge.setVisibility(View.INVISIBLE);
            holder.video_play_2.setVisibility(View.VISIBLE);
            holder.profile_2_thumb.setVisibility(View.VISIBLE);
            holder.profile_2.setText(list.get(position).getName_challenger());
            Glide.with(context).load(list.get(position).getImage_challenger()).into(holder.profile_2_pic);
            Glide.with(context).load(list.get(position).getThumb_challenger()).into(holder.profile_2_thumb);
            if (list.get(position).getEnd_date().isEmpty()) {
                holder.end_date.setText("");
            } else {
                holder.end_date.setText("Ends : " + util.getDateTime(list.get(position).getEnd_date()));
            }
        } else {
            if (list.get(position).getUserId().equals(savePref.getID())) {
                holder.join_challenge.setVisibility(View.INVISIBLE);
            } else {
                holder.join_challenge.setVisibility(View.VISIBLE);
            }
            holder.video_play_2.setVisibility(View.INVISIBLE);
            holder.profile_2_thumb.setVisibility(View.INVISIBLE);


            if (list.get(position).getEnd_date().isEmpty()) {
                holder.end_date.setText("");
            } else {
                holder.end_date.setText("Ends : " + util.getDateTime(list.get(position).getEnd_date()));
            }
        }


        if (list.get(position).getIsRules().equals("1")) {
            holder.rules.setText("Rules: View Rules");
        } else {
            holder.rules.setText("Rules: No Rules");
        }

        holder.rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getIsRules().equals("1")) {
                    new IOSDialog.Builder(context)
                            .setTitle("Rules")
                            .setCancelable(false)
                            .setMessage(list.get(position).getRules()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                            /* .setNegativeButton("Cancel", null)*/.show();
                }
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, holder.share);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.share_item, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("Share")) {
                            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type1/"+ list.get(position).getId());
                            context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
                        } else {
                            new IOSDialog.Builder(context)
                                    .setTitle("Report")
                                    .setCancelable(false)
                                    .setMessage("Would you like to report challenge?").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).setPositiveButton("Report", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    homeFragment.CHALLENGEREPORTTEXTLISTING(list.get(position).getId());
                                    dialog.dismiss();
                                }
                            })
                                    .setNegativeButton("Cancel", null).show();
                        }

                        return true;
                    }
                });
                popup.show();


            }
        });


        holder.profile_1_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUserId().equals(savePref.getID())) {
                    Swich_Fragment(new ProfileFragment(), list.get(position).getUserId());
                } else {
                    Swich_Fragment(new OtherProfileFragment(), list.get(position).getUserId());
                }

            }
        });
        holder.layout2profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!list.get(position).getUserId__challenger().isEmpty()) {
                    if (list.get(position).getUserId__challenger().equals(savePref.getID())) {
                        Swich_Fragment(new ProfileFragment(), list.get(position).getUserId__challenger());
                    } else {
                        Swich_Fragment(new OtherProfileFragment(), list.get(position).getUserId__challenger());
                    }
                }
            }
        });
        holder.join_challenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreateFragment fragment = new CreateFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("is_challenger", false);
                bundle.putBoolean("is_to_submit", true);
                bundle.putParcelable("data", list.get(position));
                bundle.putBoolean("back_on", true);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        holder.video_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getVideo() != null) {
                    if (!list.get(position).getVideo().isEmpty()) {

                        if (ConnectivityReceiver.isConnected()) {
                            homeFragment.GETPATH(list.get(position).getVideo());//0=>voteToCreator
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }

                    }
                }
            }
        });

/*
        absPlayerInternal.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == PlaybackStateCompat.STATE_PLAYING) {
                    //do something
                }
                else if (playbackState == PlaybackStateCompat.STATE_STOPPED) {
                    //do something
                    list.get(position).setIsPlaying(false);
                    holder.video_play.setImageResource(R.drawable.video_play);
                    holder.videoView.getPlayer().stop();
                }
            }



            @Override
            public void onPlayerError(ExoPlaybackException error) {
                holder.videoView.getPlayer().stop();

            }
        });
*/
        holder.video_play_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getVideo_challenger() != null) {
                    if (!list.get(position).getVideo_challenger().isEmpty()) {


                        if (ConnectivityReceiver.isConnected()) {
                            homeFragment.GETPATH(list.get(position).getVideo_challenger());//0=>voteToCreator
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }
                    }
                }
            }
        });


        holder.profile_1_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!list.get(position).getUserId__challenger().isEmpty()) {
                    if (ConnectivityReceiver.isConnected()) {
                        homeFragment.VOTECHALLENGE("0", position);//0=>voteToCreator
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }

            }
        });


        holder.profile_2_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!list.get(position).getUserId__challenger().isEmpty()) {
                    if (ConnectivityReceiver.isConnected()) {
                        homeFragment.VOTECHALLENGE("1", position);//1=>voteToChallenger )
                    } else {
                        util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }
            }


        });
        holder.chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!list.get(position).getUserId__challenger().isEmpty()) {
                    CommentFragment fragment = new CommentFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("challenge_id", list.get(position).getId());
                    fragment.setArguments(bundle);

                    FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }


        });


       /* holder.profile_2_thumb.setOnTouchListener(new OnSwipeTouchListener(context) {
            public void onSwipeTop() {
                Toast.makeText(context, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {
                 Toast.makeText(context, "right", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeLeft() {
                  Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", "");
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

            public void onSwipeBottom() {
                Toast.makeText(context, "bottom", Toast.LENGTH_SHORT).show();
            }

        });



        if (list.get(position).getUserId__challenger().isEmpty()) {
            holder.profile_two_background.setOnTouchListener(new OnSwipeTouchListener(context) {
                public void onSwipeTop() {
                    Toast.makeText(context, "top", Toast.LENGTH_SHORT).show();
                }

                public void onSwipeRight() {
                     Toast.makeText(context, "right", Toast.LENGTH_SHORT).show();
                }

                public void onSwipeLeft() {
                      Toast.makeText(context, "left", Toast.LENGTH_SHORT).show();
                    PastVideosFragment fragment = new PastVideosFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("category_id", "");
                    fragment.setArguments(bundle);

                    FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

                public void onSwipeBottom() {
                    Toast.makeText(context, "bottom", Toast.LENGTH_SHORT).show();
                }

            });
        }*/


        holder.category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PastVideosFragment fragment = new PastVideosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("category_id", list.get(position).getCategoryId());
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profile_1_up)
        TextView profile_1_up;
        @BindView(R.id.profile_2_up)
        TextView profile_2_up;
        @BindView(R.id.profile_1_score)
        TextView profile_1_score;
        @BindView(R.id.profile_2_score)
        TextView profile_2_score;
        @BindView(R.id.chat)
        ImageView chat;
        @BindView(R.id.profile_1)
        TextView profile_1;
        @BindView(R.id.profile_2)
        TextView profile_2;
        @BindView(R.id.profile_1_pic)
        ImageView profile_1_pic;
        @BindView(R.id.profile_1_thumb)
        ImageView profile_1_thumb;
        @BindView(R.id.profile_2_thumb)
        ImageView profile_2_thumb;
        @BindView(R.id.profile_2_pic)
        ImageView profile_2_pic;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.end_date)
        TextView end_date;
        @BindView(R.id.rules)
        TextView rules;
        @BindView(R.id.profile_1_layout)
        LinearLayout profile_1_layout;
        @BindView(R.id.video_play)
        ImageView video_play;
        @BindView(R.id.video_play_2)
        ImageView video_play_2;
        @BindView(R.id.join_challenge)
        TextView join_challenge;
        @BindView(R.id.reportOne)
        ImageView reportOne;
        @BindView(R.id.reportTwo)
        ImageView reportTwo;
        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.profile_two_background)
        ImageView profile_two_background;
        @BindView(R.id.challenger_button)
        RelativeLayout challenger_button;

        @BindView(R.id.parent)
        LinearLayout parent;
        @BindView(R.id.layout2profile)
        LinearLayout layout2profile;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/

    private void Swich_Fragment(Fragment fragment, String user_id) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = homeFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
