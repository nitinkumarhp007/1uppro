package com.uppro.Adapters;

import android.content.Intent;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Activities.ChattngActivity;
import com.uppro.Activities.ImageShowActivity;
import com.uppro.ModelClasses.ChattingModel;
import com.uppro.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterChatting extends RecyclerView.Adapter<AdapterChatting.RecyclerViewHolder> {
    ChattngActivity context;
    LayoutInflater inflater;
    ArrayList<ChattingModel> chatting_list;


    public AdapterChatting(ChattngActivity context, ArrayList<ChattingModel> chatting_list) {
        this.context = context;
        this.chatting_list = chatting_list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.chat_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        Linkify.addLinks(holder.leftText, Linkify.ALL);
        Linkify.addLinks(holder.rightText, Linkify.ALL);

        String timestamp = chatting_list.get(position).getTimestamp(); //timestamp : 1254155422
        long timestampString = Long.parseLong(timestamp);
        String value = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                format(new Date(timestampString * 1000)); //convertion to 16/05/2017 17:33:42

        // holder.time.setText(value);
        Log.e("value", value);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = null;
        try {
            date = formatter.parse(value);//value:  16/05/2017 17:33:42
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date_text = date.toString().substring(0, 10);


        String message = chatting_list.get(position).getMessage();
        if (chatting_list.get(position).isIs_own_message()) {
            holder.relativeLeftMessage.setVisibility(View.GONE);
            holder.leftArrow.setVisibility(View.GONE);
            holder.relativeRightMessage.setVisibility(View.VISIBLE);
            holder.rightArrow.setVisibility(View.VISIBLE);

            holder.rightMessageTime.setText(date_text);
            holder.rightName.setText(chatting_list.get(position).getOpponent_name());

            holder.relativeLeft.setVisibility(View.GONE);

            if (chatting_list.get(position).getChat_type().equals("1")) {
                holder.imageLayoutRight.setVisibility(View.VISIBLE);
                holder.rightText.setVisibility(View.INVISIBLE);
                Glide.with(context).load(chatting_list.get(position).getMessage()).into(holder.imageRight);

                holder.videoPlay_right.setVisibility(View.GONE);
            } else if (chatting_list.get(position).getChat_type().equals("2")) {
                holder.imageLayoutRight.setVisibility(View.VISIBLE);
                holder.rightText.setVisibility(View.INVISIBLE);
                Glide.with(context).load(chatting_list.get(position).getVideo_thumbnail()).into(holder.imageRight);

                holder.videoPlay_right.setVisibility(View.VISIBLE);

            } else {
                holder.rightText.setVisibility(View.VISIBLE);
                holder.imageLayoutRight.setVisibility(View.GONE);
                holder.rightText.setText(message);
            }

        } else {

            holder.relativeLeftMessage.setVisibility(View.VISIBLE);
            holder.leftArrow.setVisibility(View.VISIBLE);
            holder.relativeRightMessage.setVisibility(View.GONE);
            holder.rightArrow.setVisibility(View.GONE);

            holder.leftMessageTime.setText(date_text);
            holder.leftName.setText(chatting_list.get(position).getOpponent_name());
            holder.relativeLeft.setVisibility(View.VISIBLE);

            if (chatting_list.get(position).getChat_type().equals("1")) {
                holder.imageLayoutLeft.setVisibility(View.VISIBLE);
                holder.leftText.setVisibility(View.INVISIBLE);
                Glide.with(context).load(chatting_list.get(position).getMessage()).into(holder.imageLeft);

                holder.videoPlayLeft.setVisibility(View.GONE);
            } else if (chatting_list.get(position).getChat_type().equals("2")) {
                holder.imageLayoutLeft.setVisibility(View.VISIBLE);
                holder.leftText.setVisibility(View.INVISIBLE);
                Glide.with(context).load(chatting_list.get(position).getVideo_thumbnail()).into(holder.imageLeft);

                holder.videoPlayLeft.setVisibility(View.VISIBLE);

            } else {
                holder.leftText.setVisibility(View.VISIBLE);
                holder.imageLayoutLeft.setVisibility(View.GONE);
                holder.leftText.setText(message);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chatting_list.get(position).getChat_type().equals("1")) {
                    Intent intent = new Intent(context, ImageShowActivity.class);
                    intent.putExtra("image", chatting_list.get(position).getMessage());
                    context.startActivity(intent);
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                context.DeleteAlert(position);
                return false;
            }
        });

       /* holder.videoPlay_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!chatting_list.get(position).getMessage().isEmpty()) {
                    context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                            chatting_list.get(position).getMessage(),
                            " ", 0));
                }
            }
        });
        holder.videoPlayLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!chatting_list.get(position).getMessage().isEmpty()) {
                    context.startActivity(PlayerActivity.getVideoPlayerIntent(context,
                            chatting_list.get(position).getMessage(),
                            " ", 0));
                }
            }
        });*/

    }


    @Override
    public int getItemCount() {
        return chatting_list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.left_arrow)
        ImageView leftArrow;
        @BindView(R.id.left_text)
        TextView leftText;
        @BindView(R.id.left_message_time)
        TextView leftMessageTime;
        @BindView(R.id.left_name)
        TextView leftName;
        @BindView(R.id.relative_left_Message)
        RelativeLayout relativeLeftMessage;
        @BindView(R.id.relative_left)
        RelativeLayout relativeLeft;
        @BindView(R.id.right_arrow)
        ImageView rightArrow;
        @BindView(R.id.right_text)
        TextView rightText;
        @BindView(R.id.right_message_time)
        TextView rightMessageTime;
        @BindView(R.id.right_name)
        TextView rightName;
        @BindView(R.id.relative_right_Message)
        RelativeLayout relativeRightMessage;
        @BindView(R.id.relative_right)
        RelativeLayout relativeRight;

        @BindView(R.id.image_left)
        RoundedImageView imageLeft;
        @BindView(R.id.video_play_left)
        ImageView videoPlayLeft;
        @BindView(R.id.image_layout_left)
        RelativeLayout imageLayoutLeft;
        @BindView(R.id.image_right)
        ImageView imageRight;
        @BindView(R.id.video_play)
        ImageView videoPlay_right;
        @BindView(R.id.image_layout_right)
        RelativeLayout imageLayoutRight;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

