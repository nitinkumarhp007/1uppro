package com.uppro.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Activities.UserListActivity;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.RecyclerViewHolder> {
    UserListActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<UserModel> list;
    ArrayList<UserModel> tempList;

    public UsersAdapter(UserListActivity context, ArrayList<UserModel> list) {
        this.context = context;
        this.tempList = list;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.user_list_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getProfile_pic()).into(holder.profilePic);

        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.BackTask(list.get(position).getUser_id(), list.get(position).getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        RoundedImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.follow)
        TextView follow;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<UserModel> nList = new ArrayList<UserModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (UserModel wp : tempList) {
                if (wp.getName().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }
}
