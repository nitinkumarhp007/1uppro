package com.uppro.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.uppro.Activities.AllMemberBracketChallengeActivity;
import com.uppro.Activities.CardActivity;
import com.uppro.Fragments.BracketChallengeFragment;
import com.uppro.Fragments.ChallengeDetailFragment;
import com.uppro.Fragments.CreateFragment;
import com.uppro.Fragments.SubmitBracketFragment;
import com.uppro.Fragments.UpcomingEnterNowFragment;
import com.uppro.Fragments.UpcomingFragment;
import com.uppro.MainActivity;
import com.uppro.ModelClasses.BracketChallengeListModel;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UpcomingAdapter extends RecyclerView.Adapter<UpcomingAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    UpcomingFragment upcomingFragment;
    ArrayList<BracketChallengeListModel> list;

    public UpcomingAdapter(Context context, UpcomingFragment upcomingFragment, ArrayList<BracketChallengeListModel> list) {
        this.context = context;
        this.upcomingFragment = upcomingFragment;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.upcoming_list_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.startdate.setText("Start Date : " + util.getDateTime(list.get(position).getStartDate()));
        //holder.a_candidate.setText("End Date : " + util.getDateTime(list.get(position).getEndDate()));


        String challenge_users= String.valueOf(list.get(position).getChallenge_users_list().size());

        holder.a_candidate.setText("Available Candidate : " + challenge_users+"/"+list.get(position).getCompetitors());


        if (list.get(position).getiHaveSubmitted().equals("1")) {
            holder.enter_now.setVisibility(View.VISIBLE);
            holder.enter_now.setText("Submitted");
        } else {
            if (list.get(position).isEntered.equals("1")) {
                holder.enter_now.setVisibility(View.VISIBLE);
                holder.enter_now.setText("Submit Now");
            } else {
                holder.enter_now.setText("Enter Now");
                holder.enter_now.setVisibility(View.VISIBLE);
            }
        }


        holder.name.setText(list.get(position).getCategory_name());

        if (list.get(position).getIsPaid().equals("1"))
            holder.joining_fee.setText("Joining Fee: $" + list.get(position).getJoiningFee());
        else
            holder.joining_fee.setText("Joining Fee: Free");


        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type2/" + list.get(position).getId());
                context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
            }
        });

        holder.bracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                util.is_for_pay = false;
                UpcomingEnterNowFragment fragment = new UpcomingEnterNowFragment();
                Bundle bundle = new Bundle();
                bundle.putString("challenge_id", list.get(position).getId());
                //  bundle.putParcelable("data", list.get(position));
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = upcomingFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });


        holder.enter_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (list.get(position).getiHaveSubmitted().equals("1")) {
                    util.IOSDialog(context, "Already Submitted");
                } else {
                    if (!list.get(position).isEntered.equals("1")) {

                        if (list.get(position).getIsPaid().equals("1")) {
                            //paid event
                            Intent intent = new Intent(context, CardActivity.class);
                            intent.putExtra("challengeId", list.get(position).getId());
                            intent.putExtra("joiningFee", list.get(position).getJoiningFee());
                            context.startActivity(intent);
                            //getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                        } else {
                            SubmitChallenge(position);
                        }
                    } else {
                        SubmitChallenge(position);
                    }
                }


            }
        });
    }

    private void SubmitChallenge(int position) {
        SubmitBracketFragment fragment = new SubmitBracketFragment();
        Bundle bundle = new Bundle();
        bundle.putString("challenge_id", list.get(position).getId());
        bundle.putString("categoryname", list.get(position).category_name);
        bundle.putString("rules", list.get(position).rules);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = upcomingFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.startdate)
        TextView startdate;


        @BindView(R.id.a_candidate)
        TextView a_candidate;
        @BindView(R.id.enter_now)
        TextView enter_now;


        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.joining_fee)
        TextView joining_fee;
        @BindView(R.id.bracket)
        TextView bracket;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/


}
