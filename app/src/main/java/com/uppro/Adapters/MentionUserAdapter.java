package com.uppro.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.uppro.Fragments.CommentFragment;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MentionUserAdapter extends RecyclerView.Adapter<MentionUserAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    CommentFragment commentFragment;
    private View view;
    ArrayList<CategoryModel> list;
    ArrayList<CategoryModel> tempList;

    public MentionUserAdapter(Context context, ArrayList<CategoryModel> list, CommentFragment commentFragment) {
        this.context = context;
        this.list = list;
        this.commentFragment = commentFragment;
        this.tempList = list;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.mention_user_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).into(holder.profilePic);





        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentFragment.setclickdata(list.get(position).getName(),list.get(position).getId());
            }
        });
        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentFragment.setclickdata(list.get(position).getName(),list.get(position).getId());
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentFragment.setclickdata(list.get(position).getName(),list.get(position).getId());
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        ImageView profilePic;
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<CategoryModel> nList = new ArrayList<CategoryModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (CategoryModel wp : tempList) {
                if ((wp.getName().toLowerCase().contains(charText.toLowerCase())))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }


}
