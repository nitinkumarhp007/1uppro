package com.uppro.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Fragments.CommentFragment;
import com.uppro.Fragments.CreateFragment;
import com.uppro.Fragments.HappeningNowFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.PastVideosFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BracketHappeningNowAdapter extends RecyclerView.Adapter<BracketHappeningNowAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ChallengeListModel> list;
    HappeningNowFragment happeningNowFragment;
    SavePref savePref;
    String type = "";

    public BracketHappeningNowAdapter(Context context, String type, HappeningNowFragment happeningNowFragment, ArrayList<ChallengeListModel> list) {
        this.happeningNowFragment = happeningNowFragment;
        this.list = list;
        this.type = type;
        this.context = context;
        Inflater = LayoutInflater.from(context);
        savePref = new SavePref(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.happeningnow_list_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        //1=Happening now , 2=Wins, 3=looses

//        if (type.equals("1")) {
//            holder.profile_1_up.setVisibility(View.VISIBLE);
//            holder.profile_2_up.setVisibility(View.VISIBLE);
//        } else {
//            holder.profile_1_up.setVisibility(View.INVISIBLE);
//            holder.profile_2_up.setVisibility(View.INVISIBLE);
//        }
//
//        holder.profile_1.setText(list.get(position).getName());
//        holder.category.setText(list.get(position).getCategory_name());
//        holder.profile_1_score.setText(list.get(position).getVotestocreator());
//        holder.profile_2_score.setText(list.get(position).getVotestochallenger());
//
//        Glide.with(context).load(list.get(position).getImage()).into(holder.profile_1_pic);
//        Glide.with(context).load(list.get(position).getThumb()).into(holder.profile_1_thumb);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_1_up)
        TextView profile_1_up;
        @BindView(R.id.profile_2_up)
        TextView profile_2_up;
        @BindView(R.id.profile_1_score)
        TextView profile_1_score;
        @BindView(R.id.profile_2_score)
        TextView profile_2_score;
        @BindView(R.id.chat)
        ImageView chat;
        @BindView(R.id.profile_1)
        TextView profile_1;
        @BindView(R.id.profile_2)
        TextView profile_2;
        @BindView(R.id.profile_1_pic)
        ImageView profile_1_pic;
        @BindView(R.id.profile_1_thumb)
        ImageView profile_1_thumb;
        @BindView(R.id.profile_2_thumb)
        ImageView profile_2_thumb;
        @BindView(R.id.profile_2_pic)
        ImageView profile_2_pic;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.end_date)
        TextView end_date;
        @BindView(R.id.rules)
        TextView rules;
        @BindView(R.id.profile_1_layout)
        LinearLayout profile_1_layout;
        @BindView(R.id.video_play)
        ImageView video_play;
        @BindView(R.id.video_play_2)
        ImageView video_play_2;
        @BindView(R.id.join_challenge)
        TextView join_challenge;
        @BindView(R.id.reportOne)
        ImageView reportOne;
        @BindView(R.id.reportTwo)
        ImageView reportTwo;
        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.play_video)
        ImageView play_video;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


    private void Swich_Fragment(Fragment fragment, String user_id) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = happeningNowFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
