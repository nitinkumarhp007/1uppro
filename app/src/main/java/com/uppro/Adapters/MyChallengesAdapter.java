package com.uppro.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.uppro.Fragments.CalloutFragment;
import com.uppro.Fragments.MyChallengeListingFragment;
import com.uppro.Fragments.RequestDetailFragment;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyChallengesAdapter extends RecyclerView.Adapter<MyChallengesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ChallengeListModel> list;
    MyChallengeListingFragment myChallengeListingFragment;

    public MyChallengesAdapter(Context context, ArrayList<ChallengeListModel> list, MyChallengeListingFragment myChallengeListingFragment) {
        this.context = context;
        this.myChallengeListingFragment = myChallengeListingFragment;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.callout_list_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        //   holder.text.setText("You have been challenged to a " + list.get(position).getCategory_name() + ". Do you have what it takes to accept the challenge or are you going to wimp out. ");
        Glide.with(context).load(list.get(position).getImage()).into(holder.profile_pic);

        holder.name.setText(list.get(position).getName());
        holder.category.setText("Category: " + list.get(position).getCategory_name());


        holder.submit.setVisibility(View.INVISIBLE);



        if (!list.get(position).getCreated().isEmpty())
            holder.date.setText(util.getlongtoago(Long.parseLong(list.get(position).getCreated())));


        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Swich_Fragment(new RequestDetailFragment(), position);
            }
        });
        holder.view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Swich_Fragment(new RequestDetailFragment(), position);
            }
        });*/
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profile_pic)
        ImageView profile_pic;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.view_details)
        TextView view_details;
        @BindView(R.id.submit)
        TextView submit;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/


    private void Swich_Fragment(Fragment fragment, int position) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", list.get(position));
        bundle.putBoolean("is_from_callout", true);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = myChallengeListingFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
