package com.uppro.Adapters;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.uppro.Fragments.HappeningEnteredListFragment;
import com.uppro.ModelClasses.AllBracketChallengeRoundData;
import com.uppro.ModelClasses.ChallengeUserModel;
import com.uppro.R;
import com.uppro.Util.OnSwipeTouchListener;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class AllBracketRoundListAdapter extends RecyclerView.Adapter<AllBracketRoundListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    Fragment happeningEnteredListFragment;
    BracketClickListener bracketClickListener;
    SimpleDateFormat sdf;
    private View view;
    //    ArrayList<UserModel> list;
    ArrayList<AllBracketChallengeRoundData> list;
    String challengeId = "";
    String category_name = "";

    public AllBracketRoundListAdapter(Context context, ArrayList<AllBracketChallengeRoundData> list, String challengeId, String category_name, Fragment happeningEnteredListFragment,BracketClickListener bracketClickListener) {
        this.context = context;
        this.list = list;
        this.challengeId = challengeId;
        this.category_name = category_name;
        this.happeningEnteredListFragment = happeningEnteredListFragment;
        this.bracketClickListener = bracketClickListener;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        view = Inflater.inflate(R.layout.happening_enter_row, parent, false);
        view = Inflater.inflate(R.layout.fragment_feed_bracket_challenge, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        sdf=new SimpleDateFormat("yyyy-MM-dd");
        String date=null;
        try {
            Date dat=sdf.parse(list.get(position).getEndDate());
            sdf=new SimpleDateFormat("MM-dd-yyyy");
            date=sdf.format(dat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        holder.end_date.setText("End Date : "+(date==null?list.get(position).getEndDate():date));
        holder.end_date.setText("End Date : "+ util.convertTimeStampDateTime(Long.parseLong(String.valueOf(list.get(position).getEndDateTimestamp()))));
        holder.roundNo.setText("Round : "+list.get(position).getRound());
        holder.profile_1.setText(list.get(position).getContender1().getUsername() +"\nLevel : "+list.get(position).getContender1().getLevel());
        holder.profile_2.setText(list.get(position).getContender2().getUsername()+"\nLevel : "+list.get(position).getContender2().getLevel());


        if (list.get(position).getIsRules().equals("1")) {
            holder.rules.setVisibility(View.VISIBLE);
//            holder.rules.setText("Rules: View Rules");
        } else {
            holder.rules.setVisibility(View.GONE);
//            holder.rules.setText("Rules: No Rules");
        }

        holder.rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new IOSDialog.Builder(holder.rules.getContext())
                        .setTitle("Rules")
                        .setCancelable(false)
                        .setMessage(list.get(position).getRules()).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type2/"+ list.get(position).getChallengeId());
                context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
            }
        });

        holder.category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HappeningEnteredListFragment fragment = new HappeningEnteredListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("challenge_id", String.valueOf(list.get(position).getChallengeId()));

                fragment.setArguments(bundle);

                FragmentManager fragmentManager = happeningEnteredListFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        holder.category.setText("Bracket Challenge\n"+list.get(position).getCategory().getName());
        holder.profile_1_score.setText(""+list.get(position).getVotesContender1());
        holder.profile_2_score.setText(""+list.get(position).getVotesContender2());

        Glide.with(context).load(list.get(position).getContender1VideoThumbnail()).into(holder.profile_1_thumb);
        Glide.with(context).load(list.get(position).getContender2VideoThumbnail()).into(holder.profile_2_thumb);

        Glide.with(context).load(list.get(position).getContender1().getImage()).into(holder.profile_1_pic);
        Glide.with(context).load(list.get(position).getContender2().getImage()).into(holder.profile_2_pic);




//        FeedBracketChallengeAdapter adapter = new FeedBracketChallengeAdapter(context);
//        holder.myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
//        holder.myRecyclerView.setAdapter(adapter);

        holder.video_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bracketClickListener.onBracketGetPath(list.get(position).getContender1Video());
            }
        });

        holder.video_play_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bracketClickListener.onBracketGetPath(list.get(position).getContender2Video());
            }
        });

        holder.profile_1_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                click_touch="left";
//                LIKEVIDEO("1",0,getArguments().getString("useroneid"));
                bracketClickListener.onBracketLike("left","1",list.get(position).getChallengeId().toString(),list.get(position).getContender1().getId().toString(),position);
            }
        });

        holder.profile_2_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                click_touch="right";
//                LIKEVIDEO("1",0,getArguments().getString("usertwoid"));
                bracketClickListener.onBracketLike("right","1",list.get(position).getChallengeId().toString(),list.get(position).getContender2().getId().toString(),position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.profile_1_up)
        TextView profile_1_up;
        @BindView(R.id.profile_2_up)
        TextView profile_2_up;
        @BindView(R.id.profile_1_score)
        TextView profile_1_score;
        @BindView(R.id.profile_2_score)
        TextView profile_2_score;
        @BindView(R.id.chat)
        ImageView chat;
        @BindView(R.id.profile_1)
        TextView profile_1;
        @BindView(R.id.roundNo)
        TextView roundNo;
        @BindView(R.id.profile_2)
        TextView profile_2;
        @BindView(R.id.profile_1_pic)
        ImageView profile_1_pic;
        @BindView(R.id.profile_1_thumb)
        ImageView profile_1_thumb;
        @BindView(R.id.profile_2_thumb)
        ImageView profile_2_thumb;
        @BindView(R.id.profile_2_pic)
        ImageView profile_2_pic;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.end_date)
        TextView end_date;
        @BindView(R.id.rules)
        TextView rules;
        @BindView(R.id.profile_1_layout)
        LinearLayout profile_1_layout;
        @BindView(R.id.video_play)
        ImageView video_play;
        @BindView(R.id.video_play_2)
        ImageView video_play_2;
        @BindView(R.id.join_challenge)
        TextView join_challenge;
        @BindView(R.id.reportOne)
        ImageView reportOne;
        @BindView(R.id.reportTwo)
        ImageView reportTwo;
        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.profile_two_background)
        ImageView profile_two_background;
        @BindView(R.id.challenger_button)
        RelativeLayout challenger_button;

        @BindView(R.id.parent)
        LinearLayout parent;
        @BindView(R.id.layout2profile)
        LinearLayout layout2profile;





        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/

    public interface BracketClickListener{
        void onBracketLike(String type,String isLike,String challengedId,String challengerId,int pos);
        void onBracketGetPath(String challengerId);
    }
}
