package com.uppro.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.PastChallengeFragment;
import com.uppro.Fragments.PastVideosFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class PastVideosAdapter extends RecyclerView.Adapter<PastVideosAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    PastVideosFragment pastVideosFragment;
    ArrayList<ChallengeListModel> list;
    private SavePref savePref;

    public PastVideosAdapter(Context context, ArrayList<ChallengeListModel> list, PastVideosFragment pastVideosFragment) {
        this.context = context;
        savePref = new SavePref(context);
        this.list = list;
        this.pastVideosFragment = pastVideosFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.pastvideos_list_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        holder.ranking.setText("Rank: " + list.get(position).getIsRules());
        holder.viewsss.setText(list.get(position).getCategory_name() + " - Total Votes: " + list.get(position).getComment_count());
        Glide.with(context).load(list.get(position).getImage()).into(holder.profilePic);
        Glide.with(context).load(list.get(position).getThumb()).into(holder.image);


        holder.play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getVideo() != null) {
                    if (!list.get(position).getVideo().isEmpty()) {

                        if (ConnectivityReceiver.isConnected()) {
                            pastVideosFragment.GETPATH(list.get(position).getVideo());//0=>voteToCreator
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }

                    }
                }
            }
        });

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUserId().equals(savePref.getID())) {
                    Swich_Fragment_Profile(new ProfileFragment(), list.get(position).getUserId());
                } else {
                    Swich_Fragment_Profile(new OtherProfileFragment(), list.get(position).getUserId());
                }

            }
        });
        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUserId().equals(savePref.getID())) {
                    Swich_Fragment_Profile(new ProfileFragment(), list.get(position).getUserId());
                } else {
                    Swich_Fragment_Profile(new OtherProfileFragment(), list.get(position).getUserId());
                }

            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PastChallengeFragment fragment = new PastChallengeFragment();

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("list", list);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = pastVideosFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        RoundedImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.viewsss)
        TextView viewsss;
        @BindView(R.id.ranking)
        TextView ranking;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.play_button)
        ImageView play_button;
        @BindView(R.id.layout)
        RelativeLayout layout;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    /* public void filter(String charText) {
         charText = charText.toLowerCase();
         ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
         if (charText.length() == 0) {
             nList.addAll(tempList);
         } else {
             for (ChattingModel wp : tempList) {
                 if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                 {
                     nList.add(wp);
                 }
             }
         }
         list = nList;
         notifyDataSetChanged();
     }*/

    private void Swich_Fragment_Profile(Fragment fragment, String user_id) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = pastVideosFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
