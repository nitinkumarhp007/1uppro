package com.uppro.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.uppro.Activities.BracketChallengeNewActivity;
import com.uppro.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FeedBracketChallengeAdapter extends RecyclerView.Adapter<FeedBracketChallengeAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;

    public FeedBracketChallengeAdapter(Context context) {
        this.context = context;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.feed_bracket_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
/*
        holder.date.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getTimestamp())));

        holder.name.setText(list.get(position).getOpponent_name());

        if (list.get(position).getChat_type().equals("0"))
            holder.message.setText(list.get(position).getMessage());
        else if (list.get(position).getChat_type().equals("1"))
            holder.message.setText("Media File");


        Glide.with(context).load(list.get(position).getImage()).into(holder.profilePic);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChattngActivity.class);
                intent.putExtra("friend_name", list.get(position).getOpponent_name());
                intent.putExtra("friend_id", list.get(position).getUser_id());
                context.startActivity(intent);
            }
        });

       */

        holder.bracket_rap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, BracketChallengeNewActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return 7;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.bracket_rap)
        TextView bracket_rap;

       /* @BindView(R.id.profile_pic)
        CircleImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        */

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/
}
