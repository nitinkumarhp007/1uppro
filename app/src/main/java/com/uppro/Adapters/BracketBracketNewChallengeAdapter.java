package com.uppro.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uppro.R;

public class BracketBracketNewChallengeAdapter extends RecyclerView.Adapter<BracketBracketNewChallengeAdapter.Layout> {

    @NonNull
    @Override
    public Layout onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Layout(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bracket_round_challenge, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Layout holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class Layout extends RecyclerView.ViewHolder {
        public Layout(@NonNull View itemView) {
            super(itemView);
        }
    }

}