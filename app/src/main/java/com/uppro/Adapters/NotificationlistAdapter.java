package com.uppro.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Activities.BlockedUsersActivity;
import com.uppro.Fragments.NotificationFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.ModelClasses.CommentModel;
import com.uppro.ModelClasses.NotificationModel;
import com.uppro.R;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class NotificationlistAdapter extends RecyclerView.Adapter<NotificationlistAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<NotificationModel> list;
    NotificationFragment notificationFragment;
    private SavePref savePref;

    public NotificationlistAdapter(Context context, ArrayList<NotificationModel> list, NotificationFragment notificationFragment) {
        this.context = context;
        this.list = list;
        savePref = new SavePref(context);
        this.notificationFragment = notificationFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.noti_list_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (list.get(position).getIsRead().equals("1"))
            holder.card_view.setBackgroundColor(context.getResources().getColor(R.color.white));
        else
            holder.card_view.setBackgroundColor(context.getResources().getColor(R.color.blue_light));

        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.placeholder).into(holder.profilePic);
        holder.message.setText(list.get(position).getText());
        holder.date.setText(util.convertTimeStampDate(Long.parseLong(list.get(position).getCreated())));


        holder.profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (list.get(position).getUser_id().equals(savePref.getID())) {
                    Swich_Fragment_Profile(new ProfileFragment(), list.get(position).getUser_id());
                } else {
                    Swich_Fragment_Profile(new OtherProfileFragment(), list.get(position).getUser_id());
                }
            }
        });


        holder.llName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationFragment.READNOTIFICATION_API(position);
            }
        });


        holder.date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationFragment.READNOTIFICATION_API(position);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                notificationFragment.DeleteAlert(position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.main_layout)
        RelativeLayout main_layout;
        @BindView(R.id.profile_pic)
        RoundedImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.llName)
        LinearLayout llName;
        @BindView(R.id.card_view)
        CardView card_view;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private void Swich_Fragment_Profile(Fragment fragment, String user_id) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = notificationFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
