package com.uppro.Adapters;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.uppro.Activities.BracketChallengeNewActivity;
import com.uppro.Fragments.FeedBracketChallengeFragment;
import com.uppro.Fragments.HappeningEnteredListFragment;
import com.uppro.Fragments.VoteHappeningNowFragment;
import com.uppro.ModelClasses.ChallengeUserModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.OnSwipeTouchListener;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class HappeningEnterAdapter extends RecyclerView.Adapter<HappeningEnterAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    Fragment happeningEnteredListFragment;
    private View view;
    //    ArrayList<UserModel> list;
    ArrayList<ChallengeUserModel> list;
    String challengeId = "";
    String category_name = "";

    public HappeningEnterAdapter(Context context, ArrayList<ChallengeUserModel> list, String challengeId, String category_name, Fragment happeningEnteredListFragment) {
        this.context = context;
        this.list = list;
        this.challengeId = challengeId;
        this.category_name = category_name;
        this.happeningEnteredListFragment = happeningEnteredListFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        view = Inflater.inflate(R.layout.happening_enter_row, parent, false);
        view = Inflater.inflate(R.layout.happening_bracketchallenge_new_rowitem, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

//        holder.seeds.setText(list.get(position).getComment_count() + context.getString(R.string.seeds));

        Glide.with(context).load(list.get(position).getProfile_pic_one()).into(holder.CImage1);
        holder.CName1.setText(list.get(position).getName_one() +" (Level : "+list.get(position).getLevelone()+")");
        holder.CSeeds1.setText(list.get(position).getVotesContender1().toString() +" Votes");

        if (list.get(position).getRoundWinnerId()!=null&&!list.get(position).getRoundWinnerId().isEmpty()&& !list.get(position).getRoundWinnerId().equals("0")) {
            holder.CWinner.setText((list.get(position).getRoundWinnerId().equals(list.get(position).getUser_id_one()) ? list.get(position).getName_one() : list.get(position).getName_two()));
            holder.CWinnerLine.setVisibility(View.VISIBLE);
            holder.tvWinner.setVisibility(View.VISIBLE);
        }else {
            holder.CWinner.setText("");
            holder.CWinnerLine.setVisibility(View.GONE);
            holder.tvWinner.setVisibility(View.GONE);
        }
        holder.CName2.setText(list.get(position).getName_two()+" (Level : "+list.get(position).getLeveltwo()+")");
        Glide.with(context).load(list.get(position).getProfile_pic_two()).into(holder.CImage2);
        holder.CSeeds2.setText(list.get(position).getVotesContender2().toString() +" Votes");


        holder.CGotoBattle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedBracketChallengeFragment fragment = new FeedBracketChallengeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("challengeronelevel", list.get(position).getLevelone());
                bundle.putString("challengertwolevel", list.get(position).getLeveltwo());
                bundle.putString("challengeid", list.get(position).getChallengeId());
                bundle.putString("rules", list.get(position).getRules());
                bundle.putString("category_name",list.get(position).getCategory());
                bundle.putString("startdate",list.get(position).getStartdate());
                bundle.putString("enddate",list.get(position).getEnddate());
                bundle.putString("useronename", list.get(position).getName_one());
                bundle.putString("useroneimage", list.get(position).getProfile_pic_one());
                bundle.putString("useroneid", list.get(position).getUser_id_one());
                bundle.putString("useronevideo", list.get(position).getVideo_url_one());
                bundle.putString("useronethumb", list.get(position).getVideo_thamb_one());
                bundle.putString("useronevote", list.get(position).getVotesContender1());
                bundle.putString("useronevoted", list.get(position).getIsVotedContender1());

                bundle.putString("usertwoname", list.get(position).getName_two());
                bundle.putString("usertwoimage", list.get(position).getProfile_pic_two());
                bundle.putString("usertwoid", list.get(position).getUser_id_two());
                bundle.putString("usertwovideo", list.get(position).getVideo_url_two());
                bundle.putString("usertwothumb", list.get(position).getVideo_thamb_two());
                bundle.putString("usertwovote", list.get(position).getVotesContender2());
                bundle.putString("usertwovoted", list.get(position).getIsVotedContender2());

                fragment.setArguments(bundle);

                FragmentManager fragmentManager = happeningEnteredListFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

//        holder.play_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (list.get(position).getVideo_url() != null) {
//                    if (!list.get(position).getVideo_url().isEmpty()) {
//
//                        if (ConnectivityReceiver.isConnected()) {
//                            happeningEnteredListFragment.GETPATH(list.get(position).getVideo_url());//0=>voteToCreator
//                        } else {
//                            util.IOSDialog(context, util.internet_Connection_Error);
//                        }
//
//                    }
//                }
//            }
//        });
//
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                VoteHappeningNowFragment fragment = new VoteHappeningNowFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString("category_name", category_name);
//                bundle.putString("challengeId", challengeId);
//                bundle.putParcelableArrayList("list", list);
//                fragment.setArguments(bundle);
//
//                FragmentManager fragmentManager = happeningEnteredListFragment.getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.frame_container, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
//            }
//        });

        holder.itemView.setOnTouchListener(new OnSwipeTouchListener(holder.itemView.getContext()) {

            @Override
            public void onSwipeRight() {
                Toast.makeText(holder.itemView.getContext(), "rtl swipe", Toast.LENGTH_SHORT).show ();
            }

            @Override
            public void onSwipeLeft() {
                Toast.makeText(holder.itemView.getContext(), "ltr swipe", Toast.LENGTH_SHORT).show ();
            }

            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
            }

            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
//
//        @BindView(R.id.profile_pic)
//        CircleImageView profilePic;
//        @BindView(R.id.name)
//        TextView name;
//        @BindView(R.id.seeds)
//        TextView seeds;
//        @BindView(R.id.go_to_battle)
//        TextView go_to_battle;
//        @BindView(R.id.image)
//        ImageView image;
//        @BindView(R.id.play_button)
//        ImageView play_button;

        @BindView(R.id.CImage1)
        CircleImageView CImage1;
        @BindView(R.id.CName1)
        TextView CName1;
        @BindView(R.id.CSeeds1)
        TextView CSeeds1;
        @BindView(R.id.CGotoBattle)
        TextView CGotoBattle;
        @BindView(R.id.CImage2)
        CircleImageView CImage2;
        @BindView(R.id.CName2)
        TextView CName2;

        @BindView(R.id.CSeeds2)
        TextView CSeeds2;
        @BindView(R.id.CWinner)
        TextView CWinner;
        @BindView(R.id.tvWinner)
        TextView tvWinner;
        @BindView(R.id.CWinnerLine)
        View CWinnerLine;



        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/
}
