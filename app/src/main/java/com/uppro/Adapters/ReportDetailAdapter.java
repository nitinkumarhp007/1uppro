package com.uppro.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.uppro.Fragments.ChallengeDetailFragment;
import com.uppro.Fragments.HomeFragment;
import com.uppro.ModelClasses.ReportTextModel;
import com.uppro.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReportDetailAdapter extends RecyclerView.Adapter<ReportDetailAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ChallengeDetailFragment homeFragment;
    ArrayList<ReportTextModel> list;
    String challengeId;

    public ReportDetailAdapter(Context context, ArrayList<ReportTextModel> list, ChallengeDetailFragment homeFragment, String challengeId) {
        this.context = context;
        this.list = list;
        this.challengeId = challengeId;
        this.homeFragment = homeFragment;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.report_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.text2.setText(list.get(position).getText());






        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeFragment.REPORTCHALLENGE_API(challengeId,list.get(position).getId());
            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text2)
        TextView text2;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
