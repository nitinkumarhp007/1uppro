package com.uppro.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.uppro.Fragments.BracketHappeningNowFragment;
import com.uppro.Fragments.BracketMainFragment;
import com.uppro.Fragments.HappeningEnteredListFragment;
import com.uppro.Fragments.HomeBracketFragment;
import com.uppro.Fragments.SubmitBracketFragment;
import com.uppro.Fragments.UpcomingFragment;
import com.uppro.ModelClasses.BracketChallengeDetailAPINewModel;
import com.uppro.ModelClasses.BracketChallengeListModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.util;
import com.uppro.parser.AllAPIS;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HappeningNowBracketAdapter extends RecyclerView.Adapter<HappeningNowBracketAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    BracketHappeningNowFragment bracketHappeningNowFragment;
    ArrayList<BracketChallengeDetailAPINewModel> list;


    public HappeningNowBracketAdapter(Context context, BracketHappeningNowFragment bracketHappeningNowFragment, ArrayList<BracketChallengeDetailAPINewModel> list) {
        this.context = context;
        this.bracketHappeningNowFragment = bracketHappeningNowFragment;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.happeningnow_list_row2, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context).load(list.get(position).getCategory_image()).into(holder.image);

        holder.startdate.setText("Start Date : " + util.getDateTime(list.get(position).getStartDate()));
        holder.enddate.setText("End Date : " + util.getDateTime(list.get(position).getEndDate()));


        holder.name.setText(list.get(position).getCategory_name());

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, AllAPIS.BASE_URL + "share_type2/"+ list.get(position).getId());
                context.startActivity(Intent.createChooser(sharingIntent, "Share Challenge External"));
            }
        });


        holder.rlOutline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bracket_c_call(position);
/*
                HomeBracketFragment fragment = new HomeBracketFragment();
                Bundle bundle = new Bundle();
                bundle.putString("challenge_id", list.get(position).getId());
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = bracketHappeningNowFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/

            }
        });

        holder.bracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bracket_c_call(position);
            }
        });

        holder.play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getVideo() != null) {
                    if (!list.get(position).getVideo().isEmpty()) {

                        if (ConnectivityReceiver.isConnected()) {
                            bracketHappeningNowFragment.GETPATH(list.get(position).getVideo());//0=>voteToCreator
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }


                    }
                }
            }
        });


    }

    private void bracket_c_call(int position)
    {
        // BracketMainFragment fragment = new BracketMainFragment();
        HappeningEnteredListFragment fragment = new HappeningEnteredListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("challenge_id", list.get(position).getId());
        bundle.putParcelable("data", list.get(position));
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = bracketHappeningNowFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.rlOutline)
        RelativeLayout rlOutline;
        @BindView(R.id.start_date)
        TextView startdate;


        @BindView(R.id.end_date)
        TextView enddate;
        @BindView(R.id.layout2)
        RelativeLayout layout2;


        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.play_video)
        ImageView play_video;
        @BindView(R.id.bracket)
        TextView bracket;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase();
        ArrayList<ChattingModel> nList = new ArrayList<ChattingModel>();
        if (charText.length() == 0) {
            nList.addAll(tempList);
        } else {
            for (ChattingModel wp : tempList) {
                if (wp.getOpponent_name().toLowerCase().contains(charText.toLowerCase()))//contains for less accurate result nd matches for accurate result
                {
                    nList.add(wp);
                }
            }
        }
        list = nList;
        notifyDataSetChanged();
    }*/


}
