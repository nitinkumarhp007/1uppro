package com.uppro.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Fragments.CommentFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.ModelClasses.CommentModel;
import com.uppro.R;
import com.uppro.Util.Parameters;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;

    private View view;
    ArrayList<CommentModel> list;
    private SavePref savePref;
    CommentFragment commentFragment;

    public CommentAdapter(Context context, ArrayList<CommentModel> list, CommentFragment commentFragment) {
        this.context = context;
        this.list = list;
        this.commentFragment = commentFragment;
        savePref = new SavePref(context);
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.comment_layout, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (list.get(position).getUser_id().equals(savePref.getID())) {
            holder.ivEdit.setVisibility(View.VISIBLE);
            holder.ivDelete.setVisibility(View.VISIBLE);
        }else {
            holder.ivEdit.setVisibility(View.GONE);
            holder.ivDelete.setVisibility(View.GONE);
        }
        holder.comment.addAutoLinkMode(AutoLinkMode.MODE_MENTION);
        //holder.comment.setHashtagModeColor(ContextCompat.getColor(context, R.color.ios_blue));
        holder.comment.setMentionModeColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));


        holder.time.setText(util.convertTimeStampDateTime(Long.parseLong(list.get(position).getCreated())));

        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.placeholder).into(holder.image);
        holder.comment.setAutoLinkText(list.get(position).getComment());


        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUser_id().equals(savePref.getID())) {
                    Swich_Fragment_Profile(new ProfileFragment(), list.get(position).getUser_id());
                } else {
                    Swich_Fragment_Profile(new OtherProfileFragment(), list.get(position).getUser_id());
                }
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentFragment.edit(position);
            }
        });
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentFragment.DELETECOMMENT_API(position);
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUser_id().equals(savePref.getID())) {
                    Swich_Fragment_Profile(new ProfileFragment(), list.get(position).getUser_id());
                } else {
                    Swich_Fragment_Profile(new OtherProfileFragment(), list.get(position).getUser_id());
                }
            }
        });

        holder.comment.setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
            @Override
            public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {

                if (autoLinkMode == AutoLinkMode.MODE_MENTION) {

                    // Toast.makeText(context, "Click on MODE_MENTION" + " " + matchedText.substring(1), Toast.LENGTH_SHORT).show();

                    Fragment fragment = null;

                    if ((matchedText.substring(1)).equals(savePref.getStringLatest(Parameters.USERNAME))) {
                        fragment = new ProfileFragment();
                    } else {
                        fragment = new OtherProfileFragment();
                    }


                    Bundle bundle = new Bundle();
                    bundle.putString("user_id", matchedText.substring(1));
                    bundle.putBoolean("back_on", true);
                    fragment.setArguments(bundle);

                    FragmentManager fragmentManager = commentFragment.getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        RoundedImageView image;
        @BindView(R.id.ivEdit)
        ImageView ivEdit;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.comment)
        AutoLinkTextView comment;
        @BindView(R.id.time)
        TextView time;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private void Swich_Fragment_Profile(Fragment fragment, String user_id) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", user_id);
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = commentFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
