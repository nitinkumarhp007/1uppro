package com.uppro.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Activities.BlockedUsersActivity;
import com.uppro.R;
import com.uppro.Activities.BlockedUsersActivity;
import com.uppro.ModelClasses.CommentModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class BlockedUserlistAdapter extends RecyclerView.Adapter<BlockedUserlistAdapter.RecyclerViewHolder> {
    BlockedUsersActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<CommentModel> list;

    public BlockedUserlistAdapter(BlockedUsersActivity context, ArrayList<CommentModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.blocked_users_list_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.placeholder).into(holder.profilePic);


        holder.unblock_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.Alert(list.get(position).getUser_id(),position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        RoundedImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.unblock_button)
        TextView unblock_button;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
