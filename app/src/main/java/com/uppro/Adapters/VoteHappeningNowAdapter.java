package com.uppro.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.uppro.Activities.ExoPlayerActivity;
import com.uppro.Fragments.CommentFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.PastChallengeFragment;
import com.uppro.Fragments.ProfileFragment;
import com.uppro.Fragments.VoteHappeningNowFragment;
import com.uppro.ModelClasses.ChallengeListModel;
import com.uppro.ModelClasses.UserModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class VoteHappeningNowAdapter extends RecyclerView.Adapter<VoteHappeningNowAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private SavePref savePref;
    ArrayList<UserModel> list;
    private View view;
    String category_name = "";
    VoteHappeningNowFragment voteHappeningfragment;

    public VoteHappeningNowAdapter(Context context, ArrayList<UserModel> list, String category_name, VoteHappeningNowFragment voteHappeningfragment) {
        this.context = context;
        this.list = list;
        this.category_name = category_name;
        savePref = new SavePref(context);
        this.voteHappeningfragment = voteHappeningfragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.vote_challenge_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.profile_1.setText(list.get(position).getName());
        holder.category_name.setText("Category: " + category_name);
        //  holder.category_name.setText(category_name + " - Rank: " + list.get(position).getIsRules());
        holder.comment_count.setText(list.get(position).getComment_count());
        Glide.with(context).load(list.get(position).getProfile_pic()).into(holder.profile_1_pic);
        Glide.with(context).load(list.get(position).getVideo_thamb()).into(holder.image);

        /*holder.profile_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUserId().equals(savePref.getID())) {
                    Swich_Fragment(new ProfileFragment(), list.get(position).getUserId());
                } else {
                    Swich_Fragment(new OtherProfileFragment(), list.get(position).getUserId());
                }

            }
        });
        holder.profile_1_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getUserId().equals(savePref.getID())) {
                    Swich_Fragment(new ProfileFragment(), list.get(position).getUserId());
                } else {
                    Swich_Fragment(new OtherProfileFragment(), list.get(position).getUserId());
                }

            }
        });*/


        holder.video_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getVideo_url() != null) {
                    if (!list.get(position).getVideo_url().isEmpty()) {

                        if (ConnectivityReceiver.isConnected()) {
                            voteHappeningfragment.GETPATH(list.get(position).getVideo_url());//0=>voteToCreator
                        } else {
                            util.IOSDialog(context, util.internet_Connection_Error);
                        }


                    }
                }
            }
        });


        holder.profile_1_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    voteHappeningfragment.LIKEVIDEO("1", position, list.get(position).getUser_id());//1=>like )
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        });
        holder.profile_1_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    voteHappeningfragment.LIKEVIDEO("0", position, list.get(position).getUser_id());//1=>like )
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        });
       /* holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentFragment fragment = new CommentFragment();

                Bundle bundle = new Bundle();
                bundle.putString("challenge_id", list.get(position).getId());
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = pastChallengeFragment.getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.category_name)
        TextView category_name;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.video_play)
        ImageView video_play;
        @BindView(R.id.comment)
        ImageView comment;
        @BindView(R.id.profile_1_pic)
        ImageView profile_1_pic;
        @BindView(R.id.profile_1)
        TextView profile_1;
        @BindView(R.id.profile_1_up)
        TextView profile_1_up;
        @BindView(R.id.profile_1_down)
        TextView profile_1_down;
        @BindView(R.id.comment_count)
        TextView comment_count;
        @BindView(R.id.share)
        ImageView share;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
