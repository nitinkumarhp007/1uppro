package com.uppro.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.uppro.Fragments.FollowingUserListFragment;
import com.uppro.Fragments.OtherProfileFragment;
import com.uppro.Fragments.SearchFragment;
import com.uppro.ModelClasses.CategoryModel;
import com.uppro.R;
import com.uppro.Util.ConnectivityReceiver;
import com.uppro.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class SearchUsersAdapter extends RecyclerView.Adapter<SearchUsersAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<CategoryModel> list;
    SearchFragment searchFragment;

    public SearchUsersAdapter(Context context, ArrayList<CategoryModel> list, SearchFragment searchFragment) {
        this.context = context;
        this.list = list;
        this.searchFragment = searchFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.search_userlist_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.logo).into(holder.profilePic);

       /* if (list.get(position).getIsFollowed().equals("1"))
            holder.follow.setText("Following");
        else
            holder.follow.setText("Become a fan");*/




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Swich_Fragment(new OtherProfileFragment(),position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_pic)
        RoundedImageView profilePic;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.follow)
        TextView follow;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private void Swich_Fragment(Fragment fragment, int position) {

        Bundle bundle = new Bundle();
        bundle.putString("user_id", list.get(position).getId());
        bundle.putBoolean("back_on", true);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = searchFragment.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
