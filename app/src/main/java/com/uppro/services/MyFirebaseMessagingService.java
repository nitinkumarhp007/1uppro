package com.uppro.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.uppro.Activities.ChattngActivity;
import com.uppro.MainActivity;
import com.uppro.R;
import com.uppro.Util.SavePref;
import com.uppro.Util.util;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    String message = "", message_id = "", friend_id = "", notification_code = "", challenge_id = "", username = "", sender_id = "";

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    NotificationManager notificationManager;
    Notification notification;
    String groupId = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());
        message = remoteMessage.getData().get("title");
        notification_code = remoteMessage.getData().get("type");

        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        }

        if (notification_code.equals("11")) {//chat
            try {
                JSONObject body1 = new JSONObject(remoteMessage.getData().get("body"));
                JSONObject object = body1.getJSONObject("chatMessage");

                String message123 = object.getString("message");
                if (message == null)
                    message = "";
                message = message123;

                String created = object.getString("created");
                String type = object.getString("type");
                sender_id = object.getString("senderId");
                message_id = object.getString("id");
                username = object.getJSONObject("otherUser").optString("name");
                Log.e("chat_idp", savePref.getCHAT_ID());
                Log.e("chat_idp", sender_id);
                if (savePref.getChatScreen() && savePref.getCHAT_ID().equals(sender_id)) {
                    publishResultsMessage(message_id, type, message123, created, username, sender_id);
                } else {
                    sendNotification(getApplicationContext(), message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            if (notification_code.equals("17"))//nitin followed you
            {
                JSONObject body1 = null;
                try {
                    body1 = new JSONObject(remoteMessage.getData().get("body"));
                    JSONObject object = body1.getJSONObject("user");
                    friend_id = object.getString("id");

                    sendNotification(getApplicationContext(), message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (notification_code.equals("20"))//nitin followed you
            {
//                JSONObject body1 = null;
                try {
                    JSONObject body1 = new JSONObject(remoteMessage.getData().get("body"));
//                    body1 = new JSONObject(remoteMessage.getData().get("body"));
//                    JSONObject object = body1.getJSONObject("user");
//                    friend_id = object.getString("id");
                    message = body1.getString("notificationMessage");
                    sendNotification(getApplicationContext(), message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                JSONObject body1 = null;
                try {
                    body1 = new JSONObject(remoteMessage.getData().get("body"));
                    JSONObject object = body1.optJSONObject("challenge");
                    if (notification_code.equals("7")) {//Nitin has tagged you in the challenge.
                        challenge_id = object.optString("challengeId");
                    } else if (notification_code.equals("19")) {//new bracket challenge added
                        //Code to be done
                    } else
                        challenge_id = object.optString("id");
                    sendNotification(getApplicationContext(), message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
        Log.e("token___", token);
    }


    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;

        if (notification_code.equals("8") || notification_code.equals("10") || notification_code.equals("12")
                || notification_code.equals("13") || notification_code.equals("15") || notification_code.equals("16") || notification_code.equals("20")) {
            //Home
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("challenge_id", challenge_id);
            intent.putExtra("friend_id", friend_id);

            util.from_push = true;

        } else if (notification_code.equals("20")) {
            //Home
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("challenge_id", challenge_id);
            intent.putExtra("friend_id", friend_id);

            util.from_push = false;

        } else if (notification_code.equals("11")) {//chat
            intent = new Intent(context, ChattngActivity.class);
            intent.putExtra("friend_name", username);
            intent.putExtra("friend_id", sender_id);
        } else {
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("challenge_id", challenge_id);
            intent.putExtra("friend_id", friend_id);

            util.from_push = true;
        }
        intent.putExtra("notification_code", notification_code);
        intent.putExtra("is_from_push", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity
                    (context, 0, intent, PendingIntent.FLAG_MUTABLE);
        }
        else
        {
            pendingIntent = PendingIntent.getActivity
                    (context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        }

//        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setOngoing(false)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

    /*    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.noti_logo).setLargeIcon(icon1);;
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(icon1);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    // offer_status
    private void publishResultsMessage(String message_id, String type, String message, String created, String username, String sender_id) {

        Intent intent = new Intent(util.NOTIFICATION_MESSAGE);
        intent.putExtra("message", message);
        intent.putExtra("created", created);
        intent.putExtra("username", username);
        intent.putExtra("sender_id", sender_id);
        intent.putExtra("type", type);
        intent.putExtra("message_id", message_id);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

   /* ========================================================================

    type: 1
    notificationMessage = `userName has challenged you.`;

========================================================================

    type: 2
            const notificationMessage = `userName has accepted your challenge request.`;

========================================================================

    type: 3
            const notificationMessage = `userName has declined your challenge request.`;

========================================================================

    type: 4
            const notificationMessage = `userName has submitted the challenge request.`;

========================================================================

    type: 5 // vote_challenge_push_to_challenge_creator
            const notificationMessage = `userName has voted on the challenge.`;

========================================================================

    type: 6 // vote_challenge_push_to_challenged_user
            const notificationMessage = `userName has voted on the challenge.`;

========================================================================

    type: 11
            const notificationMessage = `userName has sent you a message.`;

========================================================================*/

}
